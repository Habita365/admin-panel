import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { isNotNumeric } from "utils";
import { useState } from "react";

const useFetchList = (pageSize, limitSize) => {
  const location = useLocation();

  const queryParams = new URLSearchParams(location.search);
  const querySortModel = queryParams.get("sortModel");
  const queryPage = queryParams.get("page");
  const queryLimit = queryParams.get("limit");

  const parsedSortModel = JSON.parse(querySortModel);

  const dispatch = useDispatch();

  const [page, setPage] = useState(
    isNotNumeric(queryPage) ? pageSize : parseInt(queryPage)
  );
  const [limit, setLimit] = useState(
    isNotNumeric(queryLimit) ? limitSize : parseInt(queryLimit)
  );
  const [sortModel, setSortModel] = useState(
    parsedSortModel?.length ? parsedSortModel : []
  );

  return {
    page,
    limit,
    sortModel,
    setPage,
    setLimit,
    dispatch,
    setSortModel,
    queryParams,
  };
};

export default useFetchList;
