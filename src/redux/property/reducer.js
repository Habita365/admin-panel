import {
  PROPERTY_CHANGE_STATUS,
  PROPERTY_CLEAR_MESSAGE,
  PROPERTY_DELETE,
  PROPERTY_DETAILS_FAIL,
  PROPERTY_DETAILS_LOADING,
  PROPERTY_DETAILS_SUCCESS,
  PROPERTY_FAIL,
  PROPERTY_LIST_FAIL,
  PROPERTY_LIST_LOADING,
  PROPERTY_LIST_SUCCESS,
  PROPERTY_TOGGLE_FEATURED,
} from "./types";

const listDefaultState = {
  Properties: [],
  loading: true,
  propertyMessage: "",
  propertyStatus: "",
  TotalProperties: 0,
};

export const propertyListReducer = (state = listDefaultState, action) => {
  switch (action.type) {
    case PROPERTY_LIST_SUCCESS: {
      let TotalProperties = action.payload?.TotalProperties;

      let Properties = action.payload?.Properties;

      let PropertiesMapped = [];

      if (Array.isArray(Properties)) {
        PropertiesMapped = Properties.map((data) => {
          let SellValue = data.SellCommercialConditions?.Value;
          const RentValue = data.RentCommercialConditions?.Value;

          let CommercialConditions;

          if (data.Purpose === "BOTH" || data.Purpose === "RENT") {
            CommercialConditions = {
              ...data.RentCommercialConditions,
            };
          } else {
            CommercialConditions = {
              ...data.SellCommercialConditions,
            };
          }

          const id = data.Id;
          const Address = data.Address;
          const Complex = data.Complex;

          delete data.Address;
          delete data.Id;
          // delete data.SellCommercialConditions;
          delete data.Complex;
          // delete data.RentCommercialConditions;
          delete Address.Id;

          return {
            ...Address,
            SellValue,
            RentValue,
            ...CommercialConditions,
            ...Complex,
            ...data,
            id,
          };
        });
      }

      return {
        loading: false,
        Properties: PropertiesMapped,
        TotalProperties,
      };
    }
    case PROPERTY_LIST_LOADING: {
      return {
        ...state,
        loading: true,
      };
    }
    case PROPERTY_DELETE: {
      const propertyId = action.payload;

      const remainingProperties = state.Properties?.filter((property) => {
        return propertyId !== property.id;
      });

      return {
        ...state,
        Properties: remainingProperties,
        propertyMessage: "Property successfully deleted",
        propertyStatus: "SUCCESS",
      };
    }
    case PROPERTY_FAIL: {
      return {
        ...state,
        propertyMessage: action.payload,
        propertyStatus: "FAILED",
      };
    }
    case PROPERTY_CLEAR_MESSAGE: {
      return {
        ...state,
        propertyMessage: "",
        propertyStatus: "",
      };
    }
    case PROPERTY_CHANGE_STATUS: {
      const { propertyId, propertyStatus, message } = action.payload;

      const Properties = state.Properties?.map((property) => {
        if (property.id === propertyId) {
          return {
            ...property,
            AdStatus: propertyStatus,
          };
        }

        return property;
      });

      return {
        ...state,
        Properties,
        propertyMessage: message || "Property status successfully changed",
        propertyStatus: "SUCCESS",
      };
    }
    case PROPERTY_TOGGLE_FEATURED: {
      const { propertyId, IsFeatured, message } = action.payload;

      const Properties = state.Properties?.map((property) => {
        if (property.id === propertyId) {
          return {
            ...property,
            IsFeatured,
          };
        }

        return property;
      });

      return {
        ...state,
        Properties,
        propertyMessage: message || "Property status successfully changed",
        propertyStatus: "SUCCESS",
      };
    }
    case PROPERTY_LIST_FAIL: {
      return {
        loading: false,
        error: action.payload,
        Properties: [],
        TotalProperties: 0,
      };
    }
    default: {
      return state;
    }
  }
};

const defaultState = {
  loading: true,
  Property: null,
};

export const propertyDetailsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case PROPERTY_DETAILS_SUCCESS:
      const { Property } = action.payload;
      return {
        loading: false,
        Property,
      };
    case PROPERTY_DETAILS_LOADING:
      return {
        ...state,
        loading: true,
      };
    case PROPERTY_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
        Property: null,
      };
    default:
      return state;
  }
};
