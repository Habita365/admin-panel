import axios from "axios";
import {
  PROPERTY_CHANGE_STATUS,
  PROPERTY_CLEAR_MESSAGE,
  PROPERTY_DELETE,
  PROPERTY_DETAILS_FAIL,
  PROPERTY_DETAILS_LOADING,
  PROPERTY_DETAILS_SUCCESS,
  PROPERTY_FAIL,
  PROPERTY_LIST_FAIL,
  PROPERTY_LIST_LOADING,
  PROPERTY_LIST_SUCCESS,
  PROPERTY_TOGGLE_FEATURED,
} from "./types";
import handleError from "utils/handleError";

const backend = process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT;

export const changePropertyStatus =
  (propertyId, propertyStatus, loading) => async (dispatch, getState) => {
    loading();
    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const res = await axios.patch(
        `${backend}/property/ad_status/${propertyId}`,
        {
          AdStatus: propertyStatus,
        },
        config
      );

      dispatch({
        type: PROPERTY_CHANGE_STATUS,
        payload: {
          propertyId,
          propertyStatus,
          message: res.data?.message,
        },
      });
    } catch (err) {
      handleError(err, dispatch, PROPERTY_FAIL);
    }
    loading();
  };

export const toggleIsFeatured =
  (propertyId, IsFeatured) => async (dispatch, getState) => {
    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const res = await axios.put(
        `${backend}/property/${propertyId}`,
        {
          IsFeatured: !IsFeatured,
        },
        config
      );

      dispatch({
        type: PROPERTY_TOGGLE_FEATURED,
        payload: {
          propertyId,
          IsFeatured: !IsFeatured,
          message: res.data?.message,
        },
      });
    } catch (err) {
      handleError(err, dispatch, PROPERTY_FAIL);
    }
  };

export const deleteProperty =
  (propertyId, loading) => async (dispatch, getState) => {
    loading();
    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      await axios.delete(`${backend}/property/${propertyId}`, config);

      dispatch({
        type: PROPERTY_DELETE,
        payload: propertyId,
      });
    } catch (err) {
      handleError(err, dispatch, PROPERTY_FAIL);
    }
    loading();
  };

export const listProperties =
  ({ page, limit, sortModel, Address, isFeatured }) =>
  async (dispatch, getState) => {
    dispatch({
      type: PROPERTY_LIST_LOADING,
    });

    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const sortedColumn = sortModel?.[0];

      const { field, sort } = sortedColumn || {};

      const addressText = `${Address ? `&Address=${Address}` : ""}`;
      const featuredText = `${isFeatured ? `&IsFeatured=${isFeatured}` : ""}`;

      const baseUrl = `${backend}/property?Page=${page}&Limit=${limit}${addressText}${featuredText}`;

      const { data } = await axios.get(
        sortedColumn
          ? `${baseUrl}&Column=${
              field === "id" ? "Id" : field
            }&Direction=${sort.toUpperCase()}`
          : `${baseUrl}`,
        config
      );

      dispatch({
        type: PROPERTY_LIST_SUCCESS,
        payload: data,
      });
    } catch (err) {
      handleError(err, dispatch, PROPERTY_LIST_FAIL);
    }
  };

export const getPropertyDetails = (id) => async (dispatch, getState) => {
  dispatch({
    type: PROPERTY_DETAILS_LOADING,
  });

  const {
    auth: { userToken },
  } = getState();

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const { data } = await axios.get(`${backend}/property/${id}`, config);
    dispatch({
      type: PROPERTY_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (err) {
    handleError(err, dispatch, PROPERTY_DETAILS_FAIL);
  }
};

export const clearMessage = () => (dispatch) => {
  dispatch({
    type: PROPERTY_CLEAR_MESSAGE,
  });
};
