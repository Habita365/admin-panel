import { combineReducers } from "redux";
import authReducer from "./auth/reducer";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { firebaseReducer } from "react-redux-firebase";
import { userListReducer, userDetailsReducer } from "./user/reducer";
import { realtorListReducer } from "./realtor/reducer";
import { personListReducer, personDetailsReducer } from "./person/reducer";
import {
  contractListReducer,
  contractDetailsReducer,
} from "./contract/reducer";
import {
  propertyDetailsReducer,
  propertyListReducer,
} from "./property/reducer";
import { visitDetailsReducer, visitListReducer } from "./visit/reducer";
import { dealingDetailsReducer, dealingListReducer } from "./dealing/reducer";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["user"],
};
const rootReducer = combineReducers({
  auth: authReducer,
  firebase: firebaseReducer,
  userList: userListReducer,
  userDetails: userDetailsReducer,
  propertyDetails: propertyDetailsReducer,
  propertyList: propertyListReducer,
  personList: personListReducer,
  personDetails: personDetailsReducer,
  contractList: contractListReducer,
  contractDetails: contractDetailsReducer,
  dealingList: dealingListReducer,
  dealingDetails: dealingDetailsReducer,
  visitList: visitListReducer,
  realtorList: realtorListReducer,
  visitDetails: visitDetailsReducer,
});

export default persistReducer(persistConfig, rootReducer);
