import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
} from "./types";
const userInfoFromStorage = localStorage.getItem("token")
  ? localStorage.getItem("token")
  : null;

const INITIAL_STATE = {
  currentUser: null,
  error: null,
  userToken: userInfoFromStorage,
};
const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return { loading: true };
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        // currentUser: action.payload,
        userToken: action.userToken,
        error: null,
      };
    case USER_LOGIN_FAIL:
      return { loading: false, error: action.payload };
    case USER_LOGOUT:
      return {};
    default:
      return state;
  }
};
export default userReducer;
