import { USER_LOGIN_FAIL, USER_LOGIN_REQUEST, USER_LOGOUT } from "./types";

export const login =
  (data) =>
  async (dispatch, getState, { getFirebase }) => {
    let fb = getFirebase();
    try {
      dispatch({
        type: USER_LOGIN_REQUEST,
      });
      await fb.auth().signInWithEmailAndPassword(data.email, data.password);

      const pathname = localStorage.getItem("pathname");

      if (pathname) {
        localStorage.removeItem("pathname");
        window.location.href = pathname;
      } else {
        window.location.href = "/";
      }
    } catch (err) {
      // error need to be dispatch
      dispatch({
        type: USER_LOGIN_FAIL,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      });
    }
  };

export const logout =
  () =>
  async (dispatch, getState, { getFirebase }) => {
    let fb = getFirebase();
    try {
      localStorage.removeItem("token");
      dispatch({ type: USER_LOGOUT });
      const pathname = window.location.pathname;

      if (pathname !== "/login") {
        localStorage.setItem("pathname", window.location.pathname);
      }
      await fb.auth().signOut();
    } catch (err) {
      console.log("Sign out error");
    }
  };
