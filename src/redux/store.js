import { createStore, applyMiddleware, compose } from "redux";
import { persistStore } from "redux-persist";
import thunk from "redux-thunk";
import { getFirebase } from "react-redux-firebase";
// import thunk from 'redux-thunk'
// import createSagaMiddleware from 'redux-saga'

// import rootSaga from './root.sagas'

import rootReducer from "./root-reducer";

// const sagaMiddleware = createSagaMiddleware()

const middleware = [thunk.withExtraArgument({ getFirebase })];

// if( process.env.NODE_ENV === 'development'){
//     middleware.push(logger)
// }

export const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(...middleware)
    // reactReduxFirebase(firebaseConfig)
  )
);

// sagaMiddleware.run(rootSaga)

export const persistor = persistStore(store);
