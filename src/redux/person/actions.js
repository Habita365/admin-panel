import axios from "axios";
import {
  PERSON_CLEAR_MESSAGE,
  PERSON_DELETE,
  PERSON_DETAILS_FAIL,
  PERSON_DETAILS_REQUEST,
  PERSON_DETAILS_SUCCESS,
  PERSON_FAIL,
  PERSON_LIST_FAIL,
  PERSON_LIST_REQUEST,
  PERSON_LIST_SUCCESS,
} from "./types";
import handleError from "utils/handleError";

const backend = process.env.REACT_APP_APP_HABITA_CONTRACT_ENDPOINT;

export const listPersons =
  ({ page, limit, sortModel }) =>
  async (dispatch, getState) => {
    dispatch({ type: PERSON_LIST_REQUEST });

    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const sortedColumn = sortModel?.[0];

      const { field, sort } = sortedColumn || {};

      const baseUrl = `${backend}/person?Page=${page || 1}&Limit=${
        limit || 15
      }`;

      const { data } = await axios.get(
        sortedColumn
          ? `${baseUrl}&Column=${field}&Direction=${sort.toUpperCase()}`
          : `${baseUrl}`,
        config
      );

      dispatch({
        type: PERSON_LIST_SUCCESS,
        payload: data,
      });
    } catch (err) {
      handleError(err, dispatch, PERSON_LIST_FAIL);
    }
  };

export const deletePerson =
  (propertyId, loading) => async (dispatch, getState) => {
    loading();
    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      await axios.delete(`${backend}/person/${propertyId}`, config);

      dispatch({
        type: PERSON_DELETE,
        payload: propertyId,
      });
    } catch (err) {
      handleError(err, dispatch, PERSON_FAIL);
    }
    loading();
  };

export const getPersonDetails = (id) => async (dispatch, getState) => {
  dispatch({
    type: PERSON_DETAILS_REQUEST,
  });

  const {
    auth: { userToken },
  } = getState();

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const { data } = await axios.get(`${backend}/person/${id}`, config);
    dispatch({
      type: PERSON_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (err) {
    handleError(err, dispatch, PERSON_DETAILS_FAIL);
  }
};

export const clearMessage = () => (dispatch) => {
  dispatch({
    type: PERSON_CLEAR_MESSAGE,
  });
};
