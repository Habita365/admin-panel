import {
  PERSON_LIST_REQUEST,
  PERSON_LIST_FAIL,
  PERSON_LIST_SUCCESS,
  PERSON_CLEAR_MESSAGE,
  PERSON_DELETE,
  PERSON_FAIL,
  PERSON_DETAILS_FAIL,
  PERSON_DETAILS_REQUEST,
  PERSON_DETAILS_SUCCESS,
} from "./types";

const initialState = {
  loading: true,
  Persons: [],
  TotalPersons: 0,
  personMessage: "",
};

export const personListReducer = (state = initialState, action) => {
  switch (action.type) {
    case PERSON_LIST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case PERSON_LIST_SUCCESS:
      let TotalPersons = action.payload?.TotalPersons;

      let Persons = action.payload?.Persons;
      let PersonsMapped = [];

      if (Array.isArray(Persons)) {
        PersonsMapped = Persons.map((person) => {
          return {
            id: person?.Id,
            ...person,
          };
        });
      }

      return {
        loading: false,
        Persons: PersonsMapped,
        TotalPersons,
      };
    case PERSON_LIST_FAIL:
      return {
        loading: false,
        error: action.payload,
        Persons: [],
        TotalPersons: 0,
      };
    case PERSON_DELETE: {
      const personId = action.payload;

      const remainingPersons = state.Persons?.filter((person) => {
        return personId !== person.id;
      });

      return {
        ...state,
        Persons: remainingPersons,
        personMessage: "Person successfully deleted",
      };
    }
    case PERSON_FAIL: {
      return {
        ...state,
        personMessage: action.payload,
      };
    }
    case PERSON_CLEAR_MESSAGE: {
      return {
        ...state,
        personMessage: "",
      };
    }
    default:
      return state;
  }
};

const defaultState = {
  loading: true,
  Person: null,
};

export const personDetailsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case PERSON_DETAILS_SUCCESS:
      const { Person } = action.payload;
      return {
        loading: false,
        Person,
      };
    case PERSON_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case PERSON_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
        Person: null,
      };
    default:
      return state;
  }
};
