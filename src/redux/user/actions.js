import axios from "axios";
import {
  USER_DETAILS_FAIL,
  USER_DETAILS_REQUEST,
  USER_DETAILS_SUCCESS,
  USER_LIST_FAIL,
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
} from "./types";
import handleError from "utils/handleError";

const backend = process.env.REACT_APP_APP_HABITA_USER_ENDPOINT;

export const listUsers = (filter) => async (dispatch, getState) => {
  const { page, limit, sortModel } = filter || {};

  const {
    auth: { userToken },
  } = getState();

  dispatch({
    type: USER_LIST_REQUEST,
  });

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const sortedColumn = sortModel?.[0];

    const { field, sort } = sortedColumn || {};

    const baseUrl = `${backend}/user?Page=${page || 1}&Limit=${limit || 15}`;

    const filterUrl = sortedColumn
      ? `${baseUrl}&Column=${field}&Direction=${sort.toUpperCase()}`
      : `${baseUrl}`;

    const { data } = await axios.get(
      filter ? filterUrl : `${backend}/user?Limit=15`,
      config
    );

    dispatch({
      type: USER_LIST_SUCCESS,
      payload: data,
    });
  } catch (err) {
    handleError(err, dispatch, USER_LIST_FAIL);
  }
};

export const getUserDetails = (id) => async (dispatch, getState) => {
  dispatch({
    type: USER_DETAILS_REQUEST,
  });

  const {
    auth: { userToken },
  } = getState();

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const { data } = await axios.get(`${backend}/user/${id}`, config);
    dispatch({
      type: USER_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (err) {
    handleError(err, dispatch, USER_DETAILS_FAIL);
  }
};
