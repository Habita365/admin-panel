import {
  USER_DETAILS_FAIL,
  USER_DETAILS_REQUEST,
  USER_DETAILS_RESET,
  USER_DETAILS_SUCCESS,
  USER_LIST_FAIL,
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
} from "./types";

const listDefaultState = {
  users: [],
  loading: true,
  TotalUsers: 0,
  error: "",
};

export const userListReducer = (state = listDefaultState, action) => {
  switch (action.type) {
    case USER_LIST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case USER_LIST_SUCCESS:
      let users = action.payload?.Users;
      let TotalUsers = action.payload?.TotalUsers;
      users &&
        users.forEach(function (data) {
          data["id"] = data["Id"];
          data.DocumentCpf = data.Document?.Cpf;
          delete data["Id"];
        });
      return {
        error: "",
        loading: false,
        users: users,
        TotalUsers,
      };
    case USER_LIST_FAIL:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

const defaultState = {
  loading: true,
  User: null,
};

export const userDetailsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case USER_DETAILS_REQUEST:
      return { ...state, loading: true };
    case USER_DETAILS_SUCCESS: {
      const { User } = action.payload;
      return { loading: false, User };
    }
    case USER_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
        User: null,
      };
    case USER_DETAILS_RESET:
      return {
        loading: true,
        User: null,
      };
    default:
      return state;
  }
};
