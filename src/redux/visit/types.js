export const VISIT_LIST_REQUEST = "VISIT_LIST_REQUEST";
export const VISIT_LIST_SUCCESS = "VISIT_LIST_SUCCESS";
export const VISIT_LIST_FAIL = "VISIT_LIST_FAIL";
export const VISIT_DETAILS_SUCCESS = "VISIT_DETAILS_SUCCESS";
export const VISIT_DETAILS_FAIL = "VISIT_DETAILS_FAIL";
export const VISIT_CHANGE_REALTOR = "VISIT_CHANGE_REALTOR";
export const VISIT_CHANGE_STATUS = "VISIT_CHANGE_STATUS";
export const VISIT_CLEAR_MESSAGE = "VISIT_CLEAR_MESSAGE";
export const VISIT_FAIL = "VISIT_FAIL";
