import {
  VISIT_LIST_REQUEST,
  VISIT_LIST_SUCCESS,
  VISIT_LIST_FAIL,
  VISIT_DETAILS_SUCCESS,
  VISIT_DETAILS_FAIL,
  VISIT_CHANGE_REALTOR,
  VISIT_FAIL,
  VISIT_CLEAR_MESSAGE,
  VISIT_CHANGE_STATUS,
} from "./types";

const listDefaultState = {
  Visits: [],
  loading: true,
};

export const visitListReducer = (state = listDefaultState, action) => {
  switch (action.type) {
    case VISIT_LIST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case VISIT_LIST_SUCCESS:
      let TotalVisits = action.payload?.TotalVisits;

      let Visits = action.payload?.Visits;
      let VisitsMapped = [];
      if (Array.isArray(Visits)) {
        VisitsMapped = Visits.map((visit) => {
          return { id: visit?.Id, ...visit };
        });
      }

      return {
        loading: false,
        Visits: VisitsMapped,
        TotalVisits,
      };
    case VISIT_CHANGE_REALTOR: {
      const { visitId, realtorId, message } = action.payload;

      const Visits = state.Visits?.map((visit) => {
        if (visit.id === visitId) {
          return {
            ...visit,
            RealtorId: realtorId,
          };
        }

        return visit;
      });

      return {
        ...state,
        Visits,
        visitMessage: message || "Realtor successfully added to visit",
        visitStatus: "SUCCESS",
      };
    }
    case VISIT_CHANGE_STATUS: {
      const { visitId, status, message } = action.payload;

      const Visits = state.Visits?.map((visit) => {
        if (visit.id === visitId) {
          return {
            ...visit,
            Status: status,
          };
        }

        return visit;
      });

      return {
        ...state,
        Visits,
        visitMessage: message || "Visit status successfully added",
        visitStatus: "SUCCESS",
      };
    }
    case VISIT_CLEAR_MESSAGE: {
      return {
        ...state,
        visitMessage: "",
        visitStatus: "",
      };
    }
    case VISIT_FAIL: {
      return {
        ...state,
        visitMessage: action.payload,
        visitStatus: "FAILED",
      };
    }
    case VISIT_LIST_FAIL:
      return {
        loading: false,
        error: action.payload,
        Visits: [],
        TotalVisits: 0,
      };
    default:
      return state;
  }
};

const visitDefaultState = {
  visit: {},
  property: {},
  ownerUser: {},
  user: {},

  loading: true,
};
export const visitDetailsReducer = (state = visitDefaultState, action) => {
  switch (action.type) {
    case VISIT_DETAILS_SUCCESS: {
      const { payload, payloadProperty, payloadOwnerUser, payloadUser } =
        action;

      if (payload) {
        return {
          ...state,
          visit: payload.Visit,
        };
      } else if (payloadProperty) {
        return {
          ...state,
          property: payloadProperty.Property,
        };
      } else if (payloadOwnerUser) {
        return {
          ...state,
          loading: false,
          ownerUser: payloadOwnerUser.User,
        };
      } else if (payloadUser) {
        return {
          ...state,
          user: payloadUser.User,
        };
      }
      break;
    }
    case VISIT_DETAILS_FAIL: {
      return {
        loading: false,
        error: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
