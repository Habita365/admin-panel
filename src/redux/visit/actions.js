import axios from "axios";
import {
  VISIT_CHANGE_REALTOR,
  VISIT_CHANGE_STATUS,
  VISIT_CLEAR_MESSAGE,
  VISIT_DETAILS_FAIL,
  VISIT_DETAILS_SUCCESS,
  VISIT_FAIL,
  VISIT_LIST_FAIL,
  VISIT_LIST_REQUEST,
  VISIT_LIST_SUCCESS,
} from "./types";
import handleError from "utils/handleError";

const backend = process.env.REACT_APP_APP_HABITA_VISIT_ENDPOINT;

export const changeVisitStatus =
  (visitId, status, loading) => async (dispatch, getState) => {
    loading();
    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const res = await axios.patch(
        `${backend}/visit/${visitId}/status`,
        {
          Status: status,
        },
        config
      );

      dispatch({
        type: VISIT_CHANGE_STATUS,
        payload: {
          visitId,
          status,
          message: res.data?.message,
        },
      });
    } catch (err) {
      handleError(err, dispatch, VISIT_FAIL);
    }
    loading();
  };

export const changeVisitRealtor =
  (visitId, realtorId, loading) => async (dispatch, getState) => {
    loading();
    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const res = await axios.patch(
        `${backend}/visit/${visitId}/realtor`,
        {
          RealtorId: realtorId,
        },
        config
      );

      dispatch({
        type: VISIT_CHANGE_REALTOR,
        payload: {
          visitId,
          realtorId,
          message: res.data?.message,
        },
      });
    } catch (err) {
      handleError(err, dispatch, VISIT_FAIL);
    }
    loading();
  };

export const listVisits =
  ({ page, limit, sortModel, IsPending } = {}) =>
  async (dispatch, getState) => {
    dispatch({
      type: VISIT_LIST_REQUEST,
    });

    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const sortedColumn = sortModel?.[0];

      const { field, sort } = sortedColumn || {};

      // const pendingStr = `&Status=["ASSIGNED","SCHEDULED"]`;
      const pendingStr = `&Status=ASSIGNED&Status=SCHEDULED`;

      const baseUrl = `${backend}/visit?Page=${page || 1}&Limit=${limit || 15}${
        IsPending ? pendingStr : ""
      }`;

      const { data } = await axios.get(
        sortedColumn
          ? `${baseUrl}&Column=${field}&Direction=${sort.toUpperCase()}`
          : `${baseUrl}`,
        config
      );

      dispatch({
        type: VISIT_LIST_SUCCESS,
        payload: data,
      });
    } catch (err) {
      handleError(err, dispatch, VISIT_LIST_FAIL);
    }
  };

export const getVisitDetails = (id) => async (dispatch, getState) => {
  const {
    auth: { userToken },
  } = getState();

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };
    const { data } = await axios.get(`${backend}/visit/${id}`, config);

    dispatch({
      type: VISIT_DETAILS_SUCCESS,
      payload: data,
    });

    const { data: dataProperty } = await axios.get(
      `${process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT}/property/${data?.Visit?.PropertyId}`,
      config
    );

    dispatch({
      type: VISIT_DETAILS_SUCCESS,
      payloadProperty: dataProperty,
    });

    const { data: dataUser } = await axios.get(
      `${process.env.REACT_APP_APP_HABITA_USER_ENDPOINT}/user/${data?.Visit?.UserId}`,
      config
    );

    dispatch({
      type: VISIT_DETAILS_SUCCESS,
      payloadUser: dataUser,
    });

    const { data: dataOwnerUser } = await axios.get(
      `${process.env.REACT_APP_APP_HABITA_USER_ENDPOINT}/user/${dataProperty?.Property?.UserId}`,
      config
    );

    dispatch({
      type: VISIT_DETAILS_SUCCESS,
      payloadOwnerUser: dataOwnerUser,
    });
  } catch (err) {
    handleError(err, dispatch, VISIT_DETAILS_FAIL);
  }
};

export const clearMessage = () => (dispatch) => {
  dispatch({
    type: VISIT_CLEAR_MESSAGE,
  });
};
