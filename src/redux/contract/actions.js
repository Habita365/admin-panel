import axios from "axios";
import handleError from "utils/handleError";
import {
  CONTRACT_DETAILS_FAIL,
  CONTRACT_DETAILS_REQUEST,
  CONTRACT_DETAILS_SUCCESS,
  CONTRACT_LIST_FAIL,
  CONTRACT_LIST_REQUEST,
  CONTRACT_LIST_SUCCESS,
} from "./types";

const backend = process.env.REACT_APP_APP_HABITA_CONTRACT_ENDPOINT;

export const listContracts =
  ({ page, limit, sortModel }) =>
  async (dispatch, getState) => {
    dispatch({ type: CONTRACT_LIST_REQUEST });

    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const sortedColumn = sortModel?.[0];

      const { field, sort } = sortedColumn || {};

      const baseUrl = `${backend}/contract?Page=${page || 1}&Limit=${
        limit || 15
      }`;

      const { data } = await axios.get(
        sortedColumn
          ? `${baseUrl}&Column=${field}&Direction=${sort.toUpperCase()}`
          : `${baseUrl}`,
        config
      );

      dispatch({
        type: CONTRACT_LIST_SUCCESS,
        payload: data,
      });
    } catch (err) {
      handleError(err, dispatch, CONTRACT_LIST_FAIL);
    }
  };

export const getContractDetails = (id) => async (dispatch, getState) => {
  dispatch({
    type: CONTRACT_DETAILS_REQUEST,
  });

  const {
    auth: { userToken },
  } = getState();

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const { data } = await axios.get(`${backend}/contract/${id}`, config);
    dispatch({
      type: CONTRACT_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (err) {
    handleError(err, dispatch, CONTRACT_DETAILS_FAIL);
  }
};
