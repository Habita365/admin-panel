import {
  CONTRACT_LIST_REQUEST,
  CONTRACT_LIST_FAIL,
  CONTRACT_LIST_SUCCESS,
  CONTRACT_DETAILS_FAIL,
  CONTRACT_DETAILS_REQUEST,
  CONTRACT_DETAILS_SUCCESS,
} from "./types";

const initialState = {
  loading: true,
  Contracts: [],
};

export const contractListReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONTRACT_LIST_REQUEST:
      return initialState;
    case CONTRACT_LIST_SUCCESS:
      let TotalContracts = action.payload?.TotalContracts;
      let Contracts = action.payload?.Contracts;
      let ContractsMapped = [];

      if (Array.isArray(Contracts)) {
        ContractsMapped = Contracts.map((contract) => {
          return {
            id: contract?.Id,
            ...contract,
          };
        });
      }

      return {
        loading: false,
        Contracts: ContractsMapped,
        TotalContracts,
      };
    case CONTRACT_LIST_FAIL:
      return {
        loading: false,
        error: action.payload,
        Contracts: [],
        TotalContracts: 0,
      };
    default:
      return state;
  }
};

const defaultState = {
  loading: true,
  Contract: null,
};

export const contractDetailsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case CONTRACT_DETAILS_SUCCESS:
      const { Contract } = action.payload;
      return {
        loading: false,
        Contract,
      };
    case CONTRACT_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case CONTRACT_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
        Contract: null,
      };
    default:
      return state;
  }
};
