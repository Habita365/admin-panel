import axios from "axios";
import handleError from "utils/handleError";
import {
  DEALING_DETAILS_FAIL,
  DEALING_DETAILS_REQUEST,
  DEALING_DETAILS_SUCCESS,
  DEALING_LIST_FAIL,
  DEALING_LIST_REQUEST,
  DEALING_LIST_SUCCESS,
} from "./types";

const backend = process.env.REACT_APP_APP_HABITA_DEALING_ENDPOINT;

export const listDealings =
  ({ page, limit, sortModel }) =>
  async (dispatch, getState) => {
    dispatch({ type: DEALING_LIST_REQUEST });

    const {
      auth: { userToken },
    } = getState();

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      };

      const sortedColumn = sortModel?.[0];

      const { field, sort } = sortedColumn || {};

      const baseUrl = `${backend}/dealing?Page=${page || 1}&Limit=${
        limit || 15
      }`;

      const { data } = await axios.get(
        sortedColumn
          ? `${baseUrl}&Column=${field}&Direction=${sort.toUpperCase()}`
          : `${baseUrl}`,
        config
      );

      dispatch({
        type: DEALING_LIST_SUCCESS,
        payload: data,
      });
    } catch (err) {
      handleError(err, dispatch, DEALING_LIST_FAIL);
    }
  };

export const getDealingDetails = (id) => async (dispatch, getState) => {
  dispatch({ type: DEALING_DETAILS_REQUEST });

  const {
    auth: { userToken },
  } = getState();

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const { data } = await axios.get(`${backend}/dealing/${id}`, config);

    dispatch({
      type: DEALING_DETAILS_SUCCESS,
      payload: data,
    });

    const { data: dataProperty } = await axios.get(
      `${process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT}/property/${data?.Dealing?.PropertyId}`,
      config
    );

    dispatch({
      type: DEALING_DETAILS_SUCCESS,
      payloadProperty: dataProperty,
    });

    const { data: dataUser } = await axios.get(
      `${process.env.REACT_APP_APP_HABITA_USER_ENDPOINT}/user/${data?.Dealing?.UserId}`,
      config
    );

    dispatch({
      type: DEALING_DETAILS_SUCCESS,
      payloadUser: dataUser,
    });

    const { data: dataOwnerUser } = await axios.get(
      `${process.env.REACT_APP_APP_HABITA_USER_ENDPOINT}/user/${dataProperty?.Property?.UserId}`,
      config
    );

    dispatch({
      type: DEALING_DETAILS_SUCCESS,
      payloadOwnerUser: dataOwnerUser,
    });
  } catch (err) {
    handleError(err, dispatch, DEALING_DETAILS_FAIL);
  }
};
