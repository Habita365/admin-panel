import {
  DEALING_DETAILS_FAIL,
  DEALING_DETAILS_REQUEST,
  DEALING_DETAILS_RESET,
  DEALING_DETAILS_SUCCESS,
  DEALING_LIST_FAIL,
  DEALING_LIST_REQUEST,
  DEALING_LIST_RESET,
  DEALING_LIST_SUCCESS,
} from "./types";

const initialState = {
  loading: true,
  Dealings: [],
  TotalDealings: 0,
};

export const dealingListReducer = (state = initialState, action) => {
  switch (action.type) {
    case DEALING_LIST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case DEALING_LIST_SUCCESS:
      let TotalDealings = action.payload?.TotalDealings;

      let Dealings = action.payload?.Dealings;
      let DealingsMapped = [];

      if (Array.isArray(Dealings)) {
        DealingsMapped = Dealings.map((dealing) => {
          return {
            id: dealing?.Id,
            ...dealing,
          };
        });
      }

      return {
        loading: false,
        Dealings: DealingsMapped,
        TotalDealings,
      };
    case DEALING_LIST_FAIL:
      return {
        loading: false,
        error: action.payload,
        Dealings: [],
        TotalDealings: 0,
      };
    case DEALING_LIST_RESET:
      return {
        ...state,
        loading: false,
        Dealings: [],
        TotalDealings: 0,
      };
    default:
      return state;
  }
};

const defaultState = {
  loading: true,
  dealing: {},
  property: {},
  ownerUser: {},
  user: {},
};

export const dealingDetailsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case DEALING_DETAILS_SUCCESS:
      const { payload, payloadProperty, payloadOwnerUser, payloadUser } =
        action;

      if (payload) {
        return {
          ...state,
          dealing: payload.Dealing,
        };
      } else if (payloadProperty) {
        return {
          ...state,
          property: payloadProperty.Property,
        };
      } else if (payloadOwnerUser) {
        return {
          ...state,
          loading: false,
          ownerUser: payloadOwnerUser.User,
        };
      } else if (payloadUser) {
        return {
          ...state,
          user: payloadUser.User,
        };
      }
      break;
    case DEALING_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case DEALING_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
        dealing: {},
        property: {},
        ownerUser: {},
        user: {},
      };
    case DEALING_DETAILS_RESET:
      return {
        loading: false,
        dealing: {},
        property: {},
        ownerUser: {},
        user: {},
      };

    default:
      return state;
  }
};
