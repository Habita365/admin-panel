import {
  REALTOR_USER_LIST_REQUEST,
  REALTOR_USER_LIST_FAIL,
  REALTOR_USER_LIST_SUCCESS,
} from "./types";

const initialState = { users: [], loading: true, TotalRealtors: 0 };

export const realtorListReducer = (state = initialState, action) => {
  switch (action.type) {
    case REALTOR_USER_LIST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case REALTOR_USER_LIST_SUCCESS:
      let users = action.payload.Realtors;
      let TotalRealtors = action.payload.TotalRealtors;
      users &&
        users.forEach(function (data) {
          data["id"] = data["Id"];
          delete data["Id"];
        });
      return {
        loading: false,
        TotalRealtors,
        users,
      };
    case REALTOR_USER_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
