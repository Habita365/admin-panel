import axios from "axios";
import {
  REALTOR_USER_LIST_FAIL,
  REALTOR_USER_LIST_REQUEST,
  REALTOR_USER_LIST_SUCCESS,
} from "./types";
import handleError from "utils/handleError";

const backend = process.env.REACT_APP_APP_HABITA_USER_ENDPOINT;

export const listUsers = (filter) => async (dispatch, getState) => {
  const { page, limit, sortModel } = filter || {};

  const {
    auth: { userToken },
  } = getState();

  dispatch({
    type: REALTOR_USER_LIST_REQUEST,
  });

  try {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const sortedColumn = sortModel?.[0];

    const { field, sort } = sortedColumn || {};

    const baseUrl = `${backend}/user/realtor?Page=${page || 1}&Limit=${
      limit || 300
    }`;

    const filterUrl = sortedColumn
      ? `${baseUrl}&Column=${field}&Direction=${sort.toUpperCase()}`
      : `${baseUrl}`;

    const { data } = await axios.get(
      filter ? filterUrl : `${backend}/user/realtor?Limit=15`,
      config
    );
    dispatch({
      type: REALTOR_USER_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    handleError(error, dispatch, REALTOR_USER_LIST_FAIL);
  }
};
