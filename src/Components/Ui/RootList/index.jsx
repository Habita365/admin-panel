import React from "react";
import AppBar from "@mui/material/AppBar";
import Tabs from "@mui/material/Tabs";
import Loader from "Components/Ui/Loader";

import { styled } from "@mui/material/styles";

const PREFIX = "EditTabs";

const classes = {
  root: `${PREFIX}-root`,
};

const Root = styled("div")(({ theme }) => ({
  [`&.${classes.root}`]: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const EditTabs = ({ children, loading, setSuccess, renderChildren }) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (_event, newValue) => {
    setValue(newValue);

    if (setSuccess) {
      setSuccess(false);
    }
  };

  return (
    <Root className={classes.root}>
      {loading ? (
        <Loader />
      ) : (
        <>
          <AppBar position="static">
            <Tabs
              variant="scrollable"
              indicatorColor="primary"
              textColor="inherit"
              scrollButtons="auto"
              value={value}
              onChange={handleChange}
              aria-label="List tabs"
            >
              {children}
            </Tabs>
          </AppBar>
          {renderChildren(value)}
        </>
      )}
    </Root>
  );
};

export default EditTabs;
