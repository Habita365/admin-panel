import React from "react";
import GridLoader from "react-spinners/GridLoader";
import { css } from "@emotion/react";
import { theme } from "../../../MuiTheme";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

const Loader = () => {
  return (
    <div
      style={{
        height: "80vh",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <GridLoader
        loading={true}
        css={override}
        size={50}
        color={theme.palette.primary.main}
      />
    </div>
  );
};

export default Loader;
