import { styled } from "@mui/material/styles";

import LinearProgress from "@mui/material/LinearProgress";

const PREFIX = "LinearProgressWithLabel";

const classes = {
  root: `${PREFIX}-root`,
};

const Root = styled("div")({
  [`& .${classes.root}`]: {
    height: 2,
  },
});

const styles = {
  minWidth: 30,
};

const LinearProgressWithLabel = ({ value }) => {
  return (
    <Root className="flex items-center">
      <div className="w-full mr-1">
        <LinearProgress
          variant="determinate"
          value={value}
          className={classes.root}
        />
      </div>
      <div style={styles}>
        <p className="text-xs text-gray-500">{`${Math.round(value)}%`}</p>
      </div>
    </Root>
  );
};

export default LinearProgressWithLabel;
