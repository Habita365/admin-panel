import {
  Avatar,
  Box,
  Card,
  CardContent,
  Link,
  Typography,
} from "@mui/material";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import React from "react";

export default function ShortcutCard({ image, label, url, description }) {
  return (
    <Card>
      <Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <CardContent>
          <Avatar src={image} />
        </CardContent>
        <CardContent>
          <Link target="_blank" href={url} underline="hover">
            <Typography variant="h5" component="h2">
              {label}
              {url && <ChevronRightIcon />}
            </Typography>
          </Link>
          <Typography color="textSecondary">{description}</Typography>
        </CardContent>
      </Box>
    </Card>
  );
}
