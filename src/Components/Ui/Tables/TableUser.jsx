import React from "react";
import Table from "@mui/material/Table";
import LinearProgress from "Components/Ui/Loader/LinearProgressWithLabel";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Avatar from "@mui/material/Avatar";
import Paper from "@mui/material/Paper";

import { addressTextTranform } from "utils/dataTransform";
import { randAvatarColors } from "utils/uiUtils";

export const Row = ({
  noExtras = false,
  Name,
  Cpf,
  PhoneNumber,
  AddressText,
  Address: A,
  Percentage,
  // PersonId,
}) => {
  const [avatarColor] = React.useState(randAvatarColors());

  const addressText = addressTextTranform(
    [A?.Address ?? "", A?.StreetNumber ?? "", A?.Neighbourhood ?? ""],
    true
  );

  const { color, bgColor: backgroundColor } = avatarColor;

  return (
    <TableRow>
      <TableCell>
        <div className="flex items-center">
          <Avatar
            alt={Name}
            style={{
              backgroundColor,
              color,
            }}
          >
            {Name[0].toUpperCase()}
          </Avatar>
          <div className="ml-2 w-28">
            <p className="text-sm font-semibold w-full truncate m-0 5">
              {Name}
            </p>
            <p className="text-sm">{Cpf}</p>
            {!noExtras && Percentage ? (
              <LinearProgress value={Number(Percentage)} />
            ) : null}
          </div>
        </div>
      </TableCell>
      {noExtras || !PhoneNumber ? null : (
        <TableCell>
          <p className="text-sm">{Cpf}</p>
        </TableCell>
      )}
      {noExtras ? null : (
        <TableCell>
          <p className="text-sm">{A ? addressText : AddressText}</p>
        </TableCell>
      )}
    </TableRow>
  );
};

const TableUser = ({ children, ...props }) => {
  return (
    <TableContainer component={Paper}>
      <Table {...props}>
        <TableHead>
          <TableRow>
            <TableCell>Nome e CPF</TableCell>
            <TableCell>Telefone</TableCell>
            <TableCell>Endereço</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>{children}</TableBody>
      </Table>
    </TableContainer>
  );
};

export default TableUser;
