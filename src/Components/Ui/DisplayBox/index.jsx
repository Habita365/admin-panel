import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

const DisplayBox = ({ label, content }) => {
  return (
    <Box my={1}>
      <TextField
        id="filled-read-only-input"
        label={label}
        defaultValue={content ? content : null}
        InputProps={{
          readOnly: true,
        }}
        variant="filled"
      />
    </Box>
  );
};

export default DisplayBox;
