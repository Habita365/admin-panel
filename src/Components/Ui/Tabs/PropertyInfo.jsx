import React from "react";
import TextDisplayField from "Components/Ui/Forms/TextDisplayField";

import SelectDisplayField from "Components/Ui/Forms/SelectDisplayField";
import { keyStorageOptions } from "Components/Views/Properties/Tabs/options";
import { MenuItem } from "@mui/material";
import { lockTypeOptions } from "Components/Views/Properties/Tabs/options";
import OpenLinkButton from "Components/Ui/Buttons/OpenLinkButton";

const PropertyInfo = ({ property }) => {
  const address = property?.Address;

  return (
    <div className="w-full lg:w-1/2">
      <div className="mb-4">Endereço </div>

      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField label="Estado" name="State" content={address.State} />
        <TextDisplayField label="Cidade" name="City" content={address.City} />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField
          label="Bairro"
          name="Neighbourhood"
          content={address.Neighbourhood}
        />
        <TextDisplayField
          label="Endereço"
          name="Address"
          content={address.Address}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField
          label="CEP"
          name="PostalCode"
          content={address.PostalCode}
        />
        <TextDisplayField
          label="Número"
          name="StreetNumber"
          content={address.StreetNumber}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField
          label="Andar"
          name="FloorNumber"
          content={address.FloorNumber}
        />
        <TextDisplayField
          label="Complemento"
          name="Compl"
          content={address.Compl}
        />
      </div>
      <div className="mb-4">Entrada no imóvel</div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectDisplayField
          name="LockType"
          label="Tipo de entrada"
          value={property.LockType}
        >
          {lockTypeOptions.map(({ text, value }) => (
            <MenuItem value={value} key={value}>
              {text}
            </MenuItem>
          ))}
        </SelectDisplayField>
        <SelectDisplayField
          name="KeyStorage"
          label="Local da chave"
          value={property.KeyStorage}
        >
          {keyStorageOptions.map(({ text, value }) => (
            <MenuItem value={value} key={value}>
              {text}
            </MenuItem>
          ))}
        </SelectDisplayField>
      </div>
      <OpenLinkButton to={`/properties/${property.Id}/edit`}>
        Abrir imóvel
      </OpenLinkButton>
    </div>
  );
};

export default PropertyInfo;
