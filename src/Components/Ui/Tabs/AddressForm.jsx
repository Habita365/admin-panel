import React, { useEffect, useState } from "react";
import AutocompleteInput from "Components/Ui/Forms/AutocompleteInput";
import { geocodeByAddress } from "react-places-autocomplete";
import TextFieldWrapper from "Components/Ui/Forms/TextField";
import MaskTextField from "Components/Ui/Forms/MaskTextField";
import fillInAddress from "utils/fillInAddress";

import Button from "@mui/material/Button";

import * as yup from "yup";

const AddressForm = ({
  watch,
  setValue,
  control,
  loading,
  userMode = false,
}) => {
  const [address, setAddress] = useState("");
  const [addressText, setAddressText] = useState("");
  const [showButton, setShowButton] = useState(false);
  const [message, setMessage] = useState("");
  const [invalid, setInvalid] = useState(false);

  const StreetNumber = watch("Address.StreetNumber");
  const Address = watch("Address.Address");
  const Neighbourhood = watch("Address.Neighbourhood");
  const PostalCode = watch("Address.PostalCode");
  const City = watch("Address.City");
  const State = watch("Address.State");
  const Compl = watch("Address.Compl");

  const handleSelect = async (addr) => {
    setInvalid(false);
    setMessage("");
    try {
      const results = await geocodeByAddress(addr);
      const result = results[0];

      if (result) {
        const addressObj = fillInAddress(result.address_components);

        if (addressObj.city === "São Paulo") {
          for (const key in addressObj) {
            switch (key) {
              case "address":
                setAddress(addressObj[key]);
                break;
              case "streetNumber":
                setValue("Address.StreetNumber", addressObj[key]);
                break;
              case "postalCode":
                setValue("Address.PostalCode", addressObj[key]);
                break;
              default:
                break;
            }

            if (!userMode) {
              switch (key) {
                case "city":
                  setValue("Address.City", addressObj[key]);
                  break;
                case "address":
                  setValue("Address.Address", addressObj[key]);
                  break;
                case "neighbourhood":
                  setValue("Address.Neighbourhood", addressObj[key]);
                  break;
                case "State":
                  setValue("Address.State", addressObj[key]);
                  break;
                default:
                  break;
              }
            }
          }
        } else {
          setMessage("Atualmente, apenas casas em São Paulo são suportadas");
          setInvalid(true);
        }
      } else {
        setInvalid(true);

        setValue("Address.StreetNumber", "");
        setValue("Address.PostalCode", "");

        if (!userMode) {
          setValue("Address.City", "");
          setValue("Address.Address", "");
          setValue("Address.State", "");
          setValue("Address.Neighbourhood", "");
        }
      }
    } catch (err) {
      if (err) {
        console.error("Error", err);
        setMessage("Um erro ocorreu");
      }
    }
  };

  useEffect(() => {
    if (!addressText && !userMode) {
      if (Address && !StreetNumber) {
        setAddressText(`${Address}, número`);
      } else if (Address && StreetNumber) {
        setAddressText(`${Address}, ${StreetNumber}`);
      }
    }
  }, [userMode, Address, StreetNumber, addressText]);

  useEffect(() => {
    const userSchemaObj = {
      PostalCode: yup.string().required(),
      StreetNumber: yup.string().required(),
      Compl: yup.string().required(),
    };
    const userSchema = yup.object().shape(userSchemaObj);

    const schema = userMode
      ? userSchema
      : yup.object().shape({
          ...userSchemaObj,
          City: yup.string().required(),
          State: yup.string().required(),
          Neighbourhood: yup.string().required(),
          Address: yup.string().required(),
        });

    if (address && !StreetNumber) {
      setAddressText(`${address}, número`);
    } else if (address && StreetNumber) {
      setAddressText(`${address}, ${StreetNumber}`);
    }

    const userObj = {
      PostalCode,
      StreetNumber,
      Compl,
    };

    const obj = {
      ...userObj,
      Neighbourhood,
      Address,
      City,
      State,
    };

    schema
      .isValid({
        ...obj,
      })
      .then((valid) => {
        setShowButton(valid);
      });
  }, [
    Neighbourhood,
    PostalCode,
    Compl,
    Address,
    address,
    StreetNumber,
    City,
    State,
    userMode,
  ]);

  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 relative">
        <AutocompleteInput
          address={addressText}
          onChange={(addr) => setAddressText(addr)}
          onSelect={handleSelect}
        >
          {(getInputProps) => {
            let getInputPropsRes;

            if (getInputProps) {
              getInputPropsRes = getInputProps({
                placeholder: "Endereço com número",
              });
            }

            return (
              <TextFieldWrapper
                type="text"
                variant="outlined"
                className="w-full"
                {...getInputPropsRes}
              />
            );
          }}
        </AutocompleteInput>
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        {!userMode && (
          <TextFieldWrapper
            label="Bairro"
            name="Address.Neighbourhood"
            placeholder="Moema"
            control={control}
          />
        )}
        <MaskTextField
          control={control}
          name="Address.PostalCode"
          mask="99999-999"
          label="CEP"
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextFieldWrapper
          label="Número da rua"
          name="Address.StreetNumber"
          control={control}
          InputLabelProps={{ shrink: true }}
        />
        <TextFieldWrapper
          label="Complemento"
          name="Address.Compl"
          control={control}
        />
      </div>
      {!userMode && (
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextFieldWrapper
            label="Cidade"
            name="Address.City"
            control={control}
          />
          <TextFieldWrapper
            label="Estado"
            name="Address.State"
            control={control}
          />
        </div>
      )}
      <p
        className={`text-lg my-4 font-semibold${
          !invalid ? "" : " text-red-400"
        }`}
      >
        {message && message}
      </p>
      {showButton && (
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      )}
    </div>
  );
};

export default AddressForm;
