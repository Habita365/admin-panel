import React from "react";
import TextDisplayField from "Components/Ui/Forms/TextDisplayField";
import OpenLinkButton from "Components/Ui/Buttons/OpenLinkButton";

const User = ({ user }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField label="Nome" name="Name" content={user.Name} />
        <TextDisplayField label="Email" name="Email" content={user.Email} />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField
          label="Telefone"
          name="PhoneNumber"
          content={user.PhoneNumber}
        />
      </div>
      <OpenLinkButton to={`/users/${user.Id}/edit`}>
        Abrir usuário
      </OpenLinkButton>
    </div>
  );
};

export default User;
