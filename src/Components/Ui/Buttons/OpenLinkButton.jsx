import OpenInNew from "@mui/icons-material/OpenInNew";
import { Button } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

export default function OpenLinkButton(props) {
  return (
    <Button
      variant="outlined"
      component={Link}
      to={props.to}
      target="_blank"
      endIcon={<OpenInNew />}
    >
      {props.children}
    </Button>
  );
}
