import React from "react";
import Tab from "@mui/material/Tab";

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  const { value, ...rest } = props;
  return (
    <Tab
      onClick={(event) => {
        event.preventDefault();
      }}
      {...a11yProps(value)}
      {...rest}
      value={value}
    />
  );
}

export default LinkTab;
