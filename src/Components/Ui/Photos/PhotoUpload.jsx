import Button from "@mui/material/Button";
import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import axios from "axios";
import { getPropertyDetails } from "redux/property/actions";
import { useDispatch } from "react-redux";

const dz = {
  width: "300px",
  height: "200px",
  border: "1px solid #c4c4c4",
  borderRadius: "8px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
};

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

export default function PhotoUpload({ record }) {
  const dispatch = useDispatch();

  const [files, setFiles] = useState([]);
  const [loading, setLoading] = useState(false);
  const propertyAddress = record?.Address?.Address;
  const propertyId = record.Id;

  const onDrop = (acceptedFiles) => {
    setFiles(
      [...acceptedFiles].map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      )
    );
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/jpeg",
  });

  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name} onClick={() => removeFile(file)}>
      <div style={thumbInner}>
        <img src={file.preview} alt="" style={img} />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  async function handleSubmit(e) {
    e.preventDefault();

    await Promise.all(
      files.map(async (file) => {
        await uploadFile(file);
      })
    );

    setTimeout(() => {
      dispatch(getPropertyDetails(propertyId));
    }, 6000);
  }

  function removeFile(file) {
    setFiles([...files].filter((f) => f.name !== file.name));
  }

  async function uploadFile(file) {
    setLoading(true);
    const options = {
      ContentType: "image/jpeg",
      Extension: "jpeg",
      CaptureDate: new Date(),
    };

    const getSignedURLRes = await fetch(
      `${process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT}/property/${propertyId}/media/initialize-upload`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(options),
      }
    );

    const getSignedURLBody = await getSignedURLRes.json();
    const signedURL = getSignedURLBody.SignedUrl;

    try {
      await axios.put(signedURL, file, {
        headers: {
          "Content-Type": "image/jpeg",
        },
      });

      removeFile(file);
    } catch (err) {
      console.log(err);
    }
    setLoading(false);
  }

  return (
    <div>
      <h3>{propertyAddress}</h3>
      <section className="container">
        <div style={dz} {...getRootProps({})}>
          <input {...getInputProps()} />
          <p>Adicionar novas imagens</p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
      <Button
        variant="contained"
        disabled={files.length <= 0 || loading}
        onClick={handleSubmit}
      >
        Upload
      </Button>
    </div>
  );
}
