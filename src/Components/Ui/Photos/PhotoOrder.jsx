import React, { useEffect, useState } from "react";
import {
  closestCenter,
  DndContext,
  DragOverlay,
  MouseSensor,
  TouchSensor,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import {
  arrayMove,
  rectSortingStrategy,
  SortableContext,
} from "@dnd-kit/sortable";

import { SortablePhoto } from "./SortablePhoto";
import { Photo } from "./Photo";
import { Button } from "@mui/material";

function Grid({ children }) {
  return (
    <div
      style={{
        display: "grid",
        gridTemplateColumns: `repeat(auto-fit, 300px)`,
        gridGap: 10,
        backgroundColor: "WhiteSmoke",
        padding: 10,
      }}
    >
      {children}
    </div>
  );
}

const PhotoOrder = ({ record }) => {
  const propertyId = record.Id;

  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [activePhoto, setActivePhoto] = useState(null);

  useEffect(() => {
    const photos = [...record.Medias]
      .map((photo) => {
        return { ...photo, id: String(photo.Id) };
      })
      .sort((a, b) => {
        return a.Priority - b.Priority;
      });

    setItems(photos);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [record?.Medias?.length]);

  const sensors = useSensors(useSensor(MouseSensor), useSensor(TouchSensor));

  return (
    <>
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
        onDragCancel={handleDragCancel}
      >
        <SortableContext items={items} strategy={rectSortingStrategy}>
          <Grid>
            {items.map((photo, index) => (
              <SortablePhoto
                key={photo.id}
                id={photo.id}
                url={photo.ThumbPath}
                index={index}
                deleteMedia={handleDelete}
              />
            ))}
          </Grid>
        </SortableContext>

        <DragOverlay adjustScale={true}>
          {activePhoto?.id ? (
            <Photo
              url={activePhoto.ThumbPath}
              index={items.indexOf(activePhoto)}
            />
          ) : null}
        </DragOverlay>
      </DndContext>
      <Button
        variant="contained"
        disabled={loading}
        onClick={updateMediaPriority}
      >
        Atualizar Ordem
      </Button>
    </>
  );

  function handleDragStart(event) {
    setActivePhoto(items.find((p) => p.id === event.active.id));
    console.log(event.active);
  }

  function handleDragEnd(event) {
    const { active, over } = event;
    console.log(event);
    if (active.id !== over.id) {
      setItems((items) => {
        const oldIndex = items.findIndex((p) => p.id === active.id);
        const newIndex = items.findIndex((p) => p.id === over.id);
        console.log(oldIndex, newIndex);
        return arrayMove(items, oldIndex, newIndex);
      });
    }

    setActivePhoto(null);
  }

  function handleDragCancel() {
    setActivePhoto(null);
  }

  async function deleteMedia(mediaId) {
    return await fetch(
      `${process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT}/property/${propertyId}/media/${mediaId}`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
  }

  async function handleDelete(mediaId) {
    if (window.confirm("Deletar imagem?")) {
      deleteMedia(mediaId).then((res) => {
        if (res.ok) {
          setItems([...items].filter((item) => item.id !== mediaId));
        }
      });
    }
  }

  async function updateMediaPriority() {
    setLoading(true);
    const priorityList = items.map((photo, index) => {
      return { Priority: index + 1, MediaId: photo.Id };
    });
    await fetch(
      `${process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT}/property/${propertyId}/media/priority`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({ MediaPriorties: priorityList }),
      }
    );
    setLoading(false);
  }
};

export default PhotoOrder;
