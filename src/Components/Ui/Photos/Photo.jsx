import { IconButton } from "@mui/material";
import React, { forwardRef } from "react";
import DeleteForeverOutlinedIcon from "@mui/icons-material/DeleteForeverOutlined";

export const Photo = forwardRef(
  ({ url, index, faded, style, onDelete, ...props }, ref) => {
    const inlineStyles = {
      opacity: faded ? "0.2" : "1",
      transformOrigin: "0 0",
      height: 200,
      width: 300,
      backgroundImage: `url("${url}")`,
      backgroundSize: "contain",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundColor: "black",
      ...style,
    };
    const iconButtonStyle = {
      position: "absolute",
      top: 0,
      right: 0,
    };

    return (
      <div style={{ position: "relative" }}>
        <IconButton style={iconButtonStyle} onClick={onDelete} size="large">
          <DeleteForeverOutlinedIcon style={{ color: "#F44336" }} />
        </IconButton>
        <div ref={ref} style={inlineStyles} {...props}></div>
      </div>
    );
  }
);
