import React from "react";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { Controller } from "react-hook-form";

const FormControlCheckbox = ({ label, datas, control, children }) => {
  return (
    <FormControl className="w-full" component="fieldset">
      {label && <FormLabel>{label}</FormLabel>}
      <FormGroup style={{ flexDirection: "row" }}>
        {datas.map(({ name, text }) => (
          <Controller
            key={name}
            name={name}
            control={control}
            render={({ field: { value, onChange } }) => (
              <FormControlLabel
                key={name}
                control={
                  <Checkbox name={name} checked={value} onChange={onChange} />
                }
                label={text}
              />
            )}
          />
        ))}
        {children}
      </FormGroup>
    </FormControl>
  );
};

export default FormControlCheckbox;
