import React from "react";
import TextField from "@mui/material/TextField";
import { Controller } from "react-hook-form";
import numeral from "numeral";

const CurrencyNumber = ({ control, name, label, disabled }) => {
  const format = (val) => numeral(parseInt(val)).format("$0,0");
  const parse = (val) => numeral(val).value();

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <TextField
          type="text"
          autoComplete="off"
          inputMode="decimal"
          pattern="[0-9]*(.[0-9]+)?"
          label={label}
          id={name}
          onChange={(e) => {
            const value = e.target.value;
            const parsedValue = parse(disabled ? "0" : value);
            field.onChange(parsedValue || 0);
          }}
          value={field.value ? format(disabled ? "0" : field.value) : "R$"}
          variant="outlined"
          className="w-full"
        />
      )}
    />
  );
};

export default CurrencyNumber;
