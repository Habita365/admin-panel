import React from "react";
import TextField from "@mui/material/TextField";
import InputMask from "react-input-mask";

import { Controller } from "react-hook-form";

const MaskTextField = ({ mask, label, control, name, required = false }) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value } }) => (
        <InputMask
          mask={mask}
          maskChar={null}
          value={value || ""}
          onChange={onChange}
        >
          {(inputProps) => {
            return (
              <TextField
                {...inputProps}
                id={name}
                label={label}
                className="w-full"
                variant="outlined"
                required={required}
              />
            );
          }}
        </InputMask>
      )}
    />
  );
};

export default MaskTextField;
