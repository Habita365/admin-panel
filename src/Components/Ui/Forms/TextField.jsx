import React from "react";
import TextField from "@mui/material/TextField";
import { Controller } from "react-hook-form";

const TextFieldWrapper = ({
  name,
  label,
  placeholder = label,
  type = "text",
  control,
  required = false,
  ...props
}) => {
  return control && name ? (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value } }) => (
        <TextField
          id={name}
          label={label}
          placeholder={placeholder}
          variant="outlined"
          className="w-full"
          value={value ?? ""}
          onChange={(e) => {
            const value = e.target.value;

            if (type === "number") {
              onChange(value ? Number(value) : null);
              return;
            }

            onChange(value);
          }}
          type={type}
          InputLabelProps={{ shrink: true }}
          required={required}
          {...props}
        />
      )}
    />
  ) : (
    <TextField
      label={label}
      placeholder={placeholder}
      variant="outlined"
      className="w-full"
      type={type}
      name={name}
      {...props}
    />
  );
};

export default TextFieldWrapper;
