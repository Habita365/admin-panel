import React, { useEffect, useState } from "react";
import PlacesAutocomplete from "react-places-autocomplete";

const AutocompleteInput = ({
  address,
  onChange,
  onSelect,
  children,
  gMapLoaded,
}) => {
  const [showMap, setShowMap] = useState(false);

  useEffect(() => {
    const scriptGoogle = document.getElementById("google_map_script");
    if (!scriptGoogle && typeof gMapLoaded !== "boolean" && !window.google) {
      window.initMap = () => {
        setShowMap(true);
      };

      const script = document.createElement("script");
      script.id = "google_map_script";
      script.async = true;
      script.defer = true;
      script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}&libraries=places&callback=initMap`;

      document.head.insertAdjacentElement("beforeend", script);
    }

    if (window.google) {
      setShowMap(true);
    }
  }, [gMapLoaded]);
  return showMap || gMapLoaded ? (
    <PlacesAutocomplete
      value={address}
      debounce={100}
      searchOptions={{
        componentRestrictions: {
          country: "br",
        },
        // location: {
        //   lat: -21,
        //   lng: -50
        // },
        // radius: 40000
      }}
      googleCallbackName="initMap"
      onChange={onChange}
      onSelect={onSelect}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps }) => (
        <div className="w-full">
          {children(getInputProps)}
          <div
            className="autocomplete-dropdown-container"
            style={{ padding: suggestions.length > 0 ? "0.5rem" : 0 }}
          >
            {suggestions.map((suggestion, i) => {
              const className = suggestion.active
                ? "suggestion-item suggestion-item--active"
                : "suggestion-item";
              const style = suggestion.active
                ? { backgroundColor: "#fafafa", cursor: "pointer" }
                : { backgroundColor: "#ffffff", cursor: "pointer" };
              return (
                <div
                  {...getSuggestionItemProps(suggestion, {
                    className,
                    style,
                  })}
                  key={i}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </PlacesAutocomplete>
  ) : (
    <>{children()}</>
  );
};

export default AutocompleteInput;
