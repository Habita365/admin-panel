import React from "react";
import { Controller } from "react-hook-form";
import DatePicker from "@mui/lab/DatePicker";
import { format } from "date-fns";
import TextField from "@mui/material/TextField";

const DateField = ({ name, control, value, onChange, label, ...props }) => {
  return name && control ? (
    <Controller
      render={({ field: { onChange, value } }) => (
        <DatePicker
          inputFormat="yyyy/MM/dd"
          margin="normal"
          mask="____/__/__"
          disablePast
          id="date-picker-inline"
          value={value}
          onChange={(value) => {
            onChange(format(value, "yyyy/MM/dd"));
          }}
          // inputVariant="outlined"
          // disableToolbar
          // variant="inline"
          label={label}
          renderInput={(props) => <TextField className="w-full" {...props} />}
          {...props}
        />
      )}
      control={control}
      name={name}
    />
  ) : (
    <DatePicker
      format="yyyy/MM/dd"
      margin="normal"
      disablePast
      id="date-picker-inline"
      value={value}
      onChange={onChange}
      label={label}
      name={name}
      renderInput={(props) => <TextField className="w-full" {...props} />}
      {...props}
    />
  );
};

export default DateField;
