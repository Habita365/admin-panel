/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { getContractDetails } from "redux/contract/actions";
import { dirtyValues, objEmptyCheck } from "utils";

import axios from "axios";

const withForm = (Component) => {
  return function HocComponent({ ContractId, Contract, success, setSuccess }) {
    const dispatch = useDispatch();

    const [loadingForm, setLoadingForm] = useState(false);
    const [message, setMessage] = useState("");
    const [error, setError] = useState(false);

    const {
      watch,
      control,
      register,
      setValue,
      handleSubmit,
      formState: { dirtyFields },
    } = useForm({
      defaultValues: Contract,
    });

    const onSubmit = async (dataValue) => {
      setError(false);
      setSuccess(false);
      setLoadingForm(true);
      try {
        const idToken = localStorage.getItem("token");
        let apiData = dirtyValues(dirtyFields, dataValue);

        if (objEmptyCheck(apiData)) {
          setError(true);
          setLoadingForm(false);

          return setMessage("Edite um campo antes de enviar.");
        }

        await axios.patch(
          `${process.env.REACT_APP_APP_HABITA_CONTRACT_ENDPOINT}/contract/${ContractId}`,
          apiData,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${idToken}`,
            },
          }
        );

        dispatch(getContractDetails(ContractId));
        setSuccess(true);
      } catch (err) {
        setError(true);
        const message = err?.response?.data?.message;
        if (message) {
          if (Array.isArray(message)) {
            setMessage(message.join(", "));
          } else {
            setMessage(message);
          }
        } else {
          setMessage("Um erro ocorreu");
        }
        console.error(err);
      }
      setLoadingForm(false);
    };

    return (
      <form onSubmit={handleSubmit(onSubmit)}>
        <Component
          data={Contract}
          control={control}
          watch={watch}
          loading={loadingForm}
          register={register}
          setValue={setValue}
        />
        {success && (
          <div className="mt-4">
            <p className="text-lg font-semibold">
              Sua solicitação foi bem-sucedida
            </p>
          </div>
        )}
        {error && (
          <div className="mt-4">
            <p className="text-lg text-red-400 font-semibold">{message}</p>
          </div>
        )}
      </form>
    );
  };
};

export default withForm;
