import PhoneInput2 from "react-phone-input-2";
import { Controller } from "react-hook-form";
import "react-phone-input-2/lib/material.css";

const PhoneInput = ({ name, control, autoFocus = true, disabled = false }) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <PhoneInput2
          country={"br"}
          disableDropdown
          inputClass="w-full"
          inputProps={{
            name,
            required: true,
            autoFocus,
            disabled,
          }}
          {...field}
        />
      )}
    />
  );
};

export default PhoneInput;
