/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { getPersonDetails } from "redux/person/actions";

import axios from "axios";
import { objEmptyCheck } from "../../../utils";

const withForm = (Component) => {
  return function HocComponent({
    PersonId,
    Person,
    index,
    IsLegalEntity,
    success,
    setSuccess,
  }) {
    const dispatch = useDispatch();

    const [loadingForm, setLoadingForm] = useState(false);
    const [message, setMessage] = useState("");
    const [error, setError] = useState(false);

    const {
      watch,
      control,
      register,
      setValue,
      handleSubmit,
      formState: { dirtyFields },
    } = useForm({
      defaultValues: Person,
    });

    const onSubmit = async (dataValue) => {
      setError(false);
      setSuccess(false);
      setLoadingForm(true);
      try {
        const idToken = localStorage.getItem("token");
        let apiData = dataValue;
        // let apiData = dirtyValues(dirtyFields, dataValue);
        if (apiData.IdentificationNumber) {
          delete apiData.IdentificationNumber;
        }

        if (index === 2) {
          apiData = {
            ...apiData,
          };
        }

        if (index === 5) {
          apiData = {
            ...dataValue.TenantInformation,
          };
        }

        if (index === 3 && !IsLegalEntity) {
          apiData = {
            ...dataValue.LegalInformation,
          };
        }

        if ((index === 3 || index === 4) && IsLegalEntity) {
          apiData = {
            CompanyInformation: dataValue.CompanyInformation,
            LegalInformation: dataValue.LegalInformation,
          };
        }

        if (objEmptyCheck(apiData)) {
          setError(true);
          setLoadingForm(false);

          return setMessage("Edite um campo antes de enviar.");
        }

        await axios.put(
          `${
            process.env.REACT_APP_APP_HABITA_CONTRACT_ENDPOINT
          }/person/${PersonId}${IsLegalEntity ? "/legal-entity" : ""}${
            index === 3 && !IsLegalEntity ? "/landlord" : ""
          }${index === 5 ? "/tenant" : ""}`,
          apiData,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${idToken}`,
            },
          }
        );

        dispatch(getPersonDetails(PersonId));
        setSuccess(true);
      } catch (err) {
        setError(true);
        const message = err?.response?.data?.message;
        if (message) {
          if (Array.isArray(message)) {
            setMessage(message.join(", "));
          } else {
            setMessage(message);
          }
        } else {
          setMessage("Um erro ocorreu");
        }
        console.error(err);
      }
      setLoadingForm(false);
    };

    return (
      <form onSubmit={handleSubmit(onSubmit)}>
        <Component
          control={control}
          watch={watch}
          loading={loadingForm || objEmptyCheck(dirtyFields)}
          register={register}
          setValue={setValue}
        />
        {success && (
          <div className="mt-4">
            <p className="text-lg font-semibold">
              Sua solicitação foi bem-sucedida
            </p>
          </div>
        )}
        {error && (
          <div className="mt-4">
            <p className="text-lg text-red-400 font-semibold">{message}</p>
          </div>
        )}
      </form>
    );
  };
};

export default withForm;
