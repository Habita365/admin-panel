import React, { useState } from "react";
import axios from "axios";

import { getUserDetails } from "redux/user/actions";
import { dirtyValues, objEmptyCheck } from "utils";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";

const withUserForm = (Component) => {
  return function HocComponent({
    UserId,
    index,
    User,
    userMode,
    success,
    setSuccess,
  }) {
    const dispatch = useDispatch();

    const [loadingForm, setLoadingForm] = useState(false);
    const [message, setMessage] = useState("");
    const [error, setError] = useState(false);

    const {
      watch,
      control,
      register,
      setValue,
      handleSubmit,
      formState: { dirtyFields },
    } = useForm({
      defaultValues: User,
    });

    const onSubmit = async (dataValue) => {
      setError(false);
      setSuccess(false);
      setLoadingForm(true);
      try {
        const idToken = localStorage.getItem("token");
        let apiData = dirtyValues(dirtyFields, dataValue);
        // data.Area = parseInt(data.Area);

        if (index === 2) {
          apiData = {
            ...apiData,
            Address: dataValue?.Address,
          };
        }

        if (objEmptyCheck(apiData)) {
          setError(true);
          setLoadingForm(false);

          return setMessage("Edite um campo antes de enviar.");
        }

        await axios.put(
          `${process.env.REACT_APP_APP_HABITA_USER_ENDPOINT}/user/${UserId}`,
          apiData,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${idToken}`,
            },
          }
        );

        dispatch(getUserDetails(UserId));
        setSuccess(true);
      } catch (err) {
        setError(true);
        const message = err?.response?.data?.message;
        if (message) {
          if (Array.isArray(message)) {
            setMessage(message.join(", "));
          } else {
            setMessage(message);
          }
        } else {
          setMessage("Um erro ocorreu");
        }
        console.error(err);
      }
      setLoadingForm(false);
    };

    return (
      <form onSubmit={handleSubmit(onSubmit)}>
        <Component
          data={User}
          control={control}
          userMode={userMode}
          watch={watch}
          register={register}
          setValue={setValue}
          loading={loadingForm}
        />
        {success && (
          <div className="mt-4">
            <p className="text-lg font-semibold">
              Sua solicitação foi bem-sucedida
            </p>
          </div>
        )}
        {error && (
          <div className="mt-4">
            <p className="text-lg text-red-400 font-semibold">{message}</p>
          </div>
        )}
      </form>
    );
  };
};

export default withUserForm;
