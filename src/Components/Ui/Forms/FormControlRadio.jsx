import React from "react";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import { Controller } from "react-hook-form";

const style = { flexDirection: "row" };

const FormControlRadio = ({ label, data, name, value, control, ...props }) => {
  return (
    <FormControl className="w-full" component="fieldset" {...props}>
      {label && <FormLabel>{label}</FormLabel>}
      {name && control ? (
        <Controller
          key={name}
          name={name}
          control={control}
          render={({ field }) => (
            <RadioGroup style={style} name={name} {...field}>
              {data.map(({ value, text }) => (
                <FormControlLabel
                  key={value}
                  value={value}
                  control={<Radio />}
                  label={text}
                />
              ))}
            </RadioGroup>
          )}
        />
      ) : (
        <RadioGroup style={style} name={name} value={value}>
          {data.map(({ value, text }) => (
            <FormControlLabel
              key={value}
              value={value}
              control={<Radio />}
              label={text}
            />
          ))}
        </RadioGroup>
      )}
    </FormControl>
  );
};

export default FormControlRadio;
