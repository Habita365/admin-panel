import { Controller } from "react-hook-form";
import TextMask from "react-text-mask";

const TextMaskField = ({
  control,
  name,
  required = false,
  readOnly = false,
  mask,
  id,
  disabled = false,
}) => {
  return (
    <Controller
      render={({ field: { onChange, value } }) => (
        <TextMask
          mask={mask}
          disabled={disabled}
          readOnly={readOnly}
          value={value || ""}
          guide={false}
          onChange={onChange}
          required={required}
          id={id}
          className="w-full border rounded-lg px-4 py-2 border-gray-400 h-14"
        />
      )}
      rules={{ required }}
      name={name}
      control={control}
    />
  );
};

export default TextMaskField;
