import React from "react";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import { Controller } from "react-hook-form";

const SelectField = ({ label, name, control, children, ...props }) => {
  return (
    <FormControl variant="outlined" className="w-full">
      <InputLabel id={name + "-label"}>{label}</InputLabel>
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value } }) => (
          <Select
            labelId={name + "-label"}
            id={name && name}
            label={label}
            defaultValue=""
            onChange={onChange}
            value={value || ""}
            {...props}
          >
            {children}
          </Select>
        )}
      />
    </FormControl>
  );
};

export default SelectField;
