import React from "react";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";

const SelectDisplayField = ({ label, name, value, children, ...props }) => {
  return (
    <FormControl variant="outlined" className="w-full">
      <InputLabel id={name + "-label"}>{label}</InputLabel>
      <Select
        disabled
        labelId={name + "-label"}
        id={name && name}
        label={label}
        value={value || ""}
        {...props}
      >
        {children}
      </Select>
    </FormControl>
  );
};

export default SelectDisplayField;
