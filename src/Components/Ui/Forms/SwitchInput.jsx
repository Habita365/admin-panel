import React from "react";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { Controller } from "react-hook-form";

function SwitchInput({ name, control, label, value: val = false }) {
  return (
    <FormControlLabel
      label={label}
      labelPlacement="start"
      control={
        name && control ? (
          <Controller
            control={control}
            name={name}
            render={({
              field: { onChange, onBlur, value = val, name, ref },
            }) => (
              <Switch
                inputRef={ref}
                onBlur={onBlur}
                checked={value}
                onChange={onChange}
                name={name}
                // {...register(name)}
                color="primary"
              />
            )}
          />
        ) : (
          <Switch checked={val} name="checkedB" color="primary" />
        )
      }
    />
  );
}

export default SwitchInput;
