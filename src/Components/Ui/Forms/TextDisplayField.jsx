import React from "react";
import TextField from "@mui/material/TextField";

export default function TextDisplayField({
  label,
  name,
  placeholder = label,
  readOnly = true,
  content,
  ...props
}) {
  return (
    <TextField
      className="w-full"
      label={label}
      name={name}
      defaultValue={content ? content : null}
      InputProps={
        readOnly
          ? {
              readOnly: true,
            }
          : undefined
      }
      placeholder={placeholder}
      variant="outlined"
      {...props}
    />
  );
}
