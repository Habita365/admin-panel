import React from "react";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { Controller } from "react-hook-form";

const SwitchField = ({ name, control, label, onChangeOut }) => {
  return (
    <FormControlLabel
      className="w-full"
      control={
        <Controller
          name={name}
          control={control}
          render={({ field: { value, onChange } }) => (
            <Switch
              checked={value}
              onChange={(e) => {
                const value = e.target.checked;
                onChange(value);
                if (onChangeOut) {
                  onChangeOut(value);
                }
              }}
              name={name}
              color="primary"
            />
          )}
        />
      }
      label={label}
    />
  );
};

export default SwitchField;
