import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "./AppBar";
import Box from "@mui/material/Box";
import Drawer from "./Drawer";
import { Redirect } from "react-router-dom";
import DrawerHeader from "Components/Ui/Drawer/DrawerHead";

function Index({ children, auth }) {
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <>
      {!auth.uid ? (
        <Redirect to="/login" />
      ) : (
        <Box
          sx={{
            display: "flex",
          }}
        >
          <CssBaseline />
          <AppBar handleDrawerOpen={handleDrawerOpen} open={open} auth={auth} />
          <Drawer handleDrawerClose={handleDrawerClose} open={open} />
          <Box
            component="main"
            sx={{
              flexGrow: 1,
              p: 3,
              w: "calc(100% - 73px)",
            }}
            className="no-xs-padding"
          >
            <DrawerHeader />
            {children}
          </Box>
        </Box>
      )}
    </>
  );
}

export default Index;
