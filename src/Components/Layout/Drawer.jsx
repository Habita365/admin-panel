import React from "react";
import MuiDrawer from "@mui/material/Drawer";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import PeopleIcon from "@mui/icons-material/People";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import ReceiptIcon from "@mui/icons-material/Receipt";
import HomeWorkIcon from "@mui/icons-material/HomeWork";
import SupervisedUserCircleIcon from "@mui/icons-material/SupervisedUserCircle";
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import BookmarksIcon from "@mui/icons-material/Bookmarks";
import { useTheme, styled } from "@mui/material/styles";
import List from "@mui/material/List";

import { Link } from "react-router-dom";
import DrawerHeader from "Components/Ui/Drawer/DrawerHead";

const Navigations = [
  { title: "Usuários", link: "users", icon: PeopleIcon },
  { title: "Imóveis", link: "properties", icon: HomeWorkIcon },
  { title: "Visitas", link: "visits", icon: BookmarksIcon },
  { title: "Propostas", link: "dealings", icon: LocalOfferIcon },

  {
    title: "Corretores",
    link: "realtors",
    icon: SupervisedUserCircleIcon,
  },
  { title: "Pessoas", link: "persons", icon: PeopleAltIcon },
  { title: "Contratos", link: "contracts", icon: ReceiptIcon },
];

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

export default function TheDrawer({ handleDrawerClose, open }) {
  const theme = useTheme();

  return (
    <Drawer variant="permanent" open={open}>
      <DrawerHeader>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === "rtl" ? (
            <ChevronRightIcon />
          ) : (
            <ChevronLeftIcon />
          )}
        </IconButton>
      </DrawerHeader>
      <Divider />
      <List>
        {Navigations.map((item, index) => (
          <Link
            style={{ textDecoration: "none", color: "black" }}
            key={index}
            to={`/${item.link}`}
          >
            <ListItem button key={index}>
              <ListItemIcon>{<item.icon />}</ListItemIcon>
              <ListItemText primary={item.title} />
            </ListItem>
          </Link>
        ))}
      </List>
      <Divider />
    </Drawer>
  );
}
