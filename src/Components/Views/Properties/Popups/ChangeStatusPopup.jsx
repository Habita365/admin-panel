import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changePropertyStatus, clearMessage } from "redux/property/actions";

import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { statusOptions } from "utils/options";

export default function ChangeStatusPopup({ open, handleClose, property }) {
  const dispatch = useDispatch();

  const [Status, setStatus] = useState("");
  const [loading, setLoading] = useState(false);

  const propertyList = useSelector((state) => state.propertyList);
  const { propertyMessage, propertyStatus } = propertyList;

  const handlePropertyStatusChange = () => {
    dispatch(
      changePropertyStatus(property?.id, Status, () =>
        setLoading((prev) => {
          return !prev;
        })
      )
    );
  };

  useEffect(() => {
    const AdStatus = property?.AdStatus;

    if (AdStatus) {
      setStatus(AdStatus);
    }
  }, [property]);

  const onClose = () => {
    handleClose();
    setStatus("");
    dispatch(clearMessage());
  };

  const handleChange = (e) => {
    setStatus(e.target.value);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Alterar o status da propriedade</DialogTitle>
      <DialogContent>
        <FormControl fullWidth variant="outlined">
          <InputLabel id="status-input-label">Status</InputLabel>
          <Select
            labelId="status-input-label"
            id="status-input"
            value={Status}
            label="Status"
            onChange={handleChange}
          >
            {statusOptions.map(({ value, text }) => (
              <MenuItem value={value} key={value}>
                {text}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </DialogContent>
      {propertyMessage ? (
        <p
          className={`text-center my-6 font-semibold ${
            propertyStatus === "SUCCESS" ? "text-green-500" : "text-red-500"
          }`}
        >
          {propertyMessage}
        </p>
      ) : null}
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button disabled={loading} onClick={handlePropertyStatusChange}>
          MUDAR O STATUS
        </Button>
      </DialogActions>
    </Dialog>
  );
}
