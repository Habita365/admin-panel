import React, { useState, useEffect } from "react";
import SelectField from "Components/Ui/Forms/SelectField";
import FormControlCheckbox from "Components/Ui/Forms/FormControlCheckbox";
import TextField from "Components/Ui/Forms/TextField";
import MenuItem from "@mui/material/MenuItem";

import Button from "@mui/material/Button";

import withForm from "Components/Ui/Forms/withForm";

import {
  lockTypeOptions,
  propertyConditionOptions,
  servicesAreasOptions,
  amenitiesOptions,
  petsAvailableOptions,
} from "./options";

import * as yup from "yup";

const InternalProperty = ({ control, watch, loading }) => {
  const [showButton, setShowButton] = useState(false);

  const Rooms = watch("Rooms");
  const Suites = watch("Suites");
  const RestRooms = watch("RestRooms");
  const ParkingSpaces = watch("ParkingSpaces");
  const Condition = watch("Condition");
  const PetSize = watch("PetSize");
  const AdditionalInfo = watch("AdditionalInfo");
  const LockType = watch("LockType");

  useEffect(() => {
    const schema = yup.object().shape({
      Rooms: yup.number().required().min(1),
      Suites: yup.number().required().min(0),
      RestRooms: yup.number().required().min(1),
      ParkingSpaces: yup.number().required().min(0),
      Condition: yup.string().required(),
      PetSize: yup.string().required(),
      AdditionalInfo: yup.string().notRequired(),
      LockType: yup.string().required(),
    });

    const schemaObj = {
      Rooms,
      Suites,
      RestRooms,
      ParkingSpaces,
      Condition,
      PetSize,
      AdditionalInfo,
      LockType,
    };

    schema
      .isValid({
        ...schemaObj,
      })
      .then((valid) => {
        setShowButton(valid);
      });
  }, [
    Rooms,
    Suites,
    RestRooms,
    ParkingSpaces,
    Condition,
    PetSize,
    LockType,
    AdditionalInfo,
  ]);

  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          name="Rooms"
          label="Quantidade de quartos"
          type="number"
          max={10}
          control={control}
        />
        <TextField
          name="Suites"
          label="Quantidade de suítes"
          type="number"
          max={10}
          control={control}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          name="RestRooms"
          label="Quantidade de banheiros"
          type="number"
          max={10}
          control={control}
        />
        <TextField
          name="ParkingSpaces"
          label="Quantidade de vagas de garagem"
          type="number"
          max={10}
          control={control}
        />
      </div>
      <div className="flex gap-4 mb-4 w-1/2">
        <SelectField label="O imóvel está:" name="Condition" control={control}>
          {propertyConditionOptions.map(({ value, text }) => (
            <MenuItem value={value} key={value}>
              {text}
            </MenuItem>
          ))}
        </SelectField>
      </div>
      <div className="flex">
        <FormControlCheckbox
          control={control}
          label="Características do imóvel"
          datas={servicesAreasOptions}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <FormControlCheckbox control={control} datas={amenitiesOptions} />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField
          name="PetSize"
          control={control}
          label="Animal de estimação"
        >
          {petsAvailableOptions.map(({ text, value }) => (
            <MenuItem value={value} key={value}>
              {text}
            </MenuItem>
          ))}
        </SelectField>
        <SelectField
          name="LockType"
          control={control}
          label="Tipo de entrada do Imóvel"
        >
          {lockTypeOptions.map(({ text, value }) => (
            <MenuItem value={value} key={value}>
              {text}
            </MenuItem>
          ))}
        </SelectField>
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          label="Alguma característica adicional sobre o imóvel?"
          name="AdditionalInfo"
          control={control}
          multiline
          rows={4}
        />
      </div>
      {showButton && (
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      )}
    </div>
  );
};

export default withForm(InternalProperty);
