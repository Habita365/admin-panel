import React, { useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import axios from "axios";

import { statusOptions } from "utils/options";
import { useDispatch } from "react-redux";
import { Button } from "@mui/material";
import { getPropertyDetails } from "redux/property/actions";

const backend = process.env.REACT_APP_APP_HABITA_PROPERTY_ENDPOINT;

const ChangeStatus = ({ PropertyId, Property }) => {
  const dispatch = useDispatch();

  const PropertyStatus = Property?.AdStatus;

  const [Status, setStatus] = useState(PropertyStatus ?? "");
  const [success, setSuccess] = useState(false);
  const [loadingForm, setLoadingForm] = useState(false);
  const [message, setMessage] = useState("");
  const [error, setError] = useState(false);

  const handleChange = (e) => {
    setStatus(e.target.value);
  };

  const onSubmit = async (e) => {
    setError(false);
    setSuccess(false);
    setLoadingForm(true);

    e.preventDefault();

    const idToken = localStorage.getItem("token");

    const config = {
      headers: {
        Authorization: `Bearer ${idToken}`,
      },
    };

    try {
      const res = await axios.patch(
        `${backend}/property/ad_status/${PropertyId}`,
        {
          AdStatus: Status,
        },
        config
      );

      const { message } = res.data;

      if (message) {
        setMessage(message);
      }

      dispatch(getPropertyDetails(PropertyId));
      setSuccess(true);
    } catch (err) {
      setError(true);
      const message = err?.response?.data?.message;
      if (message) {
        if (Array.isArray(message)) {
          setMessage(message.join(", "));
        } else {
          setMessage(message);
        }
      } else {
        setMessage("Um erro ocorreu");
      }
      console.error(err);
    }
    setLoadingForm(false);
  };

  return (
    <div className="w-1/2">
      <form onSubmit={onSubmit}>
        <FormControl fullWidth variant="outlined">
          <InputLabel id="status-input-label">Status</InputLabel>
          <Select
            labelId="status-input-label"
            id="status-input"
            value={Status}
            label="Status"
            onChange={handleChange}
          >
            {statusOptions.map(({ value, text }) => (
              <MenuItem value={value} key={value}>
                {text}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        {success && (
          <div className="mt-4">
            <p className="text-lg font-semibold">
              Sua solicitação foi bem-sucedida
            </p>
          </div>
        )}
        {error && (
          <div className="mt-4">
            <p className="text-lg text-red-400 font-semibold">{message}</p>
          </div>
        )}
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loadingForm}
          >
            Enviar
          </Button>
        </div>
      </form>
    </div>
  );
};

export default ChangeStatus;
