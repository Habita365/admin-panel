import React, { useEffect, useState } from "react";
import SwitchField from "Components/Ui/Forms/SwitchField";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormGroup from "@mui/material/FormGroup";
import SelectField from "Components/Ui/Forms/SelectField";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";

import { days, times } from "./options";
import withForm from "Components/Ui/Forms/withForm";

import * as yup from "yup";

const VisitDays = ({ control, watch, setValue, loading }) => {
  const [showButton, setShowButton] = useState(false);
  const [currentDay, setCurrentDay] = useState("");
  const [currentIndex, setCurrentIndex] = useState(0);
  const [Timings, setTimings] = useState([]);

  const onChangeDay = (day, index) => {
    setCurrentDay(day);
    setCurrentIndex(index);
  };

  const onChangeTime = (e) => {
    const value = e.target.value;
    const index = Timings.indexOf(value);
    let currentTimings;
    if (index === -1) {
      currentTimings = [...Timings, value];
    } else {
      currentTimings = Timings.filter((time) => {
        return time !== value;
      });
    }
    setTimings(currentTimings);
    const valueString = "VisitHours." + currentIndex + ".Timings";
    setValue(
      valueString,
      currentTimings.filter((time) => {
        return time?.length > 0;
      })
    );
  };

  const LockType = watch("LockType");
  const LeaveKeyWithHabita = watch("LeaveKeyWithHabita");
  const VisitHours = watch("VisitHours") || [];

  const stringifiedVisitHours = JSON.stringify(VisitHours);

  useEffect(() => {
    const valueString = "VisitHours." + currentIndex;
    const valueStrWatch = watch(valueString);
    const daysLength = valueStrWatch?.Timings || [];

    setTimings(daysLength);

    const schema = yup.object().shape({
      LeaveKeyWithHabita:
        LockType === "KEY"
          ? yup.string().required()
          : yup.string().notRequired(),
      VisitHours: yup.array().min(1).required(),
    });

    const schemaObj = {
      VisitHours,
      LeaveKeyWithHabita,
    };

    schema
      .isValid({
        ...schemaObj,
      })
      .then((valid) => {
        if (valid) {
          setShowButton(true);
        }
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    currentIndex,
    watch,
    LockType,
    LeaveKeyWithHabita,
    stringifiedVisitHours,
  ]);

  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4">
        <SelectField control={control} name="KeyStorage" label="Local da chave">
          <MenuItem value="HOMEOWNER">Proprietário</MenuItem>
          <MenuItem value="CONCIERGE">Portaria</MenuItem>
        </SelectField>
        <SwitchField
          control={control}
          name="LeaveKeyWithHabita"
          label="Deixar a chave com a Habita?"
        />
      </div>
      {/* <div className="flex items-center gap-4 mb-4 w-1/2">

      </div> */}
      <div className="mb-4">
        <div className="mb-2">
          <FormControl className="w-full" component="fieldset">
            <FormLabel className="mb-3">Disponibilidade para visitar</FormLabel>
            <FormGroup style={{ flexDirection: "row" }}>
              {days.map(({ name, text }) => {
                const findIndex = VisitHours.findIndex((visit) => {
                  return visit.Day === name;
                });
                const valueString = "VisitHours." + findIndex;
                const valueStrWatch = watch(valueString);
                const filteredTimings = valueStrWatch?.Timings.filter(
                  (time) => {
                    return time?.length > 0;
                  }
                );

                const daysLength = filteredTimings || [];
                const isChecked = daysLength?.length > 0;
                return (
                  <FormControlLabel
                    key={text}
                    control={
                      <Checkbox
                        checked={currentDay === name || isChecked}
                        color={currentDay === name ? "primary" : "default"}
                        value={name}
                        onChange={(e) => onChangeDay(e.target.value, findIndex)}
                      />
                    }
                    label={text}
                  />
                );
              })}
            </FormGroup>
          </FormControl>
        </div>
        <div className="mb-4">
          <FormControl className="w-full" component="fieldset">
            <FormGroup style={{ flexDirection: "row" }}>
              {times.map((value) => {
                const index = Timings.indexOf(value);
                const isChecked = index > -1;
                return (
                  <FormControlLabel
                    key={value}
                    control={
                      <Checkbox
                        value={value}
                        checked={isChecked}
                        onChange={onChangeTime}
                      />
                    }
                    label={value}
                  />
                );
              })}
            </FormGroup>
          </FormControl>
        </div>
      </div>
      {showButton && (
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      )}
    </div>
  );
};

export default withForm(VisitDays);
