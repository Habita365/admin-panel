import React, { useEffect, useState } from "react";
import DateField from "Components/Ui/Forms/DateField";
import FormControlCheckbox from "Components/Ui/Forms/FormControlCheckbox";
import TextField from "Components/Ui/Forms/TextField";
import CurrencyNumber from "Components/Ui/Forms/CurrencyNumber";
import Checkbox from "Components/Ui/Forms/Checkbox";
import Button from "@mui/material/Button";
import SwitchField from "Components/Ui/Forms/SwitchField";
import withForm from "Components/Ui/Forms/withForm";
import { sellExtrasOptions } from "./options";

import * as yup from "yup";

const SaleConditions = ({
  control,
  setValue,
  watch,
  purpose,
  propertyTypeCondition,
  propertyTypeConditionHouseOnly,
  loading,
}) => {
  const [exemptProperty, setExemptProperty] = useState(false);
  const [IsBusy, setIsBusy] = useState(false);
  const [showButton, setShowButton] = useState(false);
  const [others, setOthers] = useState(false);

  const onChange = (e) => {
    const checked = e.target.checked;
    setExemptProperty(checked);

    if (checked) {
      setValue("SellCommercialConditions.IptuValue", 0);
    }
  };

  const IsEmptyWatch = watch("SellCommercialConditions.IsEmpty");
  const SellValueWatch = watch("SellCommercialConditions.Value");
  const IptuValueWatch = watch("SellCommercialConditions.IptuValue");
  const CondominimumValueWatch = watch(
    "SellCommercialConditions.CondominimumValue"
  );
  const OtherIncludedItems = watch(
    "SellCommercialConditions.OtherIncludedItems"
  );

  useEffect(() => {
    const schema = yup.object().shape({
      SellValue: yup.number().required().min(1),
      IptuValue: exemptProperty
        ? yup.number().required()
        : yup.number().required().min(1),
      CondominimumValue: !propertyTypeCondition
        ? yup.number().required().min(1)
        : yup.number().notRequired(),
      IsEmpty: yup.bool().required(),
    });

    const schemaObj = {
      SellValue: SellValueWatch,
      IptuValue: IptuValueWatch,
      CondominimumValue: CondominimumValueWatch,
      IsEmpty: IsEmptyWatch,
    };

    schema
      .isValid({
        ...schemaObj,
      })
      .then((valid) => {
        setShowButton(valid);
      });
  }, [
    SellValueWatch,
    IptuValueWatch,
    propertyTypeCondition,
    exemptProperty,
    CondominimumValueWatch,
    IsEmptyWatch,
  ]);

  useEffect(() => {
    if (IsEmptyWatch === false) {
      setIsBusy(true);
    } else if (IsEmptyWatch) {
      setIsBusy(false);
    }
  }, [IsEmptyWatch]);

  useEffect(() => {
    if (OtherIncludedItems) {
      setOthers(true);
    }
  }, [OtherIncludedItems]);

  return purpose === "BOTH" || purpose === "SELL" ? (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SwitchField
          control={control}
          name="SellCommercialConditions.IsEmpty"
          label="Vago"
        />
      </div>
      {IsBusy && (
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <DateField
            label="Data em que estará vago"
            name="SellCommercialConditions.EmptyDate"
            control={control}
          />
        </div>
      )}
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <CurrencyNumber
          name="SellCommercialConditions.Value"
          label="Valor do venda"
          control={control}
        />
        {!propertyTypeConditionHouseOnly && (
          <CurrencyNumber
            name="SellCommercialConditions.CondominimumValue"
            label="Valor mensal do condomínio"
            control={control}
          />
        )}
      </div>
      <div className="mb-4">
        <div className="flex items-center gap-4 mb-2">
          <CurrencyNumber
            required
            name="SellCommercialConditions.IptuValue"
            disabled={exemptProperty}
            label="Valor total do IPTU"
            control={control}
          />
        </div>
        <Checkbox
          checked={exemptProperty}
          name="exemptProperty"
          onChange={onChange}
          label="Imóvel isento de IPTU"
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <FormControlCheckbox
          control={control}
          label="Valores Inclusos no Condomínio"
          datas={sellExtrasOptions}
        >
          <Checkbox
            name="others"
            checked={others}
            onChange={() => setOthers(!others)}
            label="Outros"
          />
        </FormControlCheckbox>
      </div>
      {others && (
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextField
            label="Outros Itens Inclusos"
            name="SellCommercialConditions.OtherIncludedItems"
            control={control}
            multiline
            rows={4}
          />
        </div>
      )}
      {showButton && (
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      )}
    </div>
  ) : null;
};

export default withForm(SaleConditions);
