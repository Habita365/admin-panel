import React, { useState, useEffect } from "react";

import SelectField from "Components/Ui/Forms/SelectField";
import FormControlCheckbox from "Components/Ui/Forms/FormControlCheckbox";
import MaskTextField from "Components/Ui/Forms/MaskTextField";
import TextField from "Components/Ui/Forms/TextField";

import Button from "@mui/material/Button";

import * as yup from "yup";
import withForm from "Components/Ui/Forms/withForm";

import MenuItem from "@mui/material/MenuItem";

import { conceirgeOptions, complexOptions } from "./options";

const BuildingDescription = ({ control, watch, loading }) => {
  const [showButton, setShowButton] = useState(false);

  const Name = watch("Complex.Name");
  const ConstructionYear = watch("Complex.ConstructionYear");
  const ConstructionCompany = watch("Complex.ConstructionCompany");
  const ConceirgeHours = watch("Complex.ConceirgeHours");
  const AdditionalInfo = watch("Complex.AdditionalInfo");

  useEffect(() => {
    const schema = yup.object().shape({
      Name: yup.string().required(),
      ConstructionYear: yup.string().required(),
      ConstructionCompany: yup.string().required(),
      ConceirgeHours: yup.string().required(),
      AdditionalInfo: yup.string().notRequired(),
    });

    const schemaObj = {
      Name,
      ConstructionYear,
      ConstructionCompany,
      ConceirgeHours,
      AdditionalInfo,
    };

    schema
      .isValid({
        ...schemaObj,
      })
      .then((valid) => {
        setShowButton(valid);
      });
  }, [
    Name,
    ConstructionYear,
    ConstructionCompany,
    ConceirgeHours,
    AdditionalInfo,
  ]);

  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          name="Complex.Name"
          label="Nome do Edifício"
          control={control}
        />
        <MaskTextField
          mask="9999"
          label="Ano de construção do Imóvel"
          name="Complex.ConstructionYear"
          control={control}
        />
      </div>
      <div className="flex gap-4 mb-4">
        <TextField
          name="Complex.ConstructionCompany"
          label="Construtora do Imóvel"
          control={control}
        />
      </div>
      <div className="flex gap-4 mb-4">
        <FormControlCheckbox
          control={control}
          label="Área comum"
          datas={complexOptions}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField
          name="Complex.ConceirgeHours"
          label="Horário da Portaria"
          control={control}
        >
          {conceirgeOptions.map(({ text, value }) => (
            <MenuItem value={value} key={value}>
              {text}
            </MenuItem>
          ))}
        </SelectField>
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          label="Alguma característica adicional sobre o condomínio?"
          name="Complex.AdditionalInfo"
          control={control}
          multiline
          rows={4}
        />
      </div>
      {showButton && (
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      )}
    </div>
  );
};

export default withForm(BuildingDescription);
