import React, { useEffect, useState } from "react";

import TextField from "Components/Ui/Forms/TextField";

import Button from "@mui/material/Button";

import * as yup from "yup";
import withForm from "Components/Ui/Forms/withForm";

const Tour360 = ({ control, watch, loading }) => {
  const [showButton, setShowButton] = useState(false);
  const [error, setError] = useState(false);

  const Tour360Url = watch("Tour360Url");

  useEffect(() => {
    const schema = yup.object().shape({
      Tour360Url: yup
        .string()
        .url("Deve ser uma url")
        .matches(
          /^https:\/\/kuula.co\/share\/collection\/[a-zA-Z0-9]{5}$/,
          "Deve ser no formato: https://kuula.co/share/collection/ABCDE"
        ),
    });

    const schemaObj = {
      Tour360Url,
    };

    schema
      .validate({ ...schemaObj })
      .then(() => {
        setError(false);
      })
      .catch((err) => {
        setError(err.errors);
      });
    schema
      .isValid({
        ...schemaObj,
      })
      .then((valid) => {
        setShowButton(valid);
      });
  }, [Tour360Url]);

  return (
    <div className="w-1/2">
      <TextField
        control={control}
        name="Tour360Url"
        label="Url do Tour 360"
        fullWidth
        variant="outlined"
        disabled={loading}
        helperText={error || ""}
        error={error}
      />
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading || !showButton}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withForm(Tour360);
