import React from "react";
import PhotoOrder from "Components/Ui/Photos/PhotoOrder";
import PhotoUpload from "Components/Ui/Photos/PhotoUpload";

import { useSelector } from "react-redux";

const Fotos = () => {
  const propertyDetails = useSelector((state) => state.propertyDetails);
  const { Property } = propertyDetails;

  return Property ? (
    <div>
      <PhotoUpload record={Property} />
      <PhotoOrder record={Property} />
    </div>
  ) : null;
};

export default Fotos;
