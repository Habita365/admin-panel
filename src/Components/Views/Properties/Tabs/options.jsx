export const propertyPurposeOptions = [
  {
    value: "RENT",
    text: "Alugar",
  },
  {
    value: "SELL",
    text: "Vender",
  },
  {
    value: "BOTH",
    text: "Ambos",
  },
];

export const userTypeOptions = [
  {
    value: "OWNER",
    text: "Proprietário",
  },
  {
    value: "MANAGER",
    text: "Administrador",
  },
];

// export const rentTypeOptions = [
//   {
//     text: "Alugar e Administrar",
//     value: true,
//   },
//   {
//     text: "Somente Alugar",
//     value: false,
//   },
// ];

export const houseStatusOptions = [
  {
    text: "Vago",
    value: "AVAILABLE",
  },
  {
    text: "Ocupado",
    value: "BUSY",
  },
];

export const houseTypeOptions = [
  {
    value: "APARTMENT",
    text: "Apartamento",
  },
  {
    value: "HOUSE",
    text: "Casa",
  },
  {
    value: "HOUSE_IN_A_CONDOMINIMUM",
    text: "Casa em Condomínio",
  },
  {
    value: "STUDIO",
    text: "Studio",
  },
  {
    value: "DUPLEX",
    text: "Duplex",
  },
  {
    value: "PENTHOUSE",
    text: "Cobertura",
  },
  {
    value: "FLAT",
    text: "Plano",
  },
];

export const rentExtrasOptions = [
  {
    name: "RentCommercialConditions.WifiCondo",
    text: "WiFi",
  },
  {
    name: "RentCommercialConditions.CableCondo",
    text: "TV a cabo",
  },
  {
    name: "RentCommercialConditions.CleaningCondo",
    text: "Limpeza Imóvel",
  },
];

export const sellExtrasOptions = [
  {
    name: "SellCommercialConditions.WifiCondo",
    text: "WiFi",
  },
  {
    name: "SellCommercialConditions.CableCondo",
    text: "TV a cabo",
  },
  {
    name: "SellCommercialConditions.CleaningCondo",
    text: "Limpeza Imóvel",
  },
];

export const lockTypeOptions = [
  {
    text: "Chave",
    value: "KEY",
  },
  {
    text: "Senha",
    value: "PASSWORD",
  },
  {
    text: "Biometria",
    value: "FINGERPRINT",
  },
];

export const propertyConditionOptions = [
  {
    value: "FURNISHED",
    text: "Mobiliado",
  },
  {
    value: "SEMI_FURNISHED",
    text: "Semi Mobiliado",
  },
  {
    value: "EMPTY",
    text: "Vazio",
  },
];

export const servicesAreasOptions = [
  {
    name: "HasPowderRoom",
    text: "Lavabo",
  },
  {
    name: "HasServiceArea",
    text: "Área de Serviços",
  },
  {
    name: "HasStoreRoom",
    text: "Depósito Individual",
  },
];

export const amenitiesOptions = [
  {
    name: "HasHeating",
    text: "Aquecedor de Água",
  },
  {
    name: "HasBalcony",
    text: "Varanda",
  },
  {
    name: "HasGourmentBalcony",
    text: "Varanda Gourment",
  },
  {
    name: "HasAirConditioning",
    text: "Ar condicionado",
  },
];

export const petsAvailableOptions = [
  {
    text: "Qualquer tamanho",
    value: "ANY_SIZE",
  },
  {
    text: "Não aceita pet",
    value: "NO_PET",
  },
  {
    text: "Aceita Pet Pequeno",
    value: "SMALL",
  },
  {
    text: "Aceita Pet Médio",
    value: "MEDIUM",
  },
  {
    text: "Aceita Pet Grande",
    value: "LARGE",
  },
];

export const conceirgeOptions = [
  {
    text: "24hrs",
    value: "ALL_TIME",
  },
  {
    text: "Diurna",
    value: "DAY",
  },
  {
    text: "Noturna",
    value: "NIGHT",
  },
  {
    text: "Sem portaria",
    value: "NO_CONCEIRGE",
  },
];

export const keyStorageOptions = [
  {
    text: "Proprietário",
    value: "HOMEOWNER",
  },
  {
    text: "Portaria",
    value: "CONCEIRGE",
  },
  {
    text: "Habita",
    value: "HABITA",
  },
];

export const complexOptions = [
  {
    text: "Churrasqueira",
    name: "Complex.HasBarbequeArea",
  },
  {
    text: "Espaço Gourmet",
    name: "Complex.HasGourmetArea",
  },
  {
    text: "Salão de Festas",
    name: "Complex.HasPartyHall",
  },
  {
    text: "Piscina",
    name: "Complex.HasPool",
  },
  {
    text: "Quadra Poliesportiva",
    name: "Complex.HasSports",
  },
  {
    text: "Playground",
    name: "Complex.HasPlayground",
  },
  {
    text: "Área de Co-Working",
    name: "Complex.HasCoWorking",
  },
  {
    text: "Área de Co-Living",
    name: "Complex.HasCoLiving",
  },
  {
    text: "Lavanderia Coletiva",
    name: "Complex.HasLaundry",
  },
  {
    text: "Salão de Jogos",
    name: "Complex.HasGame",
  },
  {
    text: "Academia",
    name: "Complex.HasGym",
  },
  {
    text: "Wifi na Área Comum",
    name: "Complex.HasExternalWifi",
  },
];

export const days = [
  {
    text: "Segunda",
    name: "MONDAY",
  },
  {
    text: "Terça",
    name: "TUESDAY",
  },
  {
    text: "Quarta",
    name: "WEDNESDAY",
  },
  {
    name: "THURSDAY",
    text: "Quinta",
  },
  {
    name: "FRIDAY",
    text: "Sexta",
  },
  {
    name: "SATURDAY",
    text: "Sábado",
  },
  {
    name: "SUNDAY",
    text: "Domingo",
  },
];

export const times = [
  "8:00 - 9:00",
  "9:00 - 10:00",
  "10:00 - 11:00",
  "11:00 - 12:00",
  "13:00 - 14:00",
  "14:00 - 15:00",
  "15:00 - 16:00",
  "16:00 - 17:00",
  "17:00 - 18:00",
  "18:00 - 19:00",
];
