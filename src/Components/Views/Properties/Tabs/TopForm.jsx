import React, { useState, useEffect } from "react";
import {
  propertyPurposeOptions,
  userTypeOptions,
  houseTypeOptions,
} from "./options";

import SelectField from "Components/Ui/Forms/SelectField";
import TextField from "Components/Ui/Forms/TextField";
import SwitchField from "Components/Ui/Forms/SwitchField";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import InputAdornment from "@mui/material/InputAdornment";
import withForm from "Components/Ui/Forms/withForm";

import * as yup from "yup";
import { useSelector } from "react-redux";
import MaskTextField from "Components/Ui/Forms/MaskTextField";

const TopForm = ({ control, watch, loading }) => {
  const userDetails = useSelector((state) => state.userDetails);
  const { User, loading: loadingUser } = userDetails;

  const [showButton, setShowButton] = useState(false);

  const Purpose = watch("Purpose");
  const Relation = watch("Relation");
  const Type = watch("Type");
  const Area = watch("Area");

  useEffect(() => {
    const schema = yup.object().shape({
      Purpose: yup.string().required(),
      Relation: yup.string().required(),
      Type: yup.string().required(),
      Area: yup.number().required().min(1),
    });

    const schemaObj = {
      Purpose,
      Relation,
      Type,
      Area,
    };

    schema
      .isValid({
        ...schemaObj,
      })
      .then((valid) => {
        if (valid && Type === "DEFAULT") {
          setShowButton(false);
          return;
        }
        setShowButton(valid);
      });
  }, [Purpose, Relation, Type, Area]);

  return (
    <div>
      <div className="w-full lg:w-1/2">
        {!loadingUser && (
          <>
            <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
              <TextField label="Nome" disabled value={User?.Name} />
              <TextField
                label="Email"
                disabled
                type="email"
                value={User?.Email}
              />
            </div>
            <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
              <TextField label="Telefone" disabled value={User?.PhoneNumber} />
            </div>
          </>
        )}
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <SwitchField control={control} name="IsFeatured" label="Destaque" />
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <SelectField
            name="Purpose"
            label="Finalidade da propriedade"
            control={control}
          >
            {propertyPurposeOptions.map(({ value, text }) => (
              <MenuItem value={value} key={value}>
                {text}
              </MenuItem>
            ))}
          </SelectField>
          <SelectField
            name="Relation"
            label="Relação de propriedade"
            control={control}
          >
            {userTypeOptions.map(({ value, text }) => (
              <MenuItem value={value} key={value}>
                {text}
              </MenuItem>
            ))}
          </SelectField>
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <SelectField name="Type" label="Tipo de Imóvel" control={control}>
            {houseTypeOptions.map(({ value, text }) => (
              <MenuItem value={value} key={value}>
                {text}
              </MenuItem>
            ))}
          </SelectField>
          <TextField
            control={control}
            name="Area"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              endAdornment: <InputAdornment position="end">m²</InputAdornment>,
            }}
          />
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <MaskTextField
            control={control}
            name="AssociateCode"
            label="Código de parceiro"
            mask="****"
          />
        </div>
        <div className="flex items-center gap-4">
          <TextField
            label="Comentário interno"
            name="InternalComments"
            control={control}
            multiline
            rows={8}
          />
        </div>
        {showButton && (
          <div className="mt-4">
            <Button
              type="submit"
              color="primary"
              variant="contained"
              disabled={loading}
            >
              Enviar
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

export default withForm(TopForm);
