import React, { useEffect, useState } from "react";
import SelectField from "Components/Ui/Forms/SelectField";
import DateField from "Components/Ui/Forms/DateField";
import TextField from "Components/Ui/Forms/TextField";
import CurrencyNumber from "Components/Ui/Forms/CurrencyNumber";
import Checkbox from "Components/Ui/Forms/Checkbox";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import { rentExtrasOptions } from "./options";
import FormControlCheckbox from "Components/Ui/Forms/FormControlCheckbox";
import SwitchField from "Components/Ui/Forms/SwitchField";
import withForm from "Components/Ui/Forms/withForm";

import * as yup from "yup";

const RentConditions = ({
  control,
  watch,
  purpose,
  setValue,
  propertyTypeConditionHouseOnly,
  propertyTypeCondition,
  loading,
}) => {
  const [exemptProperty, setExemptProperty] = useState(false);
  const [showButton, setShowButton] = useState(false);
  const [IsBusy, setIsBusy] = useState(false);
  const [others, setOthers] = useState(false);

  const IsEmptyWatch = watch("RentCommercialConditions.IsEmpty");
  const HabitaManagementWatch = watch(
    "RentCommercialConditions.HabitaManagement"
  );

  const RentValueWatch = watch("RentCommercialConditions.Value");
  const IptuValueWatch = watch("RentCommercialConditions.IptuValue");
  const CondominimumValueWatch = watch(
    "RentCommercialConditions.CondominimumValue"
  );
  const OtherIncludedItems = watch(
    "RentCommercialConditions.OtherIncludedItems"
  );
  const ReadjustmentIndex = watch("RentCommercialConditions.ReadjustmentIndex");

  useEffect(() => {
    const schema = yup.object().shape({
      RentValue: yup.number().required().min(1),
      IptuValue: yup.number().required().min(0),
      CondominimumValue: !propertyTypeCondition
        ? yup.number().required().min(1)
        : yup.number().notRequired(),
      ReadjustmentIndex: yup.string().required(),
      HabitaManagement: yup.bool().required(),
      IsEmpty: yup.bool().required(),
    });

    const schemaObj = {
      RentValue: RentValueWatch,
      IptuValue: IptuValueWatch,
      CondominimumValue: CondominimumValueWatch,
      HabitaManagement: HabitaManagementWatch,
      ReadjustmentIndex,
      IsEmpty: IsEmptyWatch,
    };

    schema.isValid(schemaObj).then((valid) => {
      setShowButton(valid);
    });
  }, [
    propertyTypeCondition,
    RentValueWatch,
    ReadjustmentIndex,
    IptuValueWatch,
    CondominimumValueWatch,
    exemptProperty,
    IsEmptyWatch,
    HabitaManagementWatch,
  ]);

  useEffect(() => {
    if (IsEmptyWatch === false) {
      setIsBusy(true);
    } else if (IsEmptyWatch) {
      setIsBusy(false);
    }
  }, [IsEmptyWatch]);

  useEffect(() => {
    if (OtherIncludedItems) {
      setOthers(true);
    }
  }, [OtherIncludedItems]);

  const onChange = (e) => {
    const checked = e.target.checked;
    setExemptProperty(checked);

    if (checked) {
      setValue("RentCommercialConditions.IptuValue", 0);
    }
  };

  const onChangeIsEmpty = (value) => {
    setValue("SellCommercialConditions.IsEmpty", value);
  };

  const onChangeHabitaManagement = (value) => {
    setValue("SellCommercialConditions.HabitaManagement", value);
  };

  return purpose === "BOTH" || purpose === "RENT" ? (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SwitchField
          control={control}
          name="RentCommercialConditions.IsEmpty"
          label="Vago"
          onChangeOut={onChangeIsEmpty}
        />
        <SwitchField
          control={control}
          name="RentCommercialConditions.HabitaManagement"
          label="Habita365 irá Alugar e Administrar"
          onChangeOut={onChangeHabitaManagement}
        />
      </div>
      {IsBusy && (
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <DateField
            label="Data em que estará vago"
            name="RentCommercialConditions.EmptyDate"
            control={control}
          />
        </div>
      )}
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <CurrencyNumber
          name="RentCommercialConditions.Value"
          label="Valor do aluguel"
          control={control}
        />
        <SelectField
          name="RentCommercialConditions.ReadjustmentIndex"
          label="Índice reajuste"
          control={control}
        >
          <MenuItem value="IGPM">IGPM</MenuItem>
          <MenuItem value="IPCA">IPCA</MenuItem>
        </SelectField>
      </div>
      <div className="mb-4">
        <div className="flex items-center gap-4 mb-2">
          <CurrencyNumber
            required
            name="RentCommercialConditions.IptuValue"
            disabled={exemptProperty}
            label="Valor total do IPTU"
            control={control}
          />
          {!propertyTypeConditionHouseOnly && (
            <CurrencyNumber
              name="RentCommercialConditions.CondominimumValue"
              disabled={propertyTypeCondition}
              label="Valor mensal do condomínio (cota ordinária)"
              control={control}
            />
          )}
        </div>
        <Checkbox
          checked={exemptProperty}
          name="exemptProperty"
          onChange={onChange}
          label="Imóvel isento de IPTU"
        />
      </div>
      {!propertyTypeConditionHouseOnly && (
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <FormControlCheckbox
            control={control}
            label="Valores Inclusos no Condomínio"
            datas={rentExtrasOptions}
          >
            <Checkbox
              name="others"
              label="Outros"
              checked={others}
              onChange={() => setOthers(!others)}
            />
          </FormControlCheckbox>
        </div>
      )}

      {others && (
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextField
            label="Outros Itens Inclusos"
            name="RentCommercialConditions.OtherIncludedItems"
            control={control}
            multiline
            rows={4}
          />
        </div>
      )}
      {showButton && (
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      )}
    </div>
  ) : null;
};

export default withForm(RentConditions);
