import React, { useEffect, useState } from "react";
import AutocompleteInput from "Components/Ui/Forms/AutocompleteInput";
import TextField from "@mui/material/TextField";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import TextFieldWrapper from "Components/Ui/Forms/TextField";
import withForm from "Components/Ui/Forms/withForm";
import MaskTextField from "Components/Ui/Forms/MaskTextField";
import fillInAddress from "utils/fillInAddress";

import Button from "@mui/material/Button";

import * as yup from "yup";

const AddressForm = withForm(
  ({
    watch,
    propertyTypeCondition,
    propertyTypeConditionHouseOnly,
    setValue,
    control,
    loading,
  }) => {
    const [address, setAddress] = useState("");
    const [showButton, setShowButton] = useState(false);
    const [message, setMessage] = useState("");
    const [invalid, setInvalid] = useState(false);

    const StreetNumber = watch("Address.StreetNumber");
    const Address = watch("Address.Address");
    const Neighbourhood = watch("Address.Neighbourhood");
    const PostalCode = watch("Address.PostalCode");
    const Compl = watch("Address.Compl");
    const FloorNumber = watch("Address.FloorNumber");

    const handleSelect = async (addr) => {
      setInvalid(false);
      setMessage("");
      try {
        const results = await geocodeByAddress(addr);
        const result = results[0];
        const coordinates = await getLatLng(result);

        if (result) {
          setValue("Address.Longitude", coordinates.lng);
          setValue("Address.Latitude", coordinates.lat);

          const addressObj = fillInAddress(result.address_components);

          if (addressObj.city === "São Paulo") {
            for (const key in addressObj) {
              switch (key) {
                case "city":
                  setValue("Address.City", addressObj[key]);
                  break;
                case "streetNumber":
                  setValue("Address.StreetNumber", addressObj[key]);
                  break;
                case "address":
                  setValue("Address.Address", addressObj[key]);
                  setAddress(addressObj[key]);
                  break;
                case "neighbourhood":
                  setValue("Address.Neighbourhood", addressObj[key]);
                  break;
                case "State":
                  setValue("Address.State", addressObj[key]);
                  break;
                case "postalCode":
                  setValue("Address.PostalCode", addressObj[key]);
                  break;
                default:
                  break;
              }
            }
          } else {
            setMessage("Atualmente, apenas casas em São Paulo são suportadas");
            setInvalid(true);
          }
        } else {
          setInvalid(true);

          setValue("Address.City", "");
          setValue("Address.StreetNumber", "");
          setValue("Address.Address", "");
          setValue("Address.Neighbourhood", "");
          setValue("Address.Compl", "");
          setValue("Address.State", "");
          setValue("Address.PostalCode", "");
          setValue("Address.FloorNumber", "");
        }
      } catch (err) {
        if (err) {
          console.error("Error", err);
          setMessage("Um erro ocorreu");
        }
      }
    };

    useEffect(() => {
      const schema = yup.object().shape({
        Neighbourhood: yup.string().required(),
        PostalCode: yup.string().required(),
        Compl: propertyTypeConditionHouseOnly
          ? yup.string().notRequired()
          : yup.string().required(),
        FloorNumber: propertyTypeCondition
          ? yup.number().notRequired()
          : yup.number().min(1).max(100).required(),
        Address: yup.string().notRequired(),
      });

      if (Address && !StreetNumber) {
        setAddress(`${Address}, número`);
      } else if (StreetNumber) {
        setAddress(`${Address}, ${StreetNumber}`);
      }

      const schemaObj = {
        Neighbourhood,
        PostalCode,
        Compl,
        FloorNumber,
        Address,
      };

      schema
        .isValid({
          ...schemaObj,
        })
        .then((valid) => {
          setShowButton(valid);
        });
    }, [
      Neighbourhood,
      PostalCode,
      Compl,
      FloorNumber,
      Address,
      propertyTypeCondition,
      StreetNumber,
      propertyTypeConditionHouseOnly,
    ]);

    return (
      <div className="w-full lg:w-1/2">
        <div className="flex items-center gap-4 mb-4 relative">
          <AutocompleteInput
            address={address}
            onChange={(addr) => setAddress(addr)}
            onSelect={handleSelect}
          >
            {(getInputProps) => {
              let getInputPropsRes;

              if (getInputProps) {
                getInputPropsRes = getInputProps({
                  placeholder: "Endereço com número",
                });
              }

              return (
                <TextField
                  type="text"
                  variant="outlined"
                  className="w-full"
                  {...getInputPropsRes}
                />
              );
            }}
          </AutocompleteInput>
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextFieldWrapper
            label="Bairro"
            name="Address.Neighbourhood"
            placeholder="Moema"
            control={control}
          />
          <MaskTextField
            control={control}
            name="Address.PostalCode"
            mask="99999-999"
            label="CEP"
          />
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextFieldWrapper
            label="Número"
            name="Address.StreetNumber"
            control={control}
            InputLabelProps={{ shrink: true }}
          />
          {!propertyTypeConditionHouseOnly ? (
            <TextFieldWrapper
              label="Complemento"
              name="Address.Compl"
              control={control}
            />
          ) : null}
          {!propertyTypeCondition ? (
            <TextFieldWrapper
              label="Andar"
              name="Address.FloorNumber"
              control={control}
              type="number"
              // InputLabelProps={{
              //   shrink: true,
              // }}
            />
          ) : null}
        </div>
        <p
          className={`text-lg my-4 font-semibold${
            !invalid ? "" : " text-red-400"
          }`}
        >
          {message && message}
        </p>
        {showButton && (
          <div className="mt-4">
            <Button
              type="submit"
              color="primary"
              variant="contained"
              disabled={loading}
            >
              Enviar
            </Button>
          </div>
        )}
      </div>
    );
  }
);

export default AddressForm;
