/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from "react";
import { styled } from "@mui/material/styles";
import { Link, useHistory } from "react-router-dom";
import { DataGrid } from "@mui/x-data-grid";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import InputAdornment from "@mui/material/InputAdornment";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { listProperties, toggleIsFeatured } from "redux/property/actions";
import { useSelector } from "react-redux";
import TextField from "@mui/material/TextField";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import EditAttributes from "@mui/icons-material/EditAttributes";
import columnsData from "./columns";
import DeletePropertyPopup from "./Popups/DeletePropertyPopup";
import ChangeStatusPopup from "./Popups/ChangeStatusPopup";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import StarIcon from "@mui/icons-material/Star";

import { queryFunc } from "utils";
import useFetchList from "hooks/useFetchList";

const PREFIX = "Properties";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

const Properties = () => {
  const inputRef = useRef(null);

  const {
    dispatch,
    page,
    limit,
    setLimit,
    setPage,
    sortModel,
    setSortModel,
    queryParams,
  } = useFetchList(1, 18);

  const history = useHistory();
  const queryAddress = queryParams.get("Address");

  const [Address, setAddress] = useState(queryAddress ?? "");
  const [property, setProperty] = useState({});
  const [open, setOpen] = useState(false);
  const [openStatus, setOpenStatus] = useState(false);

  const propertyList = useSelector((state) => state.propertyList);
  const { loading, Properties, TotalProperties } = propertyList;

  const onSortModelChange = (model) => {
    if (JSON.stringify(model) !== JSON.stringify(sortModel)) {
      setSortModel(model);
    }
  };

  const toggleFeatured = (id, value) => {
    dispatch(toggleIsFeatured(id, value));
  };

  const columns = [
    {
      field: "IsFeatured",
      headerName: "Destaque",
      type: "boolean",
      width: 150,
      sortable: false,
      renderCell: (params) => {
        const value = params.value;
        return (
          <Box
            width="100%"
            onClick={() => toggleFeatured(params.id, params.value)}
            className="cursor-pointer"
          >
            {value ? <StarIcon /> : <StarBorderIcon />}
          </Box>
        );
      },
    },
    ...columnsData,
    {
      field: "Change status",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 200,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Box display="flex" style={{ margin: "0 auto" }}>
          <Button
            onClick={() => {
              const paramsId = params?.id;

              const property = Properties.find((p) => {
                return p.id === paramsId;
              });

              setProperty(property);

              setOpenStatus(true);
            }}
            startIcon={<EditAttributes />}
          >
            Mudar o status
          </Button>
        </Box>
      ),
    },
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Link
          to={`/properties/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button startIcon={<EditIcon />}>Editar</Button>
          </Box>
        </Link>
      ),
    },
    {
      field: "Delete",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => {
        return (
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button
              onClick={() => {
                const paramsId = params?.id;

                const property = Properties.find((p) => {
                  return p.id === paramsId;
                });

                setProperty(property);

                setOpen(true);
              }}
              startIcon={<DeleteIcon />}
            >
              Excluir
            </Button>
          </Box>
        );
      },
    },
  ];

  const stringifiedSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    dispatch(listProperties({ page, limit, sortModel, Address }));
  }, [dispatch, page, limit, stringifiedSortModel, Address]);

  useEffect(() => {
    const queryObj = queryFunc(sortModel, page, limit, Address);

    if (queryObj) {
      const query = new URLSearchParams(queryObj);

      history.push({
        search: query.toString(),
      });
    }
  }, [page, stringifiedSortModel, limit, Address]);

  const onSubmitSearch = (e) => {
    e.preventDefault();

    const value = inputRef.current?.value;

    setAddress(value);
  };

  return (
    <Root>
      <DeletePropertyPopup
        open={open}
        property={property}
        handleClose={() => {
          setOpen(false);
        }}
      />

      <ChangeStatusPopup
        open={openStatus}
        property={property}
        handleClose={() => {
          setOpenStatus(false);
        }}
      />
      <Box display="flex" alignItems="center" flexWrap="wrap" m={1} mb={3}>
        <form
          onSubmit={onSubmitSearch}
          className="mr-auto-imp w-full sm:w-auto mb-4-imp flex sm:mb-0"
        >
          <TextField
            variant="outlined"
            size="small"
            placeholder="Endereço"
            inputRef={inputRef}
            type="search"
            // onChange={(e) => setAddress(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchOutlinedIcon />
                </InputAdornment>
              ),
            }}
            style={{
              marginRight: "0.75rem",
              height: "auto",
              width: "100%",
            }}
          />
          <Button variant="contained" type="submit">
            Procurar
          </Button>
        </form>
      </Box>

      <div style={{ width: "100%", height: "100%" }}>
        <DataGrid
          autoHeight
          rows={Properties}
          columns={columns}
          density="compact"
          page={page - 1}
          onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
          onPageChange={(newPage) => setPage(newPage + 1)}
          pageSize={limit}
          paginationMode="server"
          rowCount={TotalProperties}
          rowsPerPageOptions={[limit]}
          loading={loading}
          checkboxSelection
          pagination
          disableSelectionOnClick
          sortingMode="server"
          sortModel={sortModel}
          onSortModelChange={onSortModelChange}
        />
      </div>
    </Root>
  );
};

export default Properties;
