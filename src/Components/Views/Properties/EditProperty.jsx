import React, { useEffect } from "react";
import { getPropertyDetails } from "redux/property/actions";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

import EditTabs from "./EditTabs";

const EditProperty = ({ match }) => {
  const dispatch = useDispatch();

  const PropertyId = match.params.id;

  useEffect(() => {
    if (PropertyId) {
      dispatch(getPropertyDetails(PropertyId));
    }
  }, [dispatch, PropertyId]);

  return (
    <>
      <div>
        <EditTabs PropertyId={PropertyId} />
      </div>
    </>
  );
};

export default withRouter(EditProperty);
