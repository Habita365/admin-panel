import numeral from "numeral";
import { formattedDate } from "utils";

const convertPropertyStatus = (status) => {
  switch (status) {
    case "PUBLISHED": {
      return {
        value: "Publicado",
        color: "text-green-800",
      };
    }
    case "PAUSED": {
      return {
        value: "Pausado",
        color: "text-gray-800",
      };
    }
    case "PENDING_APPROVAL": {
      return {
        value: "Aguardando aprovação",
        color: "text-yellow-800",
      };
    }
    case "PENDING_PICTURES": {
      return {
        value: "Aguardando fotos",
        color: "text-yellow-800",
      };
    }
    case "REQUESTING_CHANGES": {
      return {
        value: "Necessita mudanças",
        color: "text-red-800",
      };
    }
    default: {
      return {
        value: "",
        color: "",
      };
    }
  }
};

const convertPropertyRelation = (relation) => {
  switch (relation) {
    case "OWNER": {
      return "Proprietário";
    }
    case "MANAGER": {
      return "Administrador";
    }
    default: {
      return "";
    }
  }
};
const convertPropertyPurpose = (purpose) => {
  switch (purpose) {
    case "RENT": {
      return "Aluguel";
    }
    case "SELL": {
      return "Venda";
    }
    case "BOTH": {
      return "Ambos";
    }
    default: {
      return "";
    }
  }
};

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 100,
  },
  {
    field: "Address",
    headerName: "Endereço",
    width: 200,
  },
  // {
  //   field: "Customer Address",
  //   headerName: "Endereço",
  //   width: 300,
  //   valueGetter: (params) => `${params.getValue(params.id, 'Address')||""}, ${params.getValue(params.id, 'StreetNumber')||""}, ${params.getValue(params.id, 'Compl')||""}`
  // },
  {
    field: "StreetNumber",
    headerName: "N°",
    sortable: false,
    width: 100,
  },
  {
    field: "Compl",
    headerName: "Compl.",
    width: 132,
    sortable: false,
  },
  {
    field: "Purpose",
    headerName: "Purpose",
    width: 150,
    sortable: false,
    valueFormatter: (params) => {
      return convertPropertyPurpose(params.value);
    },
  },
  {
    field: "Relation",
    headerName: "Relação",
    width: 150,
    sortable: false,
    valueFormatter: (params) => {
      return convertPropertyRelation(params.value);
    },
  },
  {
    field: "AssociateCode",
    headerName: "Código de parceiro",
    width: 150,
    sortable: false,
    // valueFormatter: (params) => {
    //   return convertPropertyRelation(params.value);
    // },
  },
  {
    field: "IsEmpty",
    headerName: "Vago",
    type: "boolean",
    width: 120,
    sortable: false,
  },
  {
    field: "RentValue",
    type: "number",
    headerName: "Aluguel",
    valueFormatter: (params) => {
      if (!params.value) return "";
      return numeral(params.value).format("$0,0");
    },
    width: 148,
  },
  {
    field: "SellValue",
    type: "number",
    headerName: "Venda",
    valueFormatter: (params) => {
      if (!params.value) return "";
      return numeral(params.value).format("$0,0");
    },
    width: 148,
  },
  {
    field: "UpdatedAt",
    headerName: "Atualizado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "CreatedAt",
    headerName: "Criado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "AdStatus",
    headerName: "Status",
    width: 170,
    sortable: false,
    valueFormatter: (params) => {
      const obj = convertPropertyStatus(params.value);
      return obj.value;
    },
    cellClassName: (params) => {
      const obj = convertPropertyStatus(params.value);
      return obj.color + " font-semibold";
    },
  },
  // {
  //   field: "Condition",
  //   headerName: "Condition",
  //   width: 150,
  // },
  // {
  //   field: "PetSize",
  //   headerName: "PetSize",
  //   width: 150,
  // },
  // {
  //   field: "LockType",
  //   headerName: "LockType",
  //   width: 150,
  // },
  // {
  //   field: "KeyStorage",
  //   headerName: "KeyStorage",
  //   width: 150,
  // },
  // {
  //   field: "IsHabitaOwned",
  //   headerName: "IsHabitaOwned",
  //   width: 150,
  //   type: "boolean",
  //   hideSortIcons: true,
  //   disableColumnMenu: true
  // },
  // {
  //   field: "IsPetFriendly",
  //   headerName: "IsPetFriendly",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 150,
  //   type: "boolean",
  // },
  // {
  //   field: "LeaveKeyWithHabita",
  //   headerName: "LeaveKeyWithHabita",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 150,
  //   type: "boolean",
  // },
  // {
  //   field: "HasPowderRoom",
  //   headerName: "PowderRoom",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "HasServiceArea",
  //   headerName: "ServiceArea",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "HasStoreRoom",
  //   headerName: "StoreRoom",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "HasHeating",
  //   headerName: "Heating",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "HasAirConditioning",
  //   headerName: "AirConditioning",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "HasBalcony",
  //   headerName: "Balcony",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "HasGourmentBalcony",
  //   headerName: "GourmentBalcony",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   width: 100,
  //   type: "boolean",
  // },
  // {
  //   field: "Rooms",
  //   headerName: "Rooms",
  //   width: 150,
  //   type: "number",
  // },
  // {
  //   field: "Suites",
  //   headerName: "Suites",
  //   width: 150,
  //   type: "number",
  // },
  // {
  //   field: "RestRooms",
  //   headerName: "RestRooms",
  //   width: 150,
  //   type: "number",
  // },
  // {
  //   field: "ParkingSpaces",
  //   headerName: "ParkingSpaces",
  //   width: 150,
  //   type: "number",
  // },
  // {
  //   field: "Name",
  //   headerName: "Complex Name",
  //   width: 200
  // },
  // {
  //   field: "HasBarbequeArea",
  //   headerName: "Complex BarbequeArea",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasGourmetArea",
  //   headerName: "Complex GourmetArea",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasPartyHall",
  //   headerName: "Complex PartyHall",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasPool",
  //   headerName: "Complex Pool",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasPlayground",
  //   headerName: "Complex Playground",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasCoLiving",
  //   headerName: "Complex CoLiving",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasCoWorking",
  //   headerName: "Complex CoWorking",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasExternalWifi",
  //   headerName: "Complex ExternalWifi",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasGym",
  //   headerName: "Complex Gym",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasLaundry",
  //   headerName: "Complex Laundry",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasGame",
  //   headerName: "Complex Game",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "HasSports",
  //   headerName: "Complex Sports",
  //   hideSortIcons: true,
  //   disableColumnMenu: true,
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "ConstructionYear",
  //   headerName: "Complex ConstructionYear",
  //   width: 200
  // },
  // {
  //   field: "ConstructionCompany",
  //   headerName: "Complex ConstructionCompany",
  //   width: 200
  // },
  // {
  //   field: "ConceirgeHours",
  //   headerName: "Complex ConciergeHours",
  //   width: 200
  // },
  // {
  //   field: "HabitaManagement",
  //   headerName: "CommercialConditions HabitaManagement",
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "EmptyDate",
  //   headerName: "CommercialConditions EmptyDate",
  //   type: "date",
  //   valueFormatter: (params) => {
  //     const dateEmpty = params.value;
  //     const dateValue = new Date(dateEmpty);
  //     const valueFormatted = format(dateValue, "yyyy/MM/dd")
  //     return valueFormatted
  //   },
  //   width: 200
  // },
  // {
  //   field: "IptuValue",
  //   type: "number",
  //   headerName: "CommercialConditions IptuValue",
  //   valueFormatter: (params) => {
  //     const valueFormatted = numeral(params.value).format("$0,0")
  //     return valueFormatted
  //   },
  //   width: 200
  // },
  // {
  //   field: "CondominimumValue",
  //   type: "number",
  //   headerName: "CommercialConditions CondominimumValue",
  //   valueFormatter: (params) => {
  //     const valueFormatted = numeral(params.value).format("$0,0")
  //     return valueFormatted
  //   },
  //   width: 200
  // },
  // {
  //   field: "WifiCondo",
  //   headerName: "CommercialConditions WifiCondo",
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "CableCondo",
  //   headerName: "CommercialConditions CableCondo",
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "CleaningCondo",
  //   headerName: "CommercialConditions CleaningCondo",
  //   type: "boolean",
  //   width: 200
  // },
  // {
  //   field: "ReadjustmentIndex",
  //   headerName: "CommercialConditions ReadjustmentIndex",
  //   width: 200
  // },
];

export default columns;
