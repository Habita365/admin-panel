import React, { useState } from "react";
import TabPanel from "Components/Ui/TabPanel";
import LinkTab from "Components/Ui/LinkTab";
import RootList from "Components/Ui/RootList";

import TopForm from "./Tabs/TopForm";
import AddressForm from "./Tabs/AddressForm";
import RentConditions from "./Tabs/RentConditions";
import SaleConditions from "./Tabs/SaleConditions";
import InternalProperty from "./Tabs/InternalProperty";
import BuildingDescription from "./Tabs/BuildingDescription";
import ChangeStatus from "./Tabs/ChangeStatus";
import VisitDays from "./Tabs/VisitDays";
import Fotos from "./Tabs/Fotos";
import Tour360 from "./Tabs/Tour360";

import { useDispatch, useSelector } from "react-redux";
import { getUserDetails } from "redux/user/actions";

const EditTabs = ({ PropertyId }) => {
  const dispatch = useDispatch();

  const [success, setSuccess] = useState(false);

  const propertyDetails = useSelector((state) => state?.propertyDetails);
  const { Property, loading } = propertyDetails;

  const UserId = Property?.UserId;

  React.useEffect(() => {
    if (UserId) {
      dispatch(getUserDetails(UserId));
    }
  }, [UserId, dispatch]);

  const Type = Property?.Type || "";
  const Purpose = Property?.Purpose || "";

  const propertyTypeCondition = Type.includes("HOUSE") && Type !== "PENTHOUSE";
  const propertyTypeConditionHouseOnly = Type === "HOUSE";

  const props = {
    PropertyId,
    Property,
    Purpose,
    propertyTypeCondition,
    propertyTypeConditionHouseOnly,
    setSuccess,
    success,
  };

  const rentCond = Purpose === "BOTH" || Purpose === "RENT";
  const sellCond = Purpose === "BOTH" || Purpose === "SELL";

  return (
    <RootList
      loading={loading}
      setSuccess={setSuccess}
      renderChildren={(value) => (
        <>
          <TabPanel value={value} index={0}>
            <TopForm {...props} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <AddressForm index={1} {...props} />
          </TabPanel>
          {rentCond && (
            <TabPanel value={value} index={2}>
              <RentConditions {...props} />
            </TabPanel>
          )}
          {sellCond && (
            <TabPanel value={value} index={3}>
              <SaleConditions {...props} />
            </TabPanel>
          )}
          <TabPanel value={value} index={4}>
            <InternalProperty {...props} />
          </TabPanel>
          {propertyTypeConditionHouseOnly === false && (
            <TabPanel value={value} index={5}>
              <BuildingDescription {...props} />
            </TabPanel>
          )}
          <TabPanel value={value} index={6}>
            <VisitDays index={6} {...props} />
          </TabPanel>
          <TabPanel value={value} index={9}>
            <ChangeStatus {...props} />
          </TabPanel>
          <TabPanel value={value} index={7}>
            <Tour360 {...props} />
          </TabPanel>
          <TabPanel value={value} index={8}>
            <Fotos {...props} />
          </TabPanel>
        </>
      )}
    >
      <LinkTab label="BÁSICO" value={0} />
      <LinkTab label="ENDEREÇO" value={1} />
      {rentCond && <LinkTab label="CONDIÇÕES DE ALUGUEL" value={2} />}
      {sellCond && <LinkTab label="CONDIÇÕES DE VENDA" value={3} />}
      <LinkTab label="PROPRIEDADE INTERNA" value={4} />
      {propertyTypeConditionHouseOnly === false && (
        <LinkTab label="DESCRIÇÃO DO EDIFÍCIO" value={5} />
      )}
      <LinkTab label="DIAS DE VISITA" value={6} />
      <LinkTab label="MUDAR DE STATUS" value={9} />
      <LinkTab label="TOUR 360" value={7} />
      <LinkTab label="FOTOS" value={8} />
    </RootList>
  );
};

export default EditTabs;
