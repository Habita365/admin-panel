import { formattedDate } from "utils";

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 90,
  },
  {
    field: "Name",
    headerName: "Nome",
    width: 220,
  },
  {
    field: "Email",
    headerName: "E-Mail",
    width: 200,
  },
  {
    field: "IdentificationNumber",
    headerName: "CPJ/CNPJ",
    width: 200,
  },
  {
    field: "PhoneNumber",
    headerName: "Celular",
    // sortable: false,
    width: 180,
  },
  // {
  //   field: "RegisteredBy",
  //   headerName: "Registrado por",
  //   // sortable: false,
  //   width: 200,
  // },
  {
    field: "IsLegalEntity",
    headerName: "Entidade legal",
    type: "boolean",
    width: 180,
    // sortable: false,
  },
  {
    field: "IsLandlord",
    headerName: "Proprietário",
    type: "boolean",
    width: 150,
    // sortable: false,
  },
  {
    field: "IsTenant",
    headerName: "Inquilino",
    type: "boolean",
    width: 150,
    // sortable: false,
  },
  {
    field: "LegalPersonId",
    headerName: "Representa PJ",
    type: "number",
    width: 180,
    // sortable: false,
  },
  {
    field: "UpdatedAt",
    headerName: "Atualizado em",
    width: 160,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "CreatedAt",
    headerName: "Criado em",
    width: 180,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
];

export default columns;
