import React, { useEffect } from "react";
import { getPersonDetails } from "redux/person/actions";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

import EditTabs from "./EditTabs";

const EditPerson = ({ match }) => {
  const dispatch = useDispatch();

  const PersonId = match.params.id;

  useEffect(() => {
    if (PersonId) {
      dispatch(getPersonDetails(PersonId));
    }
  }, [dispatch, PersonId]);

  return (
    <>
      <div>
        <EditTabs PersonId={PersonId} />
      </div>
    </>
  );
};

export default withRouter(EditPerson);
