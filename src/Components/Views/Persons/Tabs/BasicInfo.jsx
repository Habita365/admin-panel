import React from "react";
import TextField from "Components/Ui/Forms/TextField";
import TextMaskField from "Components/Ui/Forms/TextMaskField";
import PhoneInput from "Components/Ui/Forms/PhoneInput";
import withForm from "Components/Ui/Forms/withPersonForm";
import { identificationMask } from "utils";
import Button from "@mui/material/Button";

const BasicInfo = ({ control, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField name="Name" control={control} label="Nome" required />
        <TextField name="Email" control={control} label="Email" required />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextMaskField
          control={control}
          required
          label="CPF/CNPJ"
          mask={identificationMask}
          name="IdentificationNumber"
          readOnly
          w="full"
        />
        <PhoneInput
          name="PhoneNumber"
          control={control}
          label="Telefone"
          autoFocus={false}
          required
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withForm(BasicInfo);
