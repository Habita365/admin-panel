import React from "react";
import TextField from "Components/Ui/Forms/TextField";
import SelectField from "Components/Ui/Forms/SelectField";
import MaskTextField from "Components/Ui/Forms/MaskTextField";
import MenuItem from "@mui/material/MenuItem";
import DateField from "Components/Ui/Forms/DateField";
import withForm from "Components/Ui/Forms/withPersonForm";
import countries from "json/countries.json";
import Button from "@mui/material/Button";

import { professions } from "utils/options";

const Profile = ({ control, loading }) => {
  return (
    <div className="w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <MaskTextField
          name="Profile.RgNumber"
          control={control}
          mask="999.999.999-0"
          placeholder="00.000.000-0"
          label="RG"
          required
        />
        <TextField
          name="Profile.IssuingAuthority"
          label="Órg. Expedidor"
          placeholder="SSP-SP"
          control={control}
          required
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <DateField
          name="Profile.IssuingDate"
          control={control}
          disableFuture
          disablePast={false}
          label="Data de emissão"
          required
        />
        <DateField
          name="Profile.BirthDate"
          control={control}
          disableFuture
          disablePast={false}
          label="Data de nascimento"
          required
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField
          name="Profile.Nationality"
          required
          control={control}
          label="Nacionalidade"
        >
          {countries.map((country) => (
            <MenuItem
              value={country.name.toLowerCase()}
              key={country.name.toLowerCase()}
            >
              {country.name}
            </MenuItem>
          ))}
        </SelectField>
        <SelectField
          name="Profile.Gender"
          control={control}
          required
          label="Gênero"
        >
          <MenuItem value="MALE">Masculino</MenuItem>
          <MenuItem value="FEMALE">Feminino</MenuItem>
        </SelectField>
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField
          name="Profile.MaritalStatus"
          control={control}
          required
          label="Estado Civil"
        >
          <MenuItem value="SINGLE">Solteiro</MenuItem>
          <MenuItem value="MARRIED">Casado</MenuItem>
          <MenuItem value="SEPARATED">Separado</MenuItem>
          <MenuItem value="DIVORCED">Divorciado</MenuItem>
        </SelectField>
        <SelectField
          label="Profissão"
          name="Profile.Occupation"
          control={control}
        >
          {professions.map((profession) => (
            <MenuItem value={profession.value} key={profession.value}>
              {profession.label}
            </MenuItem>
          ))}
        </SelectField>
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withForm(Profile);
