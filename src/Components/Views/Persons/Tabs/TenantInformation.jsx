import React from "react";
import CurrencyNumber from "Components/Ui/Forms/CurrencyNumber";
import withForm from "Components/Ui/Forms/withPersonForm";
import Button from "@mui/material/Button";

const CompanyInformation = ({ control, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <CurrencyNumber
          name="TenantInformation.MonthlyIncome"
          control={control}
          label="Renda Mensal"
          required
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withForm(CompanyInformation);
