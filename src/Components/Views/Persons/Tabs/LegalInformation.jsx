import React from "react";
import TextField from "Components/Ui/Forms/TextField";
import SelectField from "Components/Ui/Forms/SelectField";
import TextMaskField from "Components/Ui/Forms/TextMaskField";
import MenuItem from "@mui/material/MenuItem";
import withForm from "Components/Ui/Forms/withPersonForm";
import { bankOption } from "./options";
import { identificationMask } from "utils";
import Button from "@mui/material/Button";

const LegalInformation = ({ control, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField control={control} name="LegalInformation.Bank" required>
          {bankOption.map((bank) => (
            <MenuItem key={bank.value} value={bank.value}>
              {bank.label}
            </MenuItem>
          ))}
        </SelectField>
        <TextField
          name="LegalInformation.Branch"
          control={control}
          label="Agência (com dígito)"
          required
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          name="LegalInformation.AccountNumber"
          control={control}
          label="Conta"
          required
        />
        <TextField
          name="LegalInformation.AccountName"
          control={control}
          label="Titular"
          required
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField
          control={control}
          name="LegalInformation.AccountType"
          required
        >
          <MenuItem value="CHECKING">Conta corrente</MenuItem>
          <MenuItem value="SAVINGS">Conta poupança</MenuItem>
        </SelectField>
        <TextMaskField
          control={control}
          required
          label="CPF/CNPJ"
          mask={identificationMask}
          name="LegalInformation.Cnpj"
          w="full"
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withForm(LegalInformation);
