export const bankOption = [
  {
    value: "001",
    label: "001 Banco do Brasil",
  },
  {
    value: "341",
    label: "341 Banco Itaú S.A.",
  },
  {
    value: "237",
    label: "237 Bradesco",
  },
  {
    value: "033",
    label: "033 Banco Santander",
  },
  {
    value: "104",
    label: "104 Caixa Econômica Federal",
  },
  {
    value: "399",
    label: "399 HSBC",
  },
  {
    value: "389",
    label: "389 Banco Mercantil Brasil",
  },
  {
    value: "041",
    label: "041 Banrisul",
  },
  {
    value: "422",
    label: "422 Banco Safra",
  },
  {
    value: "479",
    label: "479 Bank Boston",
  },
  {
    value: "047",
    label: "047 Banese",
  },
  {
    value: "756",
    label: "756 Bancoob",
  },
  {
    value: "004",
    label: "004 Banco do Nordeste",
  },
  {
    value: "453",
    label: "453 Banco Rural",
  },
  {
    value: "070",
    label: "070 BRB",
  },
  {
    value: "003",
    label: "003 Banco da Amazônia",
  },
  {
    value: "748",
    label: "748 Sicredi",
  },
  {
    value: "741",
    label: "741 BRP",
  },
  {
    value: "021",
    label: "021 Banestes",
  },
  {
    value: "027",
    label: "027 Banco Besc",
  },
  {
    value: "745",
    label: "745 Citibank",
  },
  {
    value: "084",
    label: "084 Unicred Uniprime Norte do Paraná",
  },
  {
    value: "087",
    label: "087 Unicred Central Santa Catarina",
  },
  {
    value: "090",
    label: "090 Unicred Central São Paulo",
  },
  {
    value: "091",
    label: "091 Unicred Central do Rio Grande do Sul",
  },
  {
    value: "099",
    label:
      "099 Uniprime Central - Central Interestadual de Cooperativas de Crédito Ltda.",
  },
  {
    value: "112",
    label: "112 Unicred Brasil Central",
  },
  {
    value: "085",
    label: "085 Cooperativa Central de Crédito Urbano-CECRED",
  },
  {
    value: "136",
    label: "136 Conf. Nacional das cooperativas centrais Unicreds",
  },
  {
    value: "098",
    label: "098 Credialiança Cooperativa de Crédito Rural",
  },
  {
    value: "077",
    label: "077 Banco Intermedium S.A.",
  },
  {
    value: "212",
    label: "212 Banco Original",
  },
  {
    value: "320",
    label: "320 Banco Industrial e Comercial S.A.",
  },
  {
    value: "749",
    label: "749 Banco Simples S.A.",
  },
  {
    value: "037",
    label: "037 Banpara",
  },
  {
    value: "260",
    label: "260 Nu Pagamentos S.A",
  },
  {
    value: "735",
    label: "735 Banco Neon S.A.",
  },
  {
    value: "097",
    label: "097 Cooperativa Central de Crédito Noroeste Brasileiro Ltda",
  },
  {
    value: "654",
    label: "654 Banco A.J.Renner S.A.",
  },
  {
    value: "133",
    label: "133 Cresol",
  },
  {
    value: "655",
    label: "655 Banco Votorantim S.A.",
  },
  {
    value: "010",
    label: "010 Credicoamo",
  },
  {
    value: "633",
    label: "633 Rendimento",
  },
  {
    value: "089",
    label: "089 CREDISAN",
  },
  {
    value: "637",
    label: "637 Sofisa",
  },
  {
    value: "208",
    label: "208 BTG PACTUAL",
  },
  {
    value: "048",
    label: "048 Banco Bamge S.A.",
  },
  {
    value: "336",
    label: "336 Banco C6 S.A.",
  },
  {
    value: "121",
    label: "121 Agibank",
  },
  {
    value: "318",
    label: "318 Banco BMG S.A.",
  },
  {
    value: "746",
    label: "746 Banco Modal S.A.",
  },
  {
    value: "092",
    label: "092 Brickell S.A. Crédito",
  },
  {
    value: "153",
    label: "153 Caixa Econômica Estadual do Rio Grande do Sul",
  },
  {
    value: "290",
    label: "290 PagSeguro Internet S.A",
  },
  {
    value: "218",
    label: "218 Banco BS2 S.A.",
  },
  {
    value: "102",
    label: "102 XP Investimentos S.A.",
  },
  {
    value: "707",
    label: "707 Banco Daycoval S.A.",
  },
  {
    value: "269",
    label: "269 HSBC Brasil Bancos de Investimentos",
  },
  {
    value: "197",
    label: "197 Stone Pagamentos S.A.",
  },
  {
    value: "623",
    label: "623 Banco Panamericano S.A.",
  },
  {
    value: "301",
    label: "301 BPP Instituição de Pagamentos S.A.",
  },
  {
    value: "114",
    label:
      "114 CECOOPES - Central das Coop. de econ. e cred. mútuo do Estado do ES",
  },
  {
    value: "279",
    label: "279 Cooperativa de Crédito Rural de Primavera do Leste (CCR)",
  },
  {
    value: "364",
    label: "364 Gerencianet pagamentos do Brasil",
  },
  {
    value: "323",
    label: "323 Mercadopago.com Representações LTDA",
  },
  {
    value: "069",
    label: "069 Banco Crefisa S.A.",
  },
  {
    value: "140",
    label: "140 Easynvest - Título Cv S.A.",
  },
  {
    value: "335",
    label: "335 Banco Digio S.A.",
  },
  {
    value: "582",
    label: "582 Unicred União",
  },
  {
    value: "403",
    label: "403 CORA Sociedade de Crédito Direto S.A.",
  },
  {
    value: "243",
    label: "243 Banco Máxima S.A.",
  },
  {
    value: "380",
    label: "380 PICPAY SERVICOS S.A",
  },
  {
    value: "292",
    label: "292 BS2 Distribuidora de Títulos e Valores Mobiliários S.A",
  },
  {
    value: "352",
    label: "352 TORO CTVM LTDA",
  },
  {
    value: "082",
    label: "082 Banco Topazio S.A.",
  },
  {
    value: "184",
    label: "184 BCO ITAÚ BBA S.A",
  },
  {
    value: "348",
    label: "348 Banco XP",
  },
  {
    value: "340",
    label: "340 Super Pagamentos e Administração de Meios Eletrônicos S.A.",
  },
  {
    value: "413",
    label: "413 BV S.A.",
  },
  {
    value: "329",
    label: "329 QI Sociedade de Crédito Direto S.A.",
  },
];
