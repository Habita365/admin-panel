import React from "react";
import TextField from "Components/Ui/Forms/TextField";
import withForm from "Components/Ui/Forms/withPersonForm";
import Button from "@mui/material/Button";

const CompanyInformation = ({ control, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          name="CompanyInformation.TradingName"
          control={control}
          label="Nome Fantasia"
          required
        />
        <TextField
          name="CompanyInformation.StateRegistration"
          control={control}
          label="Inscrição estadual"
          required
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField
          name="CompanyInformation.MunicipalRegistration"
          control={control}
          label="Inscrição municipal"
          required
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withForm(CompanyInformation);
