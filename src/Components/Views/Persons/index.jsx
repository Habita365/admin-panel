/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import { DataGrid } from "@mui/x-data-grid";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import columnsData from "./columns";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import DeletePersonPopup from "./Popups/DeletePersonPopup";
import { listPersons } from "redux/person/actions";
import useFetchList from "hooks/useFetchList";

const PREFIX = "Persons";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

const Persons = () => {
  const { dispatch, page, limit, setLimit, setPage, sortModel } = useFetchList(
    1,
    18
  );
  const [person, setPerson] = useState({});
  const [open, setOpen] = useState(false);

  const personList = useSelector((state) => state.personList);
  const { loading, Persons, TotalPersons } = personList;

  const columns = [
    ...columnsData,
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Link
          to={`/persons/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button startIcon={<EditIcon />}>Editar</Button>
          </Box>
        </Link>
      ),
    },
    {
      field: "Delete",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => {
        return (
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button
              onClick={() => {
                const paramsId = params?.id;

                const person = Persons.find((p) => {
                  return p.id === paramsId;
                });

                setPerson(person);

                setOpen(true);
              }}
              startIcon={<DeleteIcon />}
            >
              Excluir
            </Button>
          </Box>
        );
      },
    },
  ];

  const stringifiedSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    dispatch(listPersons({ page, limit, sortModel }));
  }, [dispatch, page, limit, stringifiedSortModel]);

  return (
    <Root>
      <DeletePersonPopup
        open={open}
        person={person}
        handleClose={() => {
          setOpen(false);
        }}
      />

      <div style={{ width: "100%", height: "100%" }}>
        <DataGrid
          autoHeight
          rows={Persons}
          columns={columns}
          density="compact"
          page={page - 1}
          onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
          onPageChange={(newPage) => setPage(newPage + 1)}
          pageSize={limit}
          paginationMode="server"
          rowCount={TotalPersons}
          rowsPerPageOptions={[limit]}
          loading={loading}
          checkboxSelection
          pagination
          disableSelectionOnClick
          // sortingMode="server"
          // sortModel={sortModel}
          // onSortModelChange={onSortModelChange}
        />
      </div>
    </Root>
  );
};

export default Persons;
