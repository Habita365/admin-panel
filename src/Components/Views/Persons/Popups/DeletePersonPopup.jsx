import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deletePerson, clearMessage } from "redux/person/actions";

import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import Button from "@mui/material/Button";

export default function DeletePersonPopup({ open, handleClose, person }) {
  const dispatch = useDispatch();

  const [confirmationText, setConfirmationText] = useState("");
  const [loading, setLoading] = useState(false);

  const confirmationValid = confirmationText === "CONFIRMAR";

  const personList = useSelector((state) => state.personList);
  const { personMessage, personStatus } = personList;

  const handleDelete = () => {
    dispatch(
      deletePerson(person?.id, () =>
        setLoading((prev) => {
          return !prev;
        })
      )
    );
  };

  const onClose = () => {
    setConfirmationText("");
    handleClose();
    dispatch(clearMessage());
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Excluir Imóvel</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deseja mesmo deletar o imóvel {person?.Id}?Essa ação não poderá ser
          desfeita.
        </DialogContentText>

        <TextField
          id="confirmation"
          autoComplete="off"
          label="Digite CONFIRMAR"
          type="text"
          variant="outlined"
          value={confirmationText}
          onChange={(e) => setConfirmationText(e.target.value)}
          error={!confirmationValid}
        />
      </DialogContent>
      {personMessage ? (
        <p
          className={`text-center my-6 font-semibold ${
            personStatus === "SUCCESS" ? "text-green-500" : "text-red-500"
          }`}
        >
          {personMessage}
        </p>
      ) : null}
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button disabled={!confirmationValid || loading} onClick={handleDelete}>
          EXCLUIR
        </Button>
      </DialogActions>
    </Dialog>
  );
}
