import React, { useState } from "react";
import TabPanel from "Components/Ui/TabPanel";
import LinkTab from "Components/Ui/LinkTab";
import RootList from "Components/Ui/RootList";

import AddressForm from "Components/Ui/Tabs/AddressForm";
import BasicInfo from "./Tabs/BasicInfo";
import CompanyInformation from "./Tabs/CompanyInformation";
import LegalInformation from "./Tabs/LegalInformation";
import Profile from "./Tabs/Profile";
import TenantInformation from "./Tabs/TenantInformation";
import withForm from "Components/Ui/Forms/withPersonForm";

import { useSelector } from "react-redux";

const Address = withForm(AddressForm);

const EditTabs = ({ PersonId }) => {
  const [success, setSuccess] = useState(false);

  const personDetails = useSelector((state) => state?.personDetails);
  const { Person, loading } = personDetails;

  const IsLegalEntity = Person?.IsLegalEntity;
  const IsTenant = Person?.IsTenant;
  const IsLegalRepresentative = !!Person?.LegalPersonId;

  const props = {
    PersonId,
    Person,
    setSuccess,
    success,
  };

  return (
    <RootList
      loading={loading}
      setSuccess={setSuccess}
      renderChildren={(value) => (
        <>
          <TabPanel value={value} index={0}>
            <BasicInfo {...props} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <Address index={2} {...props} />
          </TabPanel>
          {!IsLegalEntity && (
            <TabPanel value={value} index={1}>
              <Profile {...props} />
            </TabPanel>
          )}
          {IsTenant || IsLegalRepresentative ? null : (
            <TabPanel value={value} index={3}>
              <LegalInformation
                IsLegalEntity={IsLegalEntity}
                index={3}
                {...props}
              />
            </TabPanel>
          )}
          {IsLegalEntity && (
            <TabPanel value={value} index={4}>
              <CompanyInformation
                index={4}
                IsLegalEntity={IsLegalEntity}
                {...props}
              />
            </TabPanel>
          )}
          {IsTenant && (
            <TabPanel value={value} index={5}>
              <TenantInformation index={5} {...props} />
            </TabPanel>
          )}
        </>
      )}
    >
      <LinkTab label="BÁSICO" index={0} />
      <LinkTab label="ENDEREÇO" value={2} />
      {!IsLegalEntity && <LinkTab label="PERFIL" value={1} />}
      {IsTenant || IsLegalRepresentative ? null : (
        <LinkTab label="INFORMAÇÃO LEGAL" value={3} />
      )}
      {IsLegalEntity && <LinkTab label="INFORMAÇÃO EMPRESARIAL" value={4} />}
      {IsTenant && <LinkTab label="INFORMAÇÕES DO INQUILINO" value={5} />}
    </RootList>
  );
};

export default EditTabs;
