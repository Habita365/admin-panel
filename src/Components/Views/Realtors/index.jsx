/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import { DataGrid } from "@mui/x-data-grid";
import { Box } from "@mui/material";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";

import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import Button from "@mui/material/Button";
import columnsData from "./columns";
import CreateUserPopup from "./Popup/CreateUserPopup";
import useFetchList from "hooks/useFetchList";
import { listUsers } from "redux/realtor/actions";
import { queryFunc } from "../../../utils";

const PREFIX = "Realtors";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

function Realtors() {
  const history = useHistory();
  const { dispatch, page, limit, setLimit, setPage, sortModel, setSortModel } =
    useFetchList(1, 15);

  const [open, setOpen] = useState(false);

  const realtorList = useSelector((state) => state?.realtorList);
  const { loading, error, users, TotalRealtors } = realtorList;

  const stringSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    if (sortModel.length > 0) {
      dispatch(listUsers({ page, limit, sortModel }));
    }
  }, [dispatch, page, stringSortModel, limit]);

  useEffect(() => {
    const queryObj = queryFunc(sortModel, page, limit);

    if (queryObj) {
      const query = new URLSearchParams(queryObj);

      history.push({
        search: query.toString(),
      });
    }
  }, [page, limit, stringSortModel]);

  // A proper component will be made to display error
  const columns = [
    ...columnsData,
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      //
      renderCell: (params) => (
        <Link
          to={`/users/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button disabled startIcon={<EditIcon />}>
              EDIT
            </Button>
          </Box>
        </Link>
      ),
    },
  ];

  const onSortModelChange = (model) => {
    if (JSON.stringify(model) !== stringSortModel) {
      setSortModel(model);
    }
  };

  if (error) return <p>{error}</p>;

  return (
    <Root className="">
      <CreateUserPopup open={open} onClose={() => setOpen(false)} />
      <Box display="flex" justifyContent="flex-end" m={1}>
        <Button mr={4} startIcon={<AddIcon />} onClick={() => setOpen(true)}>
          CREATE
        </Button>
      </Box>

      <DataGrid
        rows={users}
        columns={columns}
        density="compact"
        autoHeight
        checkboxSelection
        disableSelectionOnClick
        loading={loading}
        pagination
        page={page - 1}
        onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
        onPageChange={(newPage) => setPage(newPage + 1)}
        pageSize={limit}
        paginationMode="server"
        rowsPerPageOptions={[limit]}
        rowCount={TotalRealtors}
        sortingMode="server"
        onSortModelChange={onSortModelChange}
        sortModel={sortModel}
      />
    </Root>
  );
}

export default Realtors;
