import { formattedDate } from "utils";

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 280,
    sortable: false,
  },
  {
    field: "Name",
    headerName: "Name",
    width: 220,
  },
  {
    field: "Email",
    headerName: "E-Mail",
    width: 150,
  },
  {
    field: "Document.Cpf",
    headerName: "CPF",
    width: 150,
    valueFormatter: (params) => params.row?.Document?.Cpf,
    sortable: false,
  },
  {
    field: "CreciSp",
    headerName: "CreciSp",
    width: 150,
    sortable: false,
  },
  {
    field: "PhoneNumber",
    headerName: "Celular",
    width: 150,
    sortable: false,
  },
  {
    field: "UpdatedAt",
    headerName: "Atualizado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "CreatedAt",
    headerName: "Criado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
];

export default columns;
