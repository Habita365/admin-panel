import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import TextField from "Components/Ui/Forms/TextField";
import MaskTextField from "Components/Ui/Forms/MaskTextField";
import PhoneInput from "Components/Ui/Forms/PhoneInput";
import axios from "axios";

import { listUsers } from "redux/realtor/actions";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";

const backend = process.env.REACT_APP_APP_HABITA_USER_ENDPOINT;

const CreateUserPopup = ({ open, onClose }) => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const { control, handleSubmit } = useForm();

  const onSubmit = async (data) => {
    setLoading(true);
    setError(false);
    setSuccess(false);
    setMessage("");

    try {
      await axios.post(`${backend}/user/realtor`, data, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      dispatch(listUsers());

      setSuccess(true);
    } catch (err) {
      const message = err?.response?.data?.message;
      if (message) {
        if (Array.isArray(message)) {
          setMessage(message.join(", "));
        } else {
          setMessage(message);
        }
      } else {
        setMessage("Um erro ocorreu");
      }
      setError(true);
    }
    setLoading(false);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Alterar o status da propriedade</DialogTitle>
      <DialogContent>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
            <TextField name="Name" control={control} label="Nome" />
            <TextField
              name="Email"
              control={control}
              label="E-mail"
              type="email"
            />
          </div>
          <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
            <TextField
              name="Password"
              control={control}
              type="password"
              label="Senha"
            />
            <MaskTextField
              name="Document.Cpf"
              control={control}
              label="CPF"
              mask="999.999.999-99"
            />
          </div>
          <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
            <MaskTextField
              control={control}
              name="CreciSp"
              label="CreciSp"
              mask="999999-a"
            />
            <PhoneInput
              name="PhoneNumber"
              control={control}
              label="Telefone"
              autoFocus={false}
              required
            />
          </div>
          <DialogActions style={{ justifyContent: "space-between" }}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              disabled={loading}
            >
              Enviar
            </Button>
            <Button type="button" onClick={onClose}>
              Cancel
            </Button>
          </DialogActions>
        </form>
        <div className="my-4">
          {success && (
            <div>
              <p className="text-lg text-center font-semibold">
                Sua solicitação foi bem-sucedida
              </p>
            </div>
          )}
          {error && (
            <div>
              <p className="text-lg text-center text-red-400 font-semibold">
                {message}
              </p>
            </div>
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default CreateUserPopup;
