import React, { useEffect } from "react";
import { getVisitDetails } from "redux/visit/actions";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

import EditVisitTabs from "./EditTabs";

const EditVisit = ({ match }) => {
  const dispatch = useDispatch();

  const VisitId = match.params.id;

  useEffect(() => {
    if (VisitId) {
      dispatch(getVisitDetails(VisitId));
    }
  }, [dispatch, VisitId]);
  return (
    <>
      <div>
        <EditVisitTabs />
      </div>
    </>
  );
};

export default withRouter(EditVisit);
