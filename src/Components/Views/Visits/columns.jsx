import { formattedDate } from "utils";

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 90,
    sortable: false,
  },
  {
    field: "VisitDate",
    headerName: "Data",
    width: 120,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "PropertyId",
    headerName: "Imóvel",
    width: 120,
    sortable: false,
  },
  {
    field: "TimeSlot",
    headerName: "Horário",
    width: 150,
    sortable: false,
  },
  {
    field: "Type",
    headerName: "Tipo",
    width: 120,
    sortable: false,
  },

  {
    field: "RealtorId",
    headerName: "Corretor",
    width: 150,
    sortable: false,
  },
];
export default columns;
