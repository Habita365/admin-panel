import React from "react";
import TextDisplayField from "Components/Ui/Forms/TextDisplayField";

import { useSelector } from "react-redux";

const Basic = () => {
  const visit = useSelector((state) => state?.visitDetails.visit);
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField
          label="Data da visita"
          name="VisitDate"
          content={visit.VisitDate}
        />
        <TextDisplayField
          label="Visite o horário"
          name="TimeSlot"
          content={visit.TimeSlot}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextDisplayField
          label="Tempos remarcados"
          name="TimesResheduled"
          content={visit.TimesResheduled}
        />
        <TextDisplayField label="Status" name="Status" content={visit.Status} />
      </div>
    </div>
  );
};

export default Basic;
