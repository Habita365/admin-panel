import React from "react";
import TabPanel from "Components/Ui/TabPanel";
import LinkTab from "Components/Ui/LinkTab";
import Basic from "./Tabs/Basic";
import PropertyInfo from "Components/Ui/Tabs/PropertyInfo";
import User from "Components/Ui/Tabs/User";
import RootList from "Components/Ui/RootList";

import { useSelector } from "react-redux";

const EditVisitTabs = () => {
  const { loading, property, user, ownerUser } = useSelector(
    (state) => state?.visitDetails
  );

  return (
    <RootList
      loading={loading}
      renderChildren={(value) => (
        <>
          <TabPanel value={value} index={0}>
            <Basic />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <PropertyInfo property={property} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <User user={user} />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <User user={ownerUser} />
          </TabPanel>
        </>
      )}
    >
      <LinkTab label="BÁSICO" index={0} />
      <LinkTab label="INFORMAÇÃO DE PROPRIEDADE" index={1} />
      <LinkTab label="VISITANTE" index={2} />
      <LinkTab label="PROPRIETÁRIO" index={3} />
    </RootList>
  );
};

export default EditVisitTabs;
