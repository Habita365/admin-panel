/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import { DataGrid } from "@mui/x-data-grid";
import Switch from "@mui/material/Switch";
import { useSelector } from "react-redux";
import columnsData from "./columns";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import FormControlLabel from "@mui/material/FormControlLabel";

import EditIcon from "@mui/icons-material/Edit";
import EditAttributes from "@mui/icons-material/EditAttributes";
import { listVisits } from "redux/visit/actions";
import { Link, useHistory } from "react-router-dom";
import AssignRealtorPopup from "./Popups/AssignRealtorPopup";
import ChangeStatusPopup from "./Popups/ChangeStatusPopup";
import ReschedulePopup from "./Popups/ReschedulePopup";
import { queryFunc } from "utils";
import useFetchList from "hooks/useFetchList";
import { listUsers } from "redux/realtor/actions";

const PREFIX = "index";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

const Visits = () => {
  const history = useHistory();
  const { dispatch, page, limit, setLimit, setPage, sortModel, setSortModel } =
    useFetchList(1, 18);

  const [visit, setVisit] = useState({});
  const [openStatus, setOpenStatus] = useState(false);
  const [openAssign, setOpenAssign] = useState(false);
  const [openReschedule, setOpenReschedule] = useState(false);
  const [IsPending, setIsPending] = useState(true);

  const visitList = useSelector((state) => state?.visitList);
  const { loading, Visits, error, TotalVisits } = visitList;

  const realtorList = useSelector((state) => state?.realtorList);

  const { users } = realtorList;

  function openModal(params) {
    const paramsId = params?.id;

    const visit = Visits.find((p) => {
      return p.id === paramsId;
    });

    setVisit(visit);
  }

  const columns = [
    ...columnsData,
    {
      field: "Reshedule visit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 300,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => {
        const VisitDate = params.row?.VisitDate;
        const future = VisitDate
          ? new Date(VisitDate).getTime() > Date.now()
          : false;

        return future ? (
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button
              onClick={() => {
                openModal(params);

                setOpenReschedule(true);
              }}
              startIcon={<EditAttributes />}
            >
              Agendar visita
            </Button>
          </Box>
        ) : null;
      },
    },
    {
      field: "Assign realtor",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 300,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Box display="flex" style={{ margin: "0 auto" }}>
          <Button
            onClick={() => {
              openModal(params);

              setOpenAssign(true);
            }}
            startIcon={<EditAttributes />}
          >
            Atribuir corretor
          </Button>
        </Box>
      ),
    },
    {
      field: "Status",
      headerName: "Status",
      width: 120,
    },
    {
      field: "Change status",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 200,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Box display="flex" style={{ margin: "0 auto" }}>
          <Button
            onClick={() => {
              openModal(params);

              setOpenStatus(true);
            }}
            startIcon={<EditAttributes />}
          >
            Mudar o status
          </Button>
        </Box>
      ),
    },
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      // editable: true,
      renderCell: (params) => (
        <Link
          to={`/visits/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button startIcon={<EditIcon />}>EDIT</Button>
          </Box>
        </Link>
      ),
    },
  ];

  const stringifiedSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    dispatch(listVisits({ page, limit, sortModel, IsPending }));
  }, [dispatch, page, stringifiedSortModel, limit, IsPending]);

  useEffect(() => {
    dispatch(listUsers());
  }, [dispatch]);

  useEffect(() => {
    const queryObj = queryFunc(sortModel, page, limit);

    if (queryObj) {
      const query = new URLSearchParams(queryObj);

      history.push({
        search: query.toString(),
      });
    }
  }, [page, limit, stringifiedSortModel]);

  if (error) return <p>{error}</p>;

  const VisitDate = visit?.VisitDate;

  const future = VisitDate ? new Date(VisitDate).getTime() > Date.now() : false;

  const onSortModelChange = (model) => {
    if (JSON.stringify(model) !== JSON.stringify(sortModel)) {
      setSortModel(model);
    }
  };

  return (
    <Root>
      <ChangeStatusPopup
        open={openStatus}
        future={future}
        visit={visit}
        handleClose={() => {
          setOpenStatus(false);
        }}
      />
      <AssignRealtorPopup
        open={openAssign && users?.length > 0}
        visit={visit}
        users={users}
        handleClose={() => setOpenAssign(false)}
      />

      <ReschedulePopup
        open={openReschedule}
        visit={visit}
        handleClose={() => {
          setOpenReschedule(false);
        }}
      />
      <Box display="flex" justifyContent="flex-end" className="mb-4">
        <FormControlLabel
          control={
            <Switch
              checked={IsPending}
              onChange={(e) => setIsPending(e.target.checked)}
            />
          }
          label="Mostrar apenas visitas futuras"
        />
      </Box>
      <div style={{ height: "65vh", width: "100%" }}>
        <DataGrid
          autoHeight
          rows={Visits}
          columns={columns}
          density="compact"
          rowCount={TotalVisits}
          checkboxSelection
          disableSelectionOnClick
          pagination
          loading={loading}
          page={page - 1}
          pageSize={limit}
          rowsPerPageOptions={[limit]}
          onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
          onPageChange={(newPage) => setPage(newPage + 1)}
          paginationMode="server"
          sortingMode="server"
          sortModel={sortModel}
          onSortModelChange={onSortModelChange}
        />
      </div>
    </Root>
  );
};

export default React.memo(Visits);
