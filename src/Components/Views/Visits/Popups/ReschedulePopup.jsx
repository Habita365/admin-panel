import React, { useState } from "react";
import { styled } from "@mui/material/styles";
import { useDispatch, useSelector } from "react-redux";
import { format, isFuture, isSameDay } from "date-fns";
import { logout } from "redux/auth/actions";
import { listVisits } from "redux/visit/actions";

import axios from "axios";
import fetcher from "utils/fetcher";
import useSwr from "swr";
import DateField from "Components/Ui/Forms/DateField";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";

const PREFIX = "ReschedulePopup";

const classes = {
  root: `${PREFIX}-root`,
  dateFieldRoot: `${PREFIX}-dateFieldRoot`,
};

const StyledDialog = styled(Dialog)({
  [`& .${classes.root}`]: {
    display: "block",
    marginRight: "auto",
    marginLeft: "auto",
  },
  [`& .${classes.dateFieldRoot}`]: {
    marginTop: "0",
    marginBottom: "0",
  },
});

const backend = process.env.REACT_APP_APP_HABITA_VISIT_ENDPOINT;

export default function ReschedulePopup({ open, handleClose, visit }) {
  const propertyId = visit?.PropertyId;
  const timeslot = visit?.TimeSlot;

  const visitDate = visit?.Date;
  const date = visitDate ? new Date(visitDate) : new Date();

  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  const visitList = useSelector((state) => state?.visitList);
  const { visitMessage, visitStatus } = visitList;

  const { data, error } = useSwr(
    propertyId ? [`${backend}/visit/timeslots/${propertyId}`] : null,
    fetcher
  );

  const loadingData = !data && !error;

  const availableTimeSlotsData = data?.AvailableTimeSlots;
  const dateFormatDefault = date ? format(date, "yyyy/MM/dd") : null;
  const addedTimeSlot = dateFormatDefault
    ? availableTimeSlotsData?.map((dateSlot) => {
        if (dateSlot.Date === dateFormatDefault) {
          return {
            ...dateSlot,
            Timings: [...dateSlot.Timings, timeslot],
          };
        }

        return dateSlot;
      })
    : availableTimeSlotsData;

  const availableTimeSlots = availableTimeSlotsData
    ? addedTimeSlot
        .filter((t) => {
          const isFutureDate = isFuture(new Date(t.Date));
          return t.Timings.length > 0 && t.Timings?.[0] !== "" && isFutureDate;
        })
        .map((t) => {
          return {
            ...t,
            Date: new Date(t.Date),
          };
        })
    : [];

  const availableTimeSlotsDate = availableTimeSlots[0]?.Date;

  const [selectedDate, setSelectedDate] = useState(
    date
      ? date
      : availableTimeSlotsDate
      ? new Date(availableTimeSlotsDate)
      : new Date()
  );
  const [selectedSlot, setSelectedSlot] = useState("");

  const currentDateTimeslots = availableTimeSlots.find((t) =>
    isSameDay(selectedDate, t.Date)
  );

  const availableMorningTimes = currentDateTimeslots?.Timings || [];

  const isInputValid = selectedSlot.length > 0;

  const onChangeTimeSlot = (e) => {
    const value = e.target.value;

    setSelectedSlot(value);
  };

  const onChangeDate = (date) => {
    if (date) {
      setSelectedDate(date);
    }
  };

  const shouldDisableDate = (_date) => {
    let shouldDisableDate = true;
    availableTimeSlots.forEach((timeSlot) => {
      if (isSameDay(new Date(timeSlot.Date), _date)) {
        shouldDisableDate = false;
        return;
      }

      if (date) {
        if (isSameDay(date, _date)) {
          shouldDisableDate = false;
        }
      }
    });
    return shouldDisableDate;
  };

  // const handleVisitStatusChange = () => {
  //   dispatch(changeVisitStatus(visit?.id, Status, () => setLoading((prev) => {
  //     return !prev
  //   })))
  // };

  const onClose = () => {
    handleClose();
  };

  const onSubmit = async () => {
    setLoading(true);

    try {
      const data = {
        TimeSlot: selectedSlot,
        VisitDate: format(selectedDate, "yyyy/MM/dd"),
      };

      await axios.patch(
        `${process.env.NEXT_PUBLIC_HABITA_VISIT_URL}/visit/${propertyId}/reschedule`,
        data,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );

      dispatch(listVisits());
    } catch (err) {
      if (err?.response?.status === 401) {
        dispatch(logout());
      }

      const errResMessage = err?.response?.data?.message;

      const message = errResMessage ? errResMessage : err.message;
      if (message === "Not authorized, token failed") {
        dispatch(logout());
      }
    }
    setLoading(false);
  };

  return (
    <StyledDialog open={open} onClose={onClose}>
      <DialogTitle>Reagendar visita</DialogTitle>
      <DialogContent
        style={{
          paddingTop: "0",
          paddingBottom: "0",
        }}
      >
        {loadingData ? null : (
          <form onSubmit={onSubmit}>
            <div className="mb-4">
              <DateField
                name="VisitDate"
                className={classes.dateFieldRoot}
                shouldDisableDate={shouldDisableDate}
                onChange={onChangeDate}
                value={selectedDate}
              />
            </div>
            <div className="mb-4">
              <FormControl className="w-full" component="fieldset">
                <FormLabel className="mb-3">
                  {`Período disponível em ${format(
                    selectedDate,
                    "dd/MM/yyyy"
                  )}`}
                </FormLabel>
                <FormGroup style={{ flexDirection: "row" }}>
                  {availableMorningTimes.map((time, i) => {
                    return (
                      <FormControlLabel
                        key={i}
                        control={
                          <Radio
                            value={time}
                            name="TimeSlot"
                            onChange={onChangeTimeSlot}
                          />
                        }
                        label={time}
                      />
                    );
                  })}
                </FormGroup>
              </FormControl>
            </div>
            <Button
              disabled={loading || !isInputValid}
              color="primary"
              type="submit"
              variant="contained"
              // className={classes.root}
            >
              MUDAR O STATUS
            </Button>
          </form>
        )}
      </DialogContent>
      {visitMessage ? (
        <p
          className={`text-center my-6 font-semibold ${
            visitStatus === "SUCCESS" ? "text-green-500" : "text-red-500"
          }`}
        >
          {visitMessage}
        </p>
      ) : null}
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </StyledDialog>
  );
}
