import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeVisitStatus, clearMessage } from "redux/visit/actions";

import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

export default function ChangeStatusPopup({
  open,
  handleClose,
  visit,
  future,
}) {
  const dispatch = useDispatch();

  const [Status, setStatus] = useState("");
  const [loading, setLoading] = useState(false);

  const visitList = useSelector((state) => state.visitList);
  const { visitMessage, visitStatus } = visitList;

  const handleVisitStatusChange = () => {
    dispatch(
      changeVisitStatus(visit?.id, Status, () =>
        setLoading((prev) => {
          return !prev;
        })
      )
    );
  };

  useEffect(() => {
    const AdStatus = visit?.AdStatus;

    if (AdStatus) {
      setStatus(AdStatus);
    }
  }, [visit]);

  const onClose = () => {
    handleClose();
    setStatus("");
    dispatch(clearMessage());
  };

  const handleChange = (e) => {
    setStatus(e.target.value);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Alterar o status da visita</DialogTitle>
      <DialogContent>
        <FormControl fullWidth variant="outlined">
          <InputLabel id="status-input-label">Status</InputLabel>
          <Select
            labelId="status-input-label"
            id="status-input"
            value={Status}
            label="Status"
            onChange={handleChange}
          >
            {future && <MenuItem value="CANCELLED">Cancelado</MenuItem>}
            {!future && <MenuItem value="CONCLUDED">Concluído</MenuItem>}
            {!future && <MenuItem value="NO_SHOW">Sem show</MenuItem>}
          </Select>
        </FormControl>
      </DialogContent>
      {visitMessage ? (
        <p
          className={`text-center my-6 font-semibold ${
            visitStatus === "SUCCESS" ? "text-green-500" : "text-red-500"
          }`}
        >
          {visitMessage}
        </p>
      ) : null}
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button disabled={loading} onClick={handleVisitStatusChange}>
          MUDAR O STATUS
        </Button>
      </DialogActions>
    </Dialog>
  );
}
