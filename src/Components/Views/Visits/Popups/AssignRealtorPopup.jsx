import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeVisitRealtor, clearMessage } from "redux/visit/actions";

import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

export default function AssignRealtorPopup({
  open,
  handleClose,
  visit,
  users,
}) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [RealtorId, setRealtorId] = useState("");

  const visitList = useSelector((state) => state?.visitList);
  const { visitMessage, visitStatus } = visitList;

  const handlevisitStatusChange = () => {
    dispatch(
      changeVisitRealtor(visit?.id, RealtorId, () =>
        setLoading((prev) => {
          return !prev;
        })
      )
    );
  };

  useEffect(() => {
    const RealtorId = visit?.RealtorId;

    if (RealtorId) {
      setRealtorId(RealtorId);
    }
  }, [visit]);

  const onClose = () => {
    handleClose();
    setRealtorId("");
    dispatch(clearMessage());
  };

  const handleChange = (e) => {
    setRealtorId(e.target.value);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Escolha corretor de imóveis</DialogTitle>
      <DialogContent>
        <FormControl fullWidth variant="outlined">
          <InputLabel id="status-input-label">Name</InputLabel>
          <Select
            labelId="status-input-label"
            id="status-input"
            value={RealtorId}
            label="Status"
            onChange={handleChange}
          >
            {users.map((user) => (
              <MenuItem value={user.id} key={user.id}>
                {user.Name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </DialogContent>
      {visitMessage ? (
        <p
          className={`text-center my-6 font-semibold ${
            visitStatus === "SUCCESS" ? "text-green-500" : "text-red-500"
          }`}
        >
          {visitMessage}
        </p>
      ) : null}
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button disabled={loading} onClick={handlevisitStatusChange}>
          ATRIBUIR CORRETOR DE IMÓVEIS
        </Button>
      </DialogActions>
    </Dialog>
  );
}
