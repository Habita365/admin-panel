import React from "react";
// import { connect } from "react-redux";
import { Avatar, Box, Button, Grid, Typography } from "@mui/material";
import { login } from "redux/auth/actions";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { theme } from "../../../MuiTheme";

import TextField from "Components/Ui/Forms/TextField";
import Loader from "Components/Ui/Loader";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

const styles = {
  wrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    gap: "8px",
    margin: "64px 16px",
  },
};

const Index = () => {
  const { control, handleSubmit } = useForm();

  const onSubmit = (data) => dispatch(login(data));
  const dispatch = useDispatch();

  const auth = useSelector((state) => state?.auth);
  const { loading, error } = auth;
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <div style={styles.wrapper}>
          <Avatar style={{ backgroundColor: theme.palette.secondary.main }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography variant="h5" style={{ textAlign: "center" }}>
            Login
          </Typography>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Box my={2}>
                <TextField
                  required
                  id="outlined-required"
                  label="Email"
                  variant="outlined"
                  control={control}
                  name="email"
                />
              </Box>
              <Box my={2}>
                <TextField
                  required
                  id="outlined-required"
                  label="Password"
                  variant="outlined"
                  type="password"
                  control={control}
                  name="password"
                />
              </Box>
              {error && (
                <Typography variant="p" color="error">
                  {error}
                </Typography>
              )}
              <Button
                variant="contained"
                size="large"
                color="primary"
                type="submit"
              >
                Login
              </Button>
            </Grid>
          </form>
        </div>
      )}
    </>
  );
};

export default Index;
