import React, { useEffect } from "react";
import { getUserDetails } from "redux/user/actions";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import EditTabs from "./EditTabs";

const EditUser = ({ match }) => {
  const dispatch = useDispatch();

  const UserId = match.params.id;
  // const userData ={...user?}
  useEffect(() => {
    if (UserId) {
      dispatch(getUserDetails(UserId));
    }
  }, [dispatch, UserId]);
  return (
    <>
      <div>
        <EditTabs UserId={UserId} />
      </div>
    </>
  );
};

export default withRouter(EditUser);
