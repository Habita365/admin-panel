import { formattedDate } from "utils";

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 90,
  },
  {
    field: "Name",
    headerName: "Nome",
    width: 220,
  },
  {
    field: "Email",
    headerName: "E-Mail",
    width: 150,
  },
  {
    field: "DocumentCpf",
    headerName: "CPF/CNPJ",
    width: 150,
    sortable: false,
  },
  {
    field: "PhoneNumber",
    headerName: "Celular",
    width: 150,
    sortable: false,
  },
  {
    field: "UpdatedAt",
    headerName: "Atualizado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "CreatedAt",
    headerName: "Criado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
];

export default columns;
