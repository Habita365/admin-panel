import React from "react";
import SelectField from "Components/Ui/Forms/SelectField";
import TextField from "Components/Ui/Forms/TextField";
import MenuItem from "@mui/material/MenuItem";
import { genderOptions, maritialStatusoptions } from "./options";
import withUserForm from "Components/Ui/Forms/withUserForm";
import Button from "@mui/material/Button";

const Complementary = ({ control, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField name="Gender" label="Gênero" control={control}>
          {genderOptions.map(({ id, name }) => (
            <MenuItem value={id} key={id}>
              {name}
            </MenuItem>
          ))}
        </SelectField>
        <SelectField
          name="MaritalStatus"
          label="Estadio Civil"
          control={control}
        >
          {maritialStatusoptions.map(({ id, name }) => (
            <MenuItem value={id} key={id}>
              {name}
            </MenuItem>
          ))}
        </SelectField>
      </div>
      <div className="flex items-center gap-4 mb-4">
        <TextField name="Document.RgNumber" label="RG" control={control} />
        <TextField
          name="Document.OrgExp"
          label="Orgão expeditor"
          control={control}
        />
      </div>
      <div className="flex items-center gap-4 mb-4">
        <TextField
          name="Document.ProofOfAddress"
          label="Prova de endereço"
          control={control}
        />
        <TextField
          name="Document.RgImage"
          label="Imagem Rg"
          control={control}
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withUserForm(Complementary);
