import React from "react";
import withUserForm from "Components/Ui/Forms/withUserForm";
import SwitchInput from "Components/Ui/Forms/SwitchInput";
import Button from "@mui/material/Button";

const TopForm = ({ control, data, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SwitchInput
          control={control}
          name="IdentityValidated"
          value={data?.IdentityValidated}
          label="Identidade validada"
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withUserForm(TopForm);
