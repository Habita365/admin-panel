export const typeOptions = [
  { id: "USER", name: "Usuário comum" },
  { id: "ADMIN", name: "Usuário Administrator" },
];

export const genderOptions = [
  { id: "MALE", name: "Masculino" },
  { id: "FEMALE", name: "Feminino" },
  { id: "OTHER", name: "Outro" },
];

export const maritialStatusoptions = [
  { id: "MARRIED", name: "Casado" },
  { id: "SINGLE", name: "Solteiro" },
  { id: "SEPARATED", name: "Separado" },
  { id: "DIVORCED", name: "Divorciado" },
];
