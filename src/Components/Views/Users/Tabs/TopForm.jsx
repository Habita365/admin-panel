import React from "react";
import SelectField from "Components/Ui/Forms/SelectField";
import TextField from "Components/Ui/Forms/TextField";
import PhoneInput from "Components/Ui/Forms/PhoneInput";
import MaskTextField from "Components/Ui/Forms/MaskTextField";
import DateField from "Components/Ui/Forms/DateField";
import MenuItem from "@mui/material/MenuItem";
import { typeOptions } from "./options";
import withUserForm from "Components/Ui/Forms/withUserForm";
import Button from "@mui/material/Button";

const TopForm = ({ control, loading }) => {
  return (
    <div className="w-full lg:w-1/2">
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <SelectField name="Type" label="Role" control={control}>
          {typeOptions.map(({ id, name }) => (
            <MenuItem value={id} key={id}>
              {name}
            </MenuItem>
          ))}
        </SelectField>
        <DateField
          name="BirthDate"
          label="Data de nascimento"
          control={control}
        />
      </div>
      <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
        <TextField name="Name" label="Nome" control={control} />
        <TextField name="Email" label="E-mail" control={control} />
      </div>
      <div className="flex items-center gap-4">
        <MaskTextField
          name="Document.Cpf"
          control={control}
          label="CPF"
          mask="999.999.999-99"
        />
        <PhoneInput
          name="PhoneNumber"
          control={control}
          label="Telefone"
          autoFocus={false}
          required
        />
      </div>
      <div className="mt-4">
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={loading}
        >
          Enviar
        </Button>
      </div>
    </div>
  );
};

export default withUserForm(TopForm);
