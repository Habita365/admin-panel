import React, { useState } from "react";
import TabPanel from "Components/Ui/TabPanel";
import LinkTab from "Components/Ui/LinkTab";
import RootList from "Components/Ui/RootList";
import withForm from "Components/Ui/Forms/withUserForm";
import AddressForm from "Components/Ui/Tabs/AddressForm";

import TopForm from "./Tabs/TopForm";
import Documentos from "./Tabs/Documentos";
import Complementary from "./Tabs/Complementary";

import { useSelector } from "react-redux";

const Address = withForm(AddressForm);

const EditTabs = ({ UserId }) => {
  const [success, setSuccess] = useState(false);

  const userDetails = useSelector((state) => state?.userDetails);
  const { User, loading } = userDetails;

  const props = {
    UserId,
    User,
    success,
    setSuccess,
  };

  return (
    <RootList
      loading={loading}
      setSuccess={setSuccess}
      renderChildren={(value) => (
        <>
          <TabPanel value={value} index={0}>
            <TopForm {...props} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Complementary {...props} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <Address index={2} userMode {...props} />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <Documentos {...props} />
          </TabPanel>
        </>
      )}
    >
      <LinkTab label="BÁSICO" index={0} />
      <LinkTab label="COMPLEMENTAR" index={1} />
      <LinkTab label="ENDEREÇO" index={2} />
      <LinkTab label="DOCUMENTOS" index={3} />
    </RootList>
  );
};

export default EditTabs;
