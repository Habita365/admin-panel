/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { styled } from "@mui/material/styles";
import { DataGrid } from "@mui/x-data-grid";
import EditIcon from "@mui/icons-material/Edit";
import { Box } from "@mui/material";

import Button from "@mui/material/Button";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { listUsers } from "redux/user/actions";
import { queryFunc } from "utils";
import useFetchList from "hooks/useFetchList";
import columnsData from "./columns";

const PREFIX = "Index";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

function Index() {
  const history = useHistory();
  const { dispatch, page, limit, setLimit, setPage, sortModel, setSortModel } =
    useFetchList(1, 15);

  const userList = useSelector((state) => state?.userList);
  const { loading, error, users, TotalUsers } = userList;

  const stringifiedSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    dispatch(listUsers({ page, limit, sortModel }));
  }, [dispatch, page, stringifiedSortModel, limit]);

  useEffect(() => {
    const queryObj = queryFunc(sortModel, page, limit);

    if (queryObj) {
      const query = new URLSearchParams(queryObj);

      history.push({
        search: query.toString(),
      });
    }
  }, [page, limit, stringifiedSortModel]);

  // A proper component will be made to display error
  if (error) return <p>{error}</p>;
  const columns = [
    ...columnsData,
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Link
          to={`/users/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button startIcon={<EditIcon />}>EDITAR</Button>
          </Box>
        </Link>
      ),
    },
  ];

  const onSortModelChange = (model) => {
    if (JSON.stringify(model) !== stringifiedSortModel) {
      setSortModel(model);
    }
  };

  return (
    <Root className="">
      <DataGrid
        rows={users}
        columns={columns}
        density="compact"
        autoHeight
        loading={loading}
        checkboxSelection
        disableSelectionOnClick
        pagination
        page={page - 1}
        pageSize={limit}
        rowsPerPageOptions={[limit]}
        onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
        onPageChange={(newPage) => setPage(newPage + 1)}
        paginationMode="server"
        rowCount={TotalUsers}
        sortingMode="server"
        onSortModelChange={onSortModelChange}
        sortModel={sortModel}
      />
    </Root>
  );
}

export default Index;
