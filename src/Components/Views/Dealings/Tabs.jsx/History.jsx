import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Stack from "@mui/material/Stack";
import React from "react";
import { formattedDate } from "utils";

const OfferCard = ({ offer }) => {
  return (
    <Card>
      <CardContent>
        <p className="mb-4">
          Enviado por: {offer.UserId} ({offer.Party})
        </p>
        <p className="mb-4">Valor: {offer.Value}</p>
        <p className="mb-4">Status: {offer.Status}</p>
        {offer.Status === "PENDING" && (
          <h1>Expira em: {formattedDate(offer.ExpiresAt)}</h1>
        )}
        <p>Message: {offer.Observation}</p>
      </CardContent>
    </Card>
  );
};

export default function History({ dealing }) {
  return (
    <div className="w-full lg:w-1/2">
      <Stack spacing={2}>
        {dealing.Offers.map((offer) => (
          <OfferCard offer={offer} key={offer.Id} />
        ))}
      </Stack>
    </div>
  );
}
