import React from "react";
import TabPanel from "Components/Ui/TabPanel";
import LinkTab from "Components/Ui/LinkTab";
import PropertyInfo from "Components/Ui/Tabs/PropertyInfo";
import User from "Components/Ui/Tabs/User";
import RootList from "Components/Ui/RootList";
import History from "./Tabs.jsx/History";

import { useSelector } from "react-redux";

const EditTabs = () => {
  const dealingDetails = useSelector((state) => state?.dealingDetails);
  const { dealing, loading, user, ownerUser, property } = dealingDetails;

  return (
    <RootList
      loading={loading}
      renderChildren={(value) => (
        <>
          <TabPanel value={value} index={0}>
            <History dealing={dealing} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <PropertyInfo property={property} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <User user={user} />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <User user={ownerUser} />
          </TabPanel>
        </>
      )}
    >
      <LinkTab label="HISTÓRICO" value={0} />
      <LinkTab label="INFORMAÇÃO DE PROPRIEDADE" index={1} />
      <LinkTab label="COMPRADOR" index={2} />
      <LinkTab label="PROPRIETÁRIO" index={3} />
    </RootList>
  );
};

export default EditTabs;
