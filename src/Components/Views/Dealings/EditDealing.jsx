import React, { useEffect } from "react";
import { getDealingDetails } from "redux/dealing/actions";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

import EditTabs from "./EditTabs";

const EditContract = ({ match }) => {
  const dispatch = useDispatch();

  const DealingId = match.params.id;

  useEffect(() => {
    if (DealingId) {
      dispatch(getDealingDetails(DealingId));
    }
  }, [dispatch, DealingId]);

  return (
    <>
      <div>
        <EditTabs />
      </div>
    </>
  );
};

export default withRouter(EditContract);
