import { formattedDate } from "utils";

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 90,
  },
  {
    field: "PropertyId",
    headerName: "Imóvel",
    width: 90,
  },
  {
    field: "UserId",
    headerName: "Usuário",
    width: 100,
  },
  {
    field: "LastOffer.Status",
    headerName: "Status",
    width: 100,
  },
  {
    field: "LastOffer.Value",
    headerName: "Valor",
    width: 100,
  },
  { field: "Purpose", headerName: "Propósito", width: 100 },
  {
    field: "CreatedAt",
    headerName: "Criado em",
    width: 100,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "UpdatedAt",
    headerName: "Atualizado em",
    width: 100,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
];

export default columns;
