/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { styled } from "@mui/material/styles";
import { DataGrid } from "@mui/x-data-grid";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import columnsData from "./columns";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import EditIcon from "@mui/icons-material/Edit";
import { listDealings } from "redux/dealing/actions";
import useFetchList from "hooks/useFetchList";

const PREFIX = "Dealings";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

const Dealings = () => {
  const { dispatch, page, limit, setLimit, setPage, sortModel } = useFetchList(
    1,
    18
  );

  const dealingList = useSelector((state) => state.dealingList);

  const { loading, Dealings, TotalDealings } = dealingList;

  const columns = [
    ...columnsData,
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Link
          to={`/dealings/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button startIcon={<EditIcon />}>Editar</Button>
          </Box>
        </Link>
      ),
    },
  ];

  const stringifiedSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    dispatch(listDealings({ page, limit, sortModel }));
  }, [dispatch, page, limit, stringifiedSortModel]);

  return (
    <Root>
      <div style={{ width: "100%", height: "100%" }}>
        <DataGrid
          autoHeight
          rows={Dealings}
          columns={columns}
          density="compact"
          page={page - 1}
          onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
          onPageChange={(newPage) => setPage(newPage + 1)}
          pageSize={limit}
          paginationMode="server"
          rowCount={TotalDealings}
          rowsPerPageOptions={[limit]}
          loading={loading}
          checkboxSelection
          pagination
          disableSelectionOnClick
        />
      </div>
    </Root>
  );
};

export default Dealings;
