import React from "react";
import { Grid } from "@mui/material";
import ShortcutCard from "../../Ui/ShortcutCard";

export default function Home() {
  const shortcuts = [
    {
      label: "Habita365",
      description: "Ambiente de produção",
      url: "https://habita365.com.br",
      image: "/images/shortcuts/habita.png",
    },
    {
      label: "Habita365 Staging",
      description: "Ambiente de testes",
      url: "https://staging.habita365.com.br",
      image: "/images/shortcuts/habita.png",
    },
    {
      label: "Slack",
      description: "Workspace Slack",
      url: "https://habita365.slack.com",
      image: "/images/shortcuts/slack.png",
    },
    {
      label: "Redash",
      description: "Dashboard com dados de BI",
      url: "https://redash.habita365.com.br",
      image: "/images/shortcuts/redash.png",
    },
    {
      label: "Superlógica",
      description: "Gestão de aluguel",
      url: "https://habita365.superlogica.net",
      image: "/images/shortcuts/superlogica.png",
    },
    {
      label: "Migrações",
      description: "Planilha com migração de usuários",
      url: "https://docs.google.com/spreadsheets/d/1D0bJhKjHJ0cxj9p29gMfbxU67A54TUuLlRtQg-rnvvA/edit?usp=sharing",
      image: "/images/shortcuts/sheets.png",
    },
  ];

  return (
    <Grid container spacing={4} justifyContent="center">
      {shortcuts.map(({ image, label, url, description }) => (
        <Grid item xs={12} sm={6} md={4} key={label}>
          <ShortcutCard
            image={image}
            label={label}
            url={url}
            description={description}
          />
        </Grid>
      ))}
    </Grid>
  );
}
