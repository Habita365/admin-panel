/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { styled } from "@mui/material/styles";
import { DataGrid } from "@mui/x-data-grid";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import columnsData from "./columns";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
// import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { listContracts } from "redux/contract/actions";
import useFetchList from "hooks/useFetchList";

const PREFIX = "Contracts";

const classes = {
  cell: `${PREFIX}-cell`,
  header: `${PREFIX}-header`,
};

const Root = styled("div")(() => ({
  [`& .${classes.cell}`]: {
    color: "red",
  },

  [`& .${classes.header}`]: {
    visibility: "hidden",
  },
}));

const Contracts = () => {
  const { dispatch, page, limit, setLimit, setPage, sortModel } = useFetchList(
    1,
    18
  );

  const contractList = useSelector((state) => state?.contractList);

  const { loading, Contracts, TotalContracts } = contractList;

  const columns = [
    ...columnsData,
    {
      field: "Edit",
      hideSortIcons: true,
      disableColumnMenu: true,
      width: 150,
      headerClassName: classes.header,
      cellClassName: classes.cell,
      renderCell: (params) => (
        <Link
          to={`/contracts/${params.id}/edit`}
          style={{ textDecoration: "none" }}
        >
          <Box display="flex" style={{ margin: "0 auto" }}>
            <Button startIcon={<EditIcon />}>Editar</Button>
          </Box>
        </Link>
      ),
    },
  ];

  const stringifiedSortModel = JSON.stringify(sortModel);

  useEffect(() => {
    dispatch(listContracts({ page, limit, sortModel }));
  }, [dispatch, page, limit, stringifiedSortModel]);

  return (
    <Root>
      <div style={{ width: "100%", height: "100%" }}>
        <DataGrid
          autoHeight
          rows={Contracts}
          columns={columns}
          density="compact"
          page={page - 1}
          onPageSizeChange={(newPageSize) => setLimit(newPageSize)}
          onPageChange={(newPage) => setPage(newPage + 1)}
          pageSize={limit}
          paginationMode="server"
          rowsPerPageOptions={[limit]}
          rowCount={TotalContracts}
          loading={loading}
          checkboxSelection
          pagination
          disableSelectionOnClick
          // sortingMode="server"
          // sortModel={sortModel}
          // onSortModelChange={onSortModelChange}
        />
      </div>
    </Root>
  );
};

export default Contracts;
