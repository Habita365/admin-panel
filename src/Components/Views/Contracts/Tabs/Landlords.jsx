import React from "react";
// import TextField from "Components/Ui/Forms/TextField";
// import SwitchField from "Components/Ui/Forms/SwitchField";
// import DateField from "Components/Ui/Forms/DateField";
// import FormControlRadio from "Components/Ui/Forms/FormControlRadio";
// import withContractForm from 'Components/Ui/Forms/withContractForm';

import TableUser, { Row } from "Components/Ui/Tables/TableUser";

const Landlords = ({ Contract }) => {
  const Landlords = Contract?.Landlords || [];

  return (
    <div className="w-full lg:w-1/2">
      <TableUser>
        {Landlords.map((landlord) => {
          const { PersonId, Person, Id, Percentage } = landlord;

          const {
            IsLegalEntity,
            Name,
            PhoneNumber,
            IdentificationNumber,
            Address,
          } = Person;
          return !IsLegalEntity ? (
            <Row
              key={Id}
              Name={Name}
              Percentage={Percentage}
              PersonId={PersonId}
              Cpf={IdentificationNumber}
              Address={Address}
              PhoneNumber={"+" + PhoneNumber}
            />
          ) : null;
        })}
      </TableUser>
    </div>
  );
};

export default Landlords;
