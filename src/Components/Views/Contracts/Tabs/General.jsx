import React from "react";
import TextField from "Components/Ui/Forms/TextDisplayField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import DateField from "Components/Ui/Forms/DateField";
import FormControlRadio from "Components/Ui/Forms/FormControlRadio";

const userTypeOptions = [
  {
    value: "OWNER",
    text: "Proprietário",
  },
  {
    value: "MANAGER",
    text: "Administrador",
  },
];

const General = ({ Contract }) => {
  return (
    <div>
      <div className="w-full lg:w-1/2">
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <FormControlLabel
            className="w-full"
            control={
              <Switch
                defaultChecked={!!Contract?.HasLegalEntity}
                disabled
                name="HasLegalEntity"
                color="primary"
              />
            }
            label="Você é uma"
          />
          <FormControlRadio
            name="RelationWithProperty"
            label="Confirme sua relação com o Imóvel?"
            value={Contract?.RelationWithProperty}
            data={userTypeOptions}
            disabled
          />
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextField
            content={Contract?.Status}
            name="Status"
            readOnly={false}
            disabled
            label="Status"
            placeholder="Status"
          />
          <TextField
            content={Contract?.Type}
            name="Type"
            readOnly={false}
            disabled
            label="Modelo"
            placeholder="Modelo"
          />
        </div>
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <DateField
            disabled
            value={Contract?.MovingDate}
            name="MovingDate"
            onChange={(value) => console.log(value)}
            label="Data de Início"
          />
        </div>
      </div>
    </div>
  );
};

export default General;
