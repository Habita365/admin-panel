import React from "react";

import TableUser, { Row } from "Components/Ui/Tables/TableUser";

const Tenants = ({ Contract }) => {
  let Tenants = Contract?.Tenants || [];

  const TenantsLength = Tenants.length;

  return (
    <div className="w-full lg:w-1/2">
      {TenantsLength > 0 && (
        <TableUser>
          {Tenants.map((tenant) => {
            const { Id, Person, Percentage } = tenant;
            const { Name, PhoneNumber, IdentificationNumber, Address } = Person;

            return (
              <Row
                key={Id}
                Name={Name}
                Percentage={Percentage}
                Cpf={IdentificationNumber}
                PersonId={Id}
                // openModalPatch={openModalPatch}
                Address={Address}
                // openModalDelete={openModalDelete}
                PhoneNumber={"+" + PhoneNumber}
              />
            );
          })}
        </TableUser>
      )}
    </div>
  );
};

export default Tenants;
