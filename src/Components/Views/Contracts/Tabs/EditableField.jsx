import React from "react";
import TextField from "Components/Ui/Forms/TextField";
import withContractForm from "Components/Ui/Forms/withContractForm";
import Button from "@mui/material/Button";

const EditableField = ({ control, loading }) => {
  return (
    <div>
      <div className="w-full lg:w-1/2">
        <div className="flex items-center gap-4 mb-4 flex-col sm:flex-row">
          <TextField
            control={control}
            name="SuperlogicaContractId"
            label="SuperlogicaContractId"
            placeholder="SuperlogicaContractId"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </div>
        <div className="mt-4">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            disabled={loading}
          >
            Enviar
          </Button>
        </div>
      </div>
    </div>
  );
};

export default withContractForm(EditableField);
