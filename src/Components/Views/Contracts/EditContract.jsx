import React, { useEffect } from "react";
import { getContractDetails } from "redux/contract/actions";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

import EditTabs from "./EditTabs";

const EditContract = ({ match }) => {
  const dispatch = useDispatch();

  const ContractId = match.params.id;

  useEffect(() => {
    if (ContractId) {
      dispatch(getContractDetails(ContractId));
    }
  }, [dispatch, ContractId]);

  return (
    <>
      <div>
        <EditTabs ContractId={ContractId} />
      </div>
    </>
  );
};

export default withRouter(EditContract);
