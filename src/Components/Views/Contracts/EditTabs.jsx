import React, { useState } from "react";
import TabPanel from "Components/Ui/TabPanel";
import LinkTab from "Components/Ui/LinkTab";
import RootList from "Components/Ui/RootList";

import { useSelector } from "react-redux";
import General from "./Tabs/General";
import EditableField from "./Tabs/EditableField";
import Landlords from "./Tabs/Landlords";
import Tenants from "./Tabs/Tenants";

const EditTabs = ({ ContractId }) => {
  const [success, setSuccess] = useState(false);

  const contractDetails = useSelector((state) => state?.contractDetails);
  const { Contract, loading } = contractDetails;

  const props = {
    ContractId,
    Contract,
    success,
    setSuccess,
  };

  return (
    <RootList
      loading={loading}
      setSuccess={setSuccess}
      renderChildren={(value) => (
        <>
          <TabPanel value={value} index={0}>
            <General {...props} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <EditableField {...props} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <Landlords {...props} />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <Tenants {...props} />
          </TabPanel>
        </>
      )}
    >
      <LinkTab label="GERAL" value={0} />
      <LinkTab label="CAMPOS EDITÁVEIS" value={1} />
      <LinkTab label="PROPRIETÁRIOS" value={2} />
      <LinkTab label="INQUILINOS" value={3} />
    </RootList>
  );
};

export default EditTabs;
