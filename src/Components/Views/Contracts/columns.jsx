import { formattedDate } from "utils";

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 90,
  },
  {
    field: "OwnerId",
    headerName: "Proprietário",
    width: 150,
  },
  {
    field: "UserId",
    headerName: "Usuario",
    // sortable: false,
    width: 150,
  },
  {
    field: "HasLegalEntity",
    headerName: "Entidade legal",
    type: "boolean",
    width: 120,
    // sortable: false,
  },
  {
    field: "RelationWithProperty",
    headerName: "Relação de propriedade",
    width: 120,
    // sortable: false,
  },
  {
    field: "Status",
    headerName: "Status",
    width: 120,
    // sortable: false,
  },
  {
    field: "Type",
    headerName: "Modelo",
    width: 120,
    // sortable: false,
  },
  {
    field: "MovingDate",
    headerName: "Data da mudança",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "UpdatedAt",
    headerName: "Atualizado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
  {
    field: "CreatedAt",
    headerName: "Criado em",
    width: 150,
    type: "date",
    valueFormatter: (params) => formattedDate(params),
  },
];

export default columns;
