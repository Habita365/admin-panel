import Layout from "Components/Layout";
import Users from "Components/Views/Users";
import Realtors from "Components/Views/Realtors";
import Persons from "Components/Views/Persons";
import Contracts from "Components/Views/Contracts";
import Properties from "Components/Views/Properties";
import Login from "Components/Views/Login";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import EditUser from "Components/Views/Users/EditUser";
import EditPerson from "Components/Views/Persons/EditPerson";
import EditContract from "Components/Views/Contracts/EditContract";
import EditProperty from "Components/Views/Properties/EditProperty";

import { useDispatch, useSelector } from "react-redux";

import numeral from "numeral";

import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import ptBrLocale from "date-fns/locale/pt-BR";
import Visits from "./Components/Views/Visits";
import EditVisit from "./Components/Views/Visits/EditVisit";
import { theme } from "./MuiTheme";
import { ThemeProvider } from "@mui/material/styles";
import Home from "./Components/Views/Home";
import Dealings from "Components/Views/Dealings";
import EditDealing from "Components/Views/Dealings/EditDealing";
import { useEffect } from "react";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { USER_LOGIN_SUCCESS } from "redux/auth/types";
import { logout } from "redux/auth/actions";

import "numeral/locales/pt-br";

numeral.locale("pt-br");

function App() {
  const dispatch = useDispatch();

  const firebaseState = useSelector((state) => state?.firebase);

  const { auth } = firebaseState;

  useEffect(() => {
    const auth = getAuth();

    const unsubscribe = onAuthStateChanged(auth, async (user) => {
      if (user) {
        const token = await user.getIdToken();

        localStorage.setItem("token", token);

        dispatch({
          type: USER_LOGIN_SUCCESS,
          userToken: token,
        });
      } else {
        localStorage.removeItem("token");
        dispatch(logout());
      }
    });

    return () => {
      unsubscribe();
    };
  }, [dispatch]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} locale={ptBrLocale}>
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/login" exact>
              <Login />
            </Route>
            <Layout auth={auth}>
              <Route path="/" exact>
                <Home />
              </Route>
              <Route path="/users" exact>
                <Users />
              </Route>
              <Route path="/users/:id/edit" exact>
                <EditUser />
              </Route>
              <Route path="/properties" exact>
                <Properties />
              </Route>
              <Route path="/properties/:id/edit" exact>
                <EditProperty />
              </Route>
              <Route path="/persons" exact>
                <Persons />
              </Route>
              <Route path="/persons/:id/edit" exact>
                <EditPerson />
              </Route>
              <Route path="/contracts" exact>
                <Contracts />
              </Route>
              <Route path="/contracts/:id/edit" exact>
                <EditContract />
              </Route>
              <Route path="/visits" exact>
                <Visits />
              </Route>
              <Route path="/visits/:id/edit" exact>
                <EditVisit />
              </Route>
              <Route path="/realtors" exact>
                <Realtors />
              </Route>
              <Route path="/dealings" exact>
                <Dealings />
              </Route>
              <Route path="/dealings/:id/edit" exact>
                <EditDealing />
              </Route>
            </Layout>
          </Switch>
        </Router>
      </ThemeProvider>
    </LocalizationProvider>
  );
}

export default App;
