import { createTheme } from "@mui/material";
import { common } from "@mui/material/colors";
// import { createTheme, adaptV4Theme } from "@mui/material";

const themeOptions = {
  palette: {
    mode: "light",
    // contrastThreshold: 2,
    primary: {
      main: "#7338f2",
      contractText: common.white,
    },
    secondary: {
      main: "#f47065",
    },
    warning: {
      main: "#f8d218",
    },
    success: {
      main: "#34a853",
    },
    error: {
      main: "#f8533a",
    },
    text: {
      primary: "#1f244b",
    },
  },
  typography: {
    fontFamily: "Poppins, Roboto, sans-serif",
    fontSize: 14,
  },
  shape: {
    borderRadius: 8,
  },
  props: {
    MuiList: {
      dense: true,
    },
    MuiMenuItem: {
      dense: true,
    },
    MuiTable: {
      size: "small",
    },
  },
};

export const theme = createTheme(themeOptions);
