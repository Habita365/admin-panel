// Regex
// const AssociateCode = /[346789ABCDEFGHJKLMNPQRTUVWXY]/;

export const CpfRegex = [
  /\d/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/,
];

export const CnpjRegex = [
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  "/",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/,
];

export const CepRegex = [/\d/, /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/];

// export const AssociateCodeRegex = [
//   AssociateCode,
//   AssociateCode,
//   AssociateCode,
//   AssociateCode,
// ];
