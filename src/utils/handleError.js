import { logout } from "redux/auth/actions";

export default function handleError(err, dispatch, dispatchType) {
  if (err.response?.status === 401) {
    dispatch(logout());
  }

  const errResMessage = err.response?.data?.message;

  const message = errResMessage ? errResMessage : err.message;
  if (message === "Not authorized, token failed") {
    dispatch(logout());
  }
  dispatch({
    type: dispatchType,
    payload: message,
  });
}
