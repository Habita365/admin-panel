export const addressTextTranform = (params, noCep) => {
  let text = "";
  for (let i = 0; i < params.length; i++) {
    if (params[i]) {
      if (i === params.length - 1) {
        if (noCep) {
          text += `${params[i]}`;
        } else {
          text += `CEP: ${params[i]}`;
        }
      } else {
        text += `${params[i]}, `;
      }
    }
  }

  return text;
};
