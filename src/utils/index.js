import { format } from "date-fns";
import { CnpjRegex, CpfRegex } from "./constants";
import { days } from "../Components/Views/Properties/Tabs/options";

export function dirtyValues(dirtyFields, allValues) {
  if (dirtyFields === true || Array.isArray(dirtyFields)) {
    return allValues;
  }
  return Object.fromEntries(
    Object.keys(dirtyFields).map((key) => [
      key,
      dirtyValues(dirtyFields[key], allValues[key]),
    ])
  );
}

export const objEmptyCheck = (obj) => {
  return (
    obj &&
    Object.keys(obj).length === 0 &&
    Object.getPrototypeOf(obj) === Object.prototype
  );
};

export const objCheck = (obj) => !objEmptyCheck(obj);

export const queryFunc = (sortModel, page, limit, address) => {
  let queryObj = {};
  if (sortModel && Object.keys(sortModel).length !== 0) {
    queryObj.sortModel = JSON.stringify(sortModel);
  }

  if (page > 1) {
    queryObj.page = page;
  }

  if (limit > 1) {
    queryObj.limit = limit;
  }

  if (address?.length > 0) {
    queryObj.Address = address;
  }

  return Object.keys(queryObj).length > 0 ? queryObj : false;
};

const isNumeric = (str) => {
  if (typeof str !== "string") return false;
  return !isNaN(str) && !isNaN(parseFloat(str));
};

export const isNotNumeric = (str) => {
  return isNumeric(str) === false;
};

export const formattedDate = (params) => {
  const paramsValue = params.value;

  if (paramsValue && isNumeric(paramsValue)) {
    const value = Number(paramsValue);
    return format(value, "dd/MM/yyyy");
  }
};

export const identificationMask = (value) => {
  if (value.length <= 14) {
    return CpfRegex;
  } else {
    return CnpjRegex;
  }
};

export const visitDaysComplete = (visitDays) => {
  visitDays = visitDays.filter(Boolean);

  for (let i = 0; i < days.length; i++) {
    const day = days[i].name;

    const visitDay = visitDays.find((item) => {
      return item.Day === day;
    });

    if (!visitDay) {
      visitDays.push({ Day: day, Timings: [] });
    }
  }

  return visitDays;
};
