export default function fillInAddress(address_components) {
  const address = {
    city: "",
    State: "",
    streetNumber: "",
    address: "",
    postalCode: "",
    neighbourhood: "",
  };
  for (const component of address_components) {
    for (const type of component.types) {
      switch (type) {
        case "street_number": {
          address.streetNumber = component.long_name || "";
          break;
        }

        case "postal_code": {
          address.postalCode = component.long_name || "";
          break;
        }

        case "sublocality_level_1": {
          address.neighbourhood = component.long_name || "";
          break;
        }

        case "sublocality_level_2": {
          address.neighbourhood = component.long_name || "";
          break;
        }

        case "sublocality_level_3": {
          address.neighbourhood = component.long_name || "";
          break;
        }

        case "sublocality_level_4": {
          address.neighbourhood = component.long_name || "";
          break;
        }

        case "sublocality_level_5": {
          address.neighbourhood = component.long_name || "";
          break;
        }

        case "route": {
          address.address = component.long_name || "";
          break;
        }

        case "administrative_area_level_1": {
          address.State = component.short_name || "";
          break;
        }

        case "administrative_area_level_2": {
          address.city = component.long_name || "";
          break;
        }

        default:
          break;
      }
    }
  }
  return address;
}
