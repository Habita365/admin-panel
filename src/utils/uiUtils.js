const avatarColors = [
  {
    bgColor: "var(--orange-light)",
    color: "var(--orange)",
  },
  {
    bgColor: "var(--principal-light)",
    color: "var(--principal)",
  },
  {
    bgColor: "var(--gray-500-light)",
    color: "var(--gray-500)",
  },
];

function getRandomValue(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export const randAvatarColors = () => {
  return getRandomValue(avatarColors);
};
